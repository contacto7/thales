<li class="">
    <a href="#templatesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-files-o"></i>
        Formatos
    </a>
    <ul class="collapse list-unstyled" id="templatesSubmenu">
        <li>
            <a href="{{ route('documentTemplates.list') }}">Listar</a>
        </li>
    </ul>
</li>
<li class="">
    <a href="#customersSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-building"></i>
        Clientes
    </a>
    <ul class="collapse list-unstyled" id="customersSubmenu">
        <li>
            <a href="{{ route('customers.list') }}">Listar</a>
        </li>
        <li>
            <a href="{{ route('customers.create') }}">Añadir</a>
        </li>
    </ul>
</li>
<li class="">
    <a href="#usersSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-users"></i>
        Usuarios
    </a>
    <ul class="collapse list-unstyled" id="usersSubmenu">
        <li>
            <a href="{{ route('users.list') }}">Listar</a>
        </li>
        <li>
            <a href="{{ route('users.create') }}">Añadir</a>
        </li>
    </ul>
</li>
<li class="">
    <a href="#aprobationSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-check-square-o"></i>
        Aprobar <span class="count-approve count-total">(0)</span>
    </a>
    <ul class="collapse list-unstyled" id="aprobationSubmenu">
        <li>
            <a href="{{ route('customers.listForApproving') }}">
                Clientes creados <span class="count-approve count-customer">(0)</span>
            </a>
        </li>
        <li>
            <a href="{{ route('documentDates.listForApproving') }}">
                Lotes creados <span class="count-approve count-doc-dates">(0)</span>
            </a>
        </li>
        <li>
            <a href="{{ route('documents.listForApproving') }}">
                Docs creados <span class="count-approve count-docs-created">(0)</span>
            </a>
        </li>
        <li>
            <a href="{{ route('documents.listForApproveDelete') }}">
                Docs eliminados <span class="count-approve count-docs-eliminated">(0)</span>
            </a>
        </li>
    </ul>
</li>

@include('partials.sidebar.logged')
