@extends('layouts.modern')

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Empresa"),
                    'icon' => "building"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ ! $user->id ? route('customers.store'): route('customers.update', ['user'=>$user->id, 'customer'=>$customer->id ]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($user->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label class="font-weight-bold color-third">DATOS DEL CONTACTO</label>
            </div>
            <div class="form-group">
                <label for="name">Nombres</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    value="{{ old('name') ?: $user->name }}"
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="last_name">Apellidos</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('last_name') ? 'is-invalid': '' }}"
                    name="last_name"
                    id="last_name"
                    value="{{ old('last_name') ?: $user->last_name }}"
                >
                @if($errors->has('last_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="email">Correo</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('email') ? 'is-invalid': '' }}"
                    name="email"
                    id="email"
                    value="{{ old('email') ?: $user->email }}"
                >
                @if($errors->has('email'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="password">Contraseña (mínimo 6 caracteres)</label>
                <input
                    type="password"
                    class="form-control {{ $errors->has('password') ? 'is-invalid': '' }}"
                    name="password"
                    id="password"
                    value=""
                >
                @if($errors->has('password'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="cellphone">Celular </label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('cellphone') ? 'is-invalid': '' }}"
                    name="cellphone"
                    id="cellphone"
                    value="{{ old('cellphone') ?: $user->cellphone }}"
                >
                @if($errors->has('cellphone'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('cellphone') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="position">Cargo </label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('position') ? 'is-invalid': '' }}"
                    name="position"
                    id="position"
                    value="{{ old('position') ?: $user->position }}"
                >
                @if($errors->has('position'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('position') }}</strong>
                </span>
                @endif
            </div>
            @if($user->id)
            <div class="form-group">
                <label for="state">Estado</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                    required
                >
                    <option
                        {{
                            (int) old('state') === \App\User::ACTIVE
                            ||
                            (int) $user->state === \App\User::ACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::ACTIVE }}"
                    >Activo</option>
                    <option
                        {{
                            (int) old('state') === \App\User::INACTIVE
                            ||
                            (int) $user->state === \App\User::INACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::INACTIVE }}"
                    >Inactivo</option>
                </select>
            </div>
            @endif

            <div class="form-group">
                <label class="font-weight-bold color-third">DATOS DE EMPRESA</label>
            </div>

            <div class="form-group">
                <label for="company_name">Razón Social</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_name') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    value="{{ old('company_name') ?: $customer->company_name }}"
                >
                @if($errors->has('company_name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="tax_number">RUC</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('tax_number') ? 'is-invalid': '' }}"
                    name="tax_number"
                    id="tax_number"
                    value="{{ old('tax_number') ?: $customer->tax_number }}"
                >
                @if($errors->has('tax_number'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('tax_number') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="turn">Giro de la empresa</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('turn') ? 'is-invalid': '' }}"
                    name="turn"
                    id="turn"
                    value="{{ old('turn') ?: $customer->turn }}"
                >
                @if($errors->has('turn'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('turn') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="workers">Número de trabajadores</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('workers') ? 'is-invalid': '' }}"
                    name="workers"
                    id="workers"
                    value="{{ old('workers') ?: $customer->workers }}"
                >
                @if($errors->has('workers'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('workers') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="">Logo de la empresa</label>
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('company_logo') ? 'is-invalid': '' }}"
                        name="company_logo"
                        id="company_logo"
                        placeholder=""
                        value=""
                    >
                    <label  class="custom-file-label" for="company_logo">

                        {{ $customer->company_logo ?:"Buscar imagen" }}
                    </label>
                    @if($errors->has('company_logo'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('company_logo') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="address">Dirección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    value="{{ old('address') ?: $customer->address }}"
                >
                @if($errors->has('address'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="phone">Teléfono</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('phone') ? 'is-invalid': '' }}"
                    name="phone"
                    id="phone"
                    value="{{ old('phone') ?: $customer->phone }}"
                >
                @if($errors->has('phone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="company_cellphone">Celular</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_cellphone') ? 'is-invalid': '' }}"
                    name="company_cellphone"
                    id="company_cellphone"
                    value="{{ old('company_cellphone') ?: $customer->cellphone }}"
                >
                @if($errors->has('company_cellphone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_cellphone') }}</strong>
                </span>
                @endif
            </div>



            <div class="form-group">
                <button type="submit" class="btn btn-thales-secondary">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
@endsection

@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
