<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DocumentType;
use Faker\Generator as Faker;

$factory->define(DocumentType::class, function (Faker $faker) {
    return [
        'name'=> $faker->word,
        'google_name'=> $faker->sentence,
        'extension'=> $faker->word,
    ];
});
