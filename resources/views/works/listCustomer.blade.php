@extends('layouts.modern')

@section('content')
    <div class="container container-servicios">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Servicios"),
                    'icon' => "briefcase"
                ])
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">Trabajo</th>
                    <th scope="col">Brindando</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Modificar</th>
                </tr>
                </thead>
                <tbody>
                @forelse($works as $work)
                    <tr>
                        <td>{{ $work->work_type_name }}</td>
                        <td>
                            @if($work->user_id)
                                Si
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            @if($work->user_id)
                                {{ $work->created_at }}
                            @endif
                        </td>
                        <td>

                            <div class="d-flex">
                            <form
                                method="POST"
                                action="{{ ! $work->work_id ? route('works.store'): route('works.update', $work->work_id) }}"
                                style="width: 100%"
                                class="monto-form"
                                novalidate
                            >
                                @if($work->work_id)
                                    @method('PUT')
                                @endif
                                @csrf
                                <div class="form-group" style="display: none">
                                    <label for="user_id">Id de usuario cliente:</label>
                                    <input type="number" class="form-control" name="user_id" value="{{ $userBelong }}">
                                </div>
                                <div class="form-group" style="display: none">
                                    <label for="work_type_id">Tipo de trabajo:</label>
                                    <input type="number" class="form-control" name="work_type_id" value="{{ $work->work_type_id }}">
                                </div>
                                <div class="d-flex">
                                    <input type="number" class="form-control monto-servicio" name="cost" value="{{ $work->cost }}" placeholder="Monto">

                                    @if($work->work_id)
                                        <button
                                            type="submit"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Actualizar monto"
                                            class="btn btn-outline-info actualizar-monto">
                                            <i class="fa fa-repeat"></i>
                                        </button>
                                    @else
                                        <button
                                            type="submit"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Agregar servicio"
                                            class="btn btn-outline-success">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    @endif

                                </div>
                            </form>
                            @if($work->work_id)
                                <form
                                    method="POST"
                                    action="{{ route('works.delete', $work->work_id) }}"
                                    class=""
                                    style="width: 37px"
                                    novalidate
                                >
                                    @method('DELETE')
                                    @csrf
                                    <button
                                        type="submit"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Eliminar servicio"
                                        class="btn btn-outline-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </form>
                            @endif
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center p-4 text-uppercase">
            <a
                class="btn btn-outline-info"
                href="{{ route('documentDates.listCustomer', $userBelong) }}"
            >
                <i class="fa fa-bars"></i>
                Ver documentos
            </a>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

    </div>
    <style>
        @cannot('viewCost', [\App\Work::class] )
        .monto-servicio, .actualizar-monto{
            display: none;
        }
        .monto-form{
            width: inherit !important;
        }
        @endcannot
    </style>
@endsection
@push('scripts')
@endpush
