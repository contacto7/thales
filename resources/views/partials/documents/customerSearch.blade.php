<div class="head-page">
    @include('partials.title', [
        'title' => __("Documentos"),
        'icon' => "files-o"
    ])
    <form action="{{ route('documents.customerSearch') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row d-none">
            <label
                for="document_date_id"
                class="col-sm-3 col-form-label"
            >
                Documento
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="document_date_id"
                    id="document_date_id"
                    type="number"
                    value="{{ $documentDateId }}"
                    placeholder="{{ __("Document Date Id") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Documento
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Ingrese el nombre del documento") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="sub_category_id"
                class="col-sm-3 col-form-label"
            >
                Categoría
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="sub_category_id"
                    id="sub_category_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\SubCategory::documentDateAvailable($documentDateId) as $subCategory)
                        <option
                            value="{{ $subCategory->id }}"
                        >{{ $subCategory->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-thales-secondary"
                    name="filter"
                    type="submit"
                    value="buscar"
                >
            </div>
        </div>
    </form>
</div>


