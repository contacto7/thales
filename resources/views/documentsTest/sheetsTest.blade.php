<?php
//require __DIR__ . '/vendor/autoload.php';
define('STDIN',fopen("php://stdin","r"));
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */

function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Docs API PHP Quickstart');
    $client->setScopes(Google_Service_Docs::DOCUMENTS);
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
    $client->setScopes(Google_Service_Docs::DRIVE);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');

    //HESER
    /*
        $authUrl = $client->createAuthUrl();
        dd($authUrl);
    */
    //HESER

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory('token.json');
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode("4/vwEcc5a6Qd7_mc8c8yLvinVbAK5eYKvswMBu5prSRU3WYgCdwQzIX6w");

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path)
{
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

/*COPIAR UN DOCUMENTO DESDE UN TEMPLATE*/
function copyFromTemplate($templateDocumentId, $copyTitle, $driveService){
    $copy = new Google_Service_Drive_DriveFile(array(
        'name' => $copyTitle
    ));
    $driveResponse = $driveService->files->copy($templateDocumentId, $copy);
    $documentCopyId = $driveResponse->id;

    return $documentCopyId;
}

/*ACTUALIZAR UN DOCUMENTO*/
function updateDocument($requests, $documentId, $service){
    $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        'requests' => $requests
    ]);

    $response = $service->spreadsheets->batchUpdate($documentId, $batchUpdateRequest);

    return $response;
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);
$driveService = new Google_Service_Drive($client);

// https://docs.google.com/document/d/195j9eDD3ccgjQRttHhJPymLJUCOUjs-jmwTrekvdjFE/edit
//$documentId = '1hlVFO4YCNESMxWvWHy44BiqE3838N2d28rCfEBZbZRk';

$documentId = '1uW861Y2PyiYa0R247ucnYOg18vB2MaOq0uvPUq6l1zw';

$doc = $service->spreadsheets->get($documentId);
$copyTitle = "Aca debería estar el titulo del documento - Empresa";
//$copyTitle = 'Copy - '.$doc->getTitle();
//printf("The document title is: %s\n", $doc->getTitle());


//PRIMERO: COPIAMOS EL DOCUMENTO
$documentCopyId = copyFromTemplate($documentId, $copyTitle, $driveService);

//SEGUNDO: DEFINIMOS LO QUE SE ACTUALIZARÁ
$requests = array();

//https://stackoverflow.com/questions/43664483/insert-image-into-google-sheets-cell-using-google-sheets-api
$requests[] =  new Google_Service_Sheets_Request([
    'findReplace' => [
        'find' => "#LOGO",
        //'replacement' => '=IMAGE("https://inoloop.com/img/Logo-morado.png",4,100,200)',
        'replacement' => '=IMAGE("https://inoloop.com/img/Logo-morado.png")',
        'allSheets' => true
    ]
]);

//TERCERO: ACTUALIZAMOS EL DOCUMENTO CREADO DEL TEMPLATE
$responseFromUpdate =  updateDocument($requests, $documentCopyId, $service);

$document_edit_url = "https://docs.google.com/spreadsheets/d/".$documentCopyId."/edit";
header('Location: '.$document_edit_url);
exit();

/*END HESER*/

