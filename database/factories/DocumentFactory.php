<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'name'=> $faker->word,
        'description'=> $faker->sentence,
        'state'=> $faker->numberBetween(1,4),
        'google_document_id'=> $faker->bothify('##????###?????####???#?#?#?#?#'),
        'document_template_id'=>\App\DocumentTemplate::all()->random()->id,
        'document_date_id'=>\App\DocumentDate::all()->random()->id,
        'google_download_id'=> $faker->bothify('##????###?????####???#?#?#?#?#'),
    ];
});
