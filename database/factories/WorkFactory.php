<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Work;
use Faker\Generator as Faker;

$factory->define(Work::class, function (Faker $faker) {
    return [
        'cost'=> $faker->randomFloat(2,800,2500),
        'work_type_id'=>\App\WorkType::all()->random()->id,
        'user_id'=>\App\Customer::all()->random()->id,
    ];
});
