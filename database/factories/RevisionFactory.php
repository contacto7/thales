<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Revision;
use Faker\Generator as Faker;

$factory->define(Revision::class, function (Faker $faker) {
    return [
        'comments'=> $faker->text,
        'type'=> $faker->numberBetween(1,2),
        'document_id'=>\App\Document::all()->random()->id,
        'user_id'=>\App\User::all()->random()->id,
    ];
});
