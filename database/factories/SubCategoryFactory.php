<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SubCategory;
use Faker\Generator as Faker;

$factory->define(SubCategory::class, function (Faker $faker) {
    return [
        'name'=> $faker->word,
        'description'=> $faker->sentence,
        'category_id'=>\App\Category::all()->random()->id,
    ];
});
