<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Helpers\Helper;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\UserRequest;
use App\Mail\ApproveCustomerMail;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function list(){
        //POLICIES
        $canViewOnlyTheirs = auth()->user()->can('viewOnlyTheirs', [Customer::class] );
        //END POLICIES

        $customers = Customer::with([])
            ->orderBy('id', 'desc')
            ->whereHas("user", function($sq) {
                $sq->where('role_id', Role::CUSTOMER);
                $sq->where('state', '!=', User::FOR_ACTIVATION);
            });

        if($canViewOnlyTheirs){
            $customers = $customers->where('user_created_id', auth()->user()->id);
        }

        $customers = $customers->paginate(12);

        //dd($vouchers);
        return view('customers.list', compact('customers') );
    }
    public function listForApproving(){
        $customers = Customer::with([])
            ->orderBy('id', 'desc')
            ->whereHas("user", function($sq) {
                $sq->where('role_id', Role::CUSTOMER);
                $sq->where('state', User::FOR_ACTIVATION);
            })
            ->paginate(12);

        //dd($vouchers);
        return view('customers.listForApproving', compact('customers') );
    }
    public function approve(Customer $customer){
        try {
            $customer->user->fill(['state'=>User::ACTIVE])->save();
            return back()->with('message',['success',
                __("Se aprobó el cliente")]);
        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error aprobando el cliente,
                por favor intente de nuevo.")]);


        }
    }

    public function admin($id){
        $customer = Customer::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $customer );
        //END POLICIES

        if($canViewThis){
            return view('customers.admin', compact('customer'));
        }else{
            return redirect('/')->with('message',['danger', __("Usted no puede ver este cliente.")]);
        }
    }
    public function filter(Request $request, Customer $customer){

        //$company = $company->newQuery();
        $customers = $customer::with([]);

        $name = $request->has('company_name') ? $request->input('company_name'): null;
        $tax_number = $request->has('tax_number') ? $request->input('tax_number'): null;
        $user_created_id = $request->has('user_created_id') ? $request->input('user_created_id'): null;

        //POLICIES
        $canViewOnlyTheirs = auth()->user()->can('viewOnlyTheirs', [Customer::class] );
        //END POLICIES


        // Search for a user based on their name.
        if ($name) {
            $customers->where('company_name', 'LIKE', "%$name%");
        }

        // Search for a user based on their name.
        if ($tax_number) {
            $customers->where('tax_number', $tax_number);
        }

        // Search for a user based on their name.
        if ($user_created_id) {
            $customers->where('user_created_id', $user_created_id);
        }

        if ($canViewOnlyTheirs){
            $customers->where('user_created_id', auth()->user()->id);
        }

        $customers = $customers->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('customers.list', compact('customers'));

    }

    public function create(){
        $user = new User();
        $customer = new Customer();

        $btnText = __("Crear cliente");
        return view('customers.form', compact('user', 'customer', 'btnText'));
    }

    public function store(CustomerRequest $customerRequest){
        //POLICIES
        $canCreate = auth()->user()->can('create', [Customer::class] );
        //END POLICIES

        //USER INFO
        $slug = $customerRequest->input('name').'-'.$customerRequest->input('last_name').'-'.\Str::random(4);
        $customerRequest->merge(['remember_token' => \Str::random(10) ]);
        $customerRequest->merge(['slug' => $slug ]);
        $customerRequest->merge(['role_id' => Role::CUSTOMER ]);

        //CHECK IF ADMIN HAS TO ACTIVATE
        $state = $canCreate ? User::ACTIVE : User::FOR_ACTIVATION;
        $customerRequest->merge(['state' => $state ]);
        //COMPANY INFO
        $company_name = $customerRequest->input('company_name');
        $company_logo = null;
        $tax_number = $customerRequest->input('tax_number');
        $turn = $customerRequest->input('turn');
        $workers = $customerRequest->input('workers');
        $address = $customerRequest->input('address');
        $phone = $customerRequest->input('phone');
        $company_cellphone = $customerRequest->input('company_cellphone');

        /*
         * TAKING OUT THE COMPANY INFO
         * BECAUSE WE FIRST SUBMIT THE USER REQUEST
         * THEN WE SUBMIT TE CUSTOMER INFO
         */

        if($customerRequest->has('company_logo')){
            $company_logo = Helper::uploadFile('company_logo', 'companies');
        }

        $customerRequest = $customerRequest->except(['company_name','company_logo','tax_number','address','turn','workers','phone','company_cellphone']);

        //dd($address);
        try {

            $user = User::create($customerRequest);

            $customer = Customer::create([
                'company_name' => $company_name,
                'company_logo' => $company_logo,
                'tax_number' => $tax_number,
                'address' => $address,
                'turn' => $turn,
                'workers' => $workers,
                'phone' => $phone,
                'cellphone' => $company_cellphone,
                'user_id' => $user->id,
                'user_created_id' => auth()->user()->id
            ]);

            //dd($customerRequest);
            //return back()->with('message',['success', __("Se agregó el cliente correctamente.")]);
            //WE REDIRECT TO THE SERVICES OFFERED TO CUSTOMER
            if($canCreate){
                return redirect()->route('works.listCustomer', $user->id);
            }else{

                $admins = User::where('role_id',Role::ADMIN)->get();

                $recipients = $admins;

                \Mail::to($recipients)->send( new ApproveCustomerMail( $customer, auth()->user() ) );

                return redirect()->route('customers.list')->with('message',['success',
                    __("Se creó el cliente, solicite la activación al administrador.")]);
            }

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el cliente,
                por favor verifique que está colocando los datos requeridos.")]);


        }



    }

    public function edit($id){
        $customer = Customer::where('id',$id)->first();
        $user = $customer->user;

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $customer );
        //END POLICIES

        $btnText = "Actualizar cliente";

        //dd($vouchers);
        if ($customer && $canViewThis ) {
            return view('customers.form', compact('customer','user','btnText'));
        }else{
            return redirect('/')->with('message',['danger', __("No tiene permiso para editar este cliente.")]);
        }

    }

    public function update(CustomerRequest $customerRequest, User $user, Customer $customer){

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $customer );
        //END POLICIES

        if(!$canViewThis){
            return redirect('/')->with('message',['danger', __("No tiene permiso para editar este cliente.")]);
        }

        //USER INFO
        $slug = $customerRequest->input('name').'-'.$customerRequest->input('last_name').'-'.\Str::random(4);
        $customerRequest->merge(['slug' => $slug ]);
        //COMPANY INFO
        $customerReq = new \stdClass();
        $company_name = $customerRequest->has('company_name') ? $customerRequest->input('company_name'): null;
        $company_logo = null;
        $tax_number = $customerRequest->has('tax_number') ? $customerRequest->input('tax_number'): null;
        $address = $customerRequest->has('address') ? $customerRequest->input('address'): null;
        $turn = $customerRequest->has('turn') ? $customerRequest->input('turn'): null;
        $workers = $customerRequest->has('workers') ? $customerRequest->input('workers'): null;
        $phone = $customerRequest->has('phone') ? $customerRequest->input('phone'): null;
        $company_cellphone = $customerRequest->has('company_cellphone') ? $customerRequest->input('company_cellphone'): null;

        /*
         * TAKING OUT THE COMPANY INFO
         * BECAUSE WE FIRST SUBMIT THE USER REQUEST
         * THEN WE SUBMIT TE CUSTOMER INFO
         */
        if($customerRequest->has('company_logo')){
            \Storage::delete('companies/'.$customerRequest->company_logo);
            $company_logo = Helper::uploadFile('company_logo', 'companies');
            $customerRequest->merge(['company_logo' => $company_logo ]);
        }

        //IF HAS PASSWORD, WE SAVE, IF NOT, WE TAKE OUT PASSWORD OF THE VALUES UPDATED
        if($customerRequest->has('password')){
            $userRequest = $customerRequest->except(['company_name','company_logo','tax_number','address','turn','workers','phone','company_cellphone']);
        }else{
            $userRequest = $customerRequest->except(['company_name','company_logo','tax_number','address','turn','workers','phone','company_cellphone', 'password']);
        }

        //dd($address);
        try {
            //FIRST WE FILL THE USER INFORMATION
            $user->fill($userRequest)->save();

            $company_name ? $customerReq->company_name = $company_name : null;
            $company_logo ? $customerReq->company_logo = $company_logo : null;
            $tax_number ? $customerReq->tax_number = $tax_number : null;
            $address ? $customerReq->address = $address : null;
            $turn ? $customerReq->turn = $turn : null;
            $workers ? $customerReq->workers = $workers : null;
            $phone ? $customerReq->phone = $phone : null;
            $company_cellphone ? $customerReq->cellphone = $company_cellphone : null;

            $customer->fill((array) $customerReq)->save();

            //dd($customerRequest);
            //return back()->with('message',['success', __("Se agregó el cliente correctamente.")]);
            //WE REDIRECT TO THE SERVICES OFFERED TO CUSTOMER
            return back()->with('message',['success', __("Se actualizó el cliente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el cliente,
                por favor verifique que está colocando los datos requeridos.")]);


        }







    }



}
