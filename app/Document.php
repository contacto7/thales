<?php

namespace App;

use App\Traits\GoogleApi;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Document
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $state
 * @property string $google_document_id
 * @property int $user_created_id
 * @property int $user_belong_id
 * @property int|null $probability
 * @property int|null $impact
 * @property int $document_template_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\DocumentTemplate $documentTemplate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Revision[] $revisions
 * @property-read int|null $revisions_count
 * @property-read \App\User $userBelong
 * @property-read \App\User $userCreated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereDocumentTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereGoogleDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereImpact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereProbability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereUserBelongId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Document whereUserCreatedId($value)
 * @mixin \Eloquent
 */
class Document extends Model
{
    use GoogleApi;

    const FOR_REVISION = 1;
    const REVISED = 2;
    const APPROVED = 3;
    const PRINTED = 4;
    const FOR_DELETION = 5;

    const RETRYS = 10;

    //TEXTS
    const EMPRESA = "##EMPRESA##";
    const DIRECCION = "##DIRECCION##";
    const GIRO = "##GIRO##";
    const TRABAJADORES = "##TRABAJADORES##";
    const RUC = "##RUC##";
    const NOMBRES = "##NOMBRES##";
    const APELLIDOS = "##APELLIDOS##";
    const LOGO = "##LOGO##";
    const FECHA = "##FECHA##";

    protected $fillable = [
        'name','description','state', 'google_document_id', 'document_template_id',
        'document_date_id','google_download_id', 'user_deleted_id',
    ];

    public function documentDate(){
        return $this->belongsTo(DocumentDate::class);
    }
    public function documentTemplate(){
        return $this->belongsTo(DocumentTemplate::class);
    }
    public function revisions(){
        return $this->hasMany(Revision::class);
    }
    public function userDeleted(){
        return $this->belongsTo(User::class);
    }

    public function createDocument($documentTemplate,$document_date_id,$driveService,$docsService,$sheetsService,$slidesService,$user,$document_date_formatted){
        $contact_name = $user->name;
        $contact_last_name = $user->last_name;
        $contact_email = $user->last_name;
        $company_name = $user->customer->company_name;

        $whitelist = array(
            '127.0.0.1',
            '::1'
        );

        /*ADD IMAGES ONLY WHEN IN PRODUCTION, BECAUSE CANNOT ADD LOCAL IMAGES TO DOCS*/
        if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            $company_logo = $user->customer->company_logo ? $user->customer->pathAttachment():url('image/company-logo.png');
            $company_logo = \App::make('url')->to($company_logo);
        }else{
            $company_logo = "https://inoloop.com/img/Logo-morado.png";
        }
        /*
        */
        $ruc = (string) $user->customer->tax_number;
        $company_workers= (string) $user->customer->workers;
        $company_address = (string) $user->customer->address;
        $company_turn = (string) $user->customer->turn;

        //VARIABLES
        $documentId = $documentTemplate->google_document_id;
        $copyTitle = $documentTemplate->name." - ".$company_name;
        $documentType = $documentTemplate->document_type_id;
        //VARIABLES

        //PICK THE CORRECT SERVICE: DOCS, SPREADSHEETS OR SLIDES
        $service = $this->getServiceByType($docsService, $sheetsService, $slidesService,  $documentType);

        //COPIA DE DOCUMENTO
        try {
            //Si lanza un error el drive, continuamos
            $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);
        }
        catch (\Throwable $t) {
            //continue
        }

        $document  = $this->getDocumentByType($service, $documentCopyId, $documentType);

        $requests = array();

        $requestsReplaceImage = $this->replaceImageByType($company_logo,Document::LOGO, $documentType, $document);

        if($company_logo){
            if (is_array($requestsReplaceImage)){
                $requests = array_merge($requests, $requestsReplaceImage);
            }else{
                $requests[] = $requestsReplaceImage;
            }
        }
        //REORDENAMOS LAS INSERCIONES DE IMAGENES, PARA QUE SE AGREGUEN DESDE EL ÚLTIMO Y
        //NO SE VAYA PERDIENDO EL POSICIONAMIENTO DE LOS OTROS REEMPLAZOS
        $requests = array_reverse($requests);

        $requests[] = $this->replaceTextByType(Document::LOGO, "", $documentType);
        $requests[] = $this->replaceTextByType(Document::NOMBRES, $contact_name, $documentType);
        $requests[] = $this->replaceTextByType(Document::FECHA, $document_date_formatted, $documentType);
        $requests[] = $this->replaceTextByType(Document::EMPRESA, $company_name, $documentType);
        $requests[] = $this->replaceTextByType(Document::RUC, $ruc, $documentType);
        if($company_address){
            $requests[] = $this->replaceTextByType(Document::DIRECCION, $company_address, $documentType);
        }
        if($company_turn){
            $requests[] = $this->replaceTextByType(Document::GIRO, $company_turn, $documentType);
        }
        if($company_workers){
            $requests[] = $this->replaceTextByType(Document::TRABAJADORES, $company_workers, $documentType);
        }

        //ACTUALIZACION DE DOCUMENTO
        try {
            //Si lanza un error el drive, continuamos
            $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $service, $documentType);
        }
        catch (\Throwable $t) {
            //continue
        }


        //CREATION OF DOCUMENT IN THE SYSTEM
        $document = Document::create([
            'name'=>$copyTitle,
            'description'=>$copyTitle,
            'state'=>1,
            'google_document_id'=>$documentCopyId,
            'document_template_id'=>$documentTemplate->id,
            'document_date_id'=>$document_date_id,
        ]);

        //WE STORE THE REVISION
        $revision = Revision::create(
            [
                'comments'=>'Creó documento',
                'type'=>Revision::DOCUMENT,
                'document_id'=>$document->id,
                'user_id'=>auth()->user()->id
            ]
        );
    }
}
