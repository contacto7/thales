<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'random_identifier','completed','total','document_date_id',
    ];

    public function documentDate(){
        return $this->belongsTo(DocumentDate::class);
    }
}
