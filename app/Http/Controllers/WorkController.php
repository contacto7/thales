<?php

namespace App\Http\Controllers;

use App\DocumentDate;
use App\Http\Requests\WorkRequest;
use App\Work;
use App\WorkType;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    //
    public function listCustomer($id){
        $userBelong = $id;

        $works = WorkType::
            leftJoin('works', function($join) use ($userBelong){
                $join->on('work_types.id', '=', 'works.work_type_id')
                    ->where('works.user_id', $userBelong);
            })
            ->orderBy('work_types.id', 'desc')
            ->select(
                'work_types.id as work_type_id',
                'work_types.name as work_type_name',
                'works.id as work_id',
                'works.user_id as user_id',
                'works.cost as cost',
                'works.created_at as created_at'
            )
            ->get();

        //sdd($works);
        return view('works.listCustomer', compact('works', 'userBelong') );
    }

    public function store(WorkRequest $workRequest){

        try {
            Work::create($workRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó el servicio correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el servicio,
                por favor verifique que está colocando los datos requeridos.")]);
        }

    }

    public function update(WorkRequest $workRequest, Work $work){
        try {
            $work->fill($workRequest->only('cost'))->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el servicio.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error editando el servicio,
                por favor verifique que está colocando los datos requeridos.")]);


        }
    }

    public function delete(Work $work){
        try {
            $work->delete();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se eliminó el servicio.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error eliminandoel servicio,
                por favor verifique que está colocando los datos requeridos.")]);


        }
    }



}
