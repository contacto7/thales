<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Customer
 *
 * @property int $id
 * @property string $company_name
 * @property string $company_logo
 * @property string $tax_number
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $cellphone
 * @property int $user_id
 * @property int $user_created_id
 * @property-read \App\User $user
 * @property-read \App\User $userCreated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Work[] $works
 * @property-read int|null $works_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCompanyLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereTaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserCreatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserId($value)
 * @mixin \Eloquent
 */
class Customer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'company_name','company_logo','tax_number', 'address', 'turn',
        'workers','phone','cellphone','user_id','user_created_id',
    ];

    public function pathAttachment(){
        return "/images/companies/".$this->company_logo;
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function userCreated(){
        return $this->belongsTo(User::class, 'user_created_id');
    }


}
