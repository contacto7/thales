<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role_id === Role::ADMIN) {
            return $next($request);
        }

        return redirect()->route('home'); // If user is not an admin.
    }
}
