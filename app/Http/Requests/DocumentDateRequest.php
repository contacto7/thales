<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentDateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':{
                return [
                    'document_date' => 'required|date_format:Y-m-d',
                ];
            }
            case 'POST':{
                return [
                    'user_belong_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'document_date' => 'required|date_format:Y-m-d',
                ];
            }
        }
    }
}
