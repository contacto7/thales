<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DocumentDate;
use Faker\Generator as Faker;

$factory->define(DocumentDate::class, function (Faker $faker) {
    return [
        'document_date'=>$faker->dateTime,
        'user_created_id'=>\App\User::all()->random()->id,
        'user_belong_id'=>\App\User::all()->random()->id,
        'state'=> $faker->numberBetween(1,2),
    ];
});
