@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.documents.adminSearch')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">Documentos</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Estado</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($documents as $document)
                    <tr>
                        <td>

                            <a
                                class="deleteDocument"
                                href="{{ route('documents.delete', $document->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Eliminar"
                            >
                                <i class="fa fa-times"></i>
                            </a>
                            {{ $document->name.$document->documentTemplate->documentType->extension }}
                        </td>
                        <td>

                            <a
                                class=""
                                href="{{ route('customers.admin', $document->documentDate->userBelong->customer->id) }}"
                            >
                                {{ $document->documentDate->userBelong->customer->company_name }}
                            </a>
                        </td>
                        <td>
                            <div class="form-group">
                                @if((int) $document->state === \App\Document::FOR_DELETION)
                                    POR ELIMINAR
                                @else
                                    <select
                                        class="form-control document-state-change"
                                        data-document="{{ $document->id }}"
                                        required
                                        @cannot('changeState', [\App\Document::class] ) disabled  @endcannot
                                    >
                                        <option
                                            {{ (int) $document->state === \App\Document::FOR_REVISION ? 'selected' : '' }}
                                            value="{{ \App\Document::FOR_REVISION }}"
                                        >Por revisar</option>
                                        <option
                                            {{
                                                (int) $document->state === \App\Document::REVISED ? 'selected' : '' }}
                                            value="{{ \App\Document::REVISED }}"
                                        >Revisado</option>
                                        <option
                                            {{ (int) $document->state === \App\Document::APPROVED ? 'selected' : '' }}
                                            value="{{ \App\Document::APPROVED }}"
                                            @cannot('changeStateToApproved', [\App\Document::class] ) disabled  @endcannot
                                        >Aprobado</option>
                                        <option
                                            {{ (int) $document->state === \App\Document::PRINTED ? 'selected' : '' }}
                                            value="{{ \App\Document::PRINTED }}"
                                            @cannot('changeStateToPrinted', [\App\Document::class] ) disabled  @endcannot
                                        >Impreso</option>
                                    </select>
                                @endif
                            </div>
                        </td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.editDocument', ['document_id'=>$document->id, 'google_document_id'=>$document->google_document_id, 'google_name'=>$document->documentTemplate->documentType->google_name]) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Editar documento"
                                target="_blank"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a
                                class="btn btn-outline-info editDocument"
                                data-toggle="modal"
                                data-target="#modalEdit"
                                href="#modalEdit"
                                data-id="{{ $document->id }}"
                                data-name="{{ $document->name }}"
                                title="Subir documento de descarga"
                            >
                                <i class="fa fa-upload"></i>
                            </a>
                            @can('printDocument', [\App\Document::class, $document])
                                <a
                                    class="btn btn-outline-info"
                                    @if($document->google_download_id)
                                    href="{{ route('documents.downloadDocumentAddedForPrint', ['document_id'=>$document->id, "google_document_id"=>$document->google_download_id, "filename"=>$document->name]) }}"
                                    @else
                                    href="{{ route('documents.downloadDocument', ['document_id'=>$document->id, "google_document_id"=>$document->google_document_id, "filename"=>$document->name]) }}"
                                    @endif

                                    target="_blank"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Imprimir documento"
                                >
                                    <i class="fa fa-print"></i>
                                </a>
                            @endcan
                            <a
                                class="btn btn-outline-info btn-revisions"
                                data-toggle="modal"
                                data-placement="top"
                                title="Historial de revisiones"
                                data-target="#modalRevisions"
                                href="#modalRevisions"
                                data-id="{{ $document->id }}"
                            >
                                <i class="fa fa-clock-o"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalRevisions">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Revisiones</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>


        <div class="row justify-content-center">
            {{ $documents->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        let document_id_actual;
        /*
        * Función para cambiar el estado de un documento
         * cuando se cambio el selector del documento
        */
        $(document).on('change', '.document-state-change', function(){
            let document_id = $(this).attr("data-document");
            let state = $(this).val();

            console.log(document_id);
            console.log(state);

            changeStateFromDocumentSelected(document_id, state);
        });

        function changeStateFromDocumentSelected(document_id, state) {
            //Enviamos una solicitud con el id del documento
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('documents.changeState') }}',
                data: {
                    document_id: document_id,
                    state: state
                },
                success: function (data) {
                    switch (parseInt(data)) {
                        case 1:
                            console.log('Se actualizó el estado.');
                            break;
                        case 2:
                            console.log('Hubo un error actualizando el documento.');
                            break;
                        case 3:
                            console.log('No se ingresó un ID del documento.');
                            break;
                        case 4:
                            console.log('No puede cambiar el estado del documento.');
                            break;
                        case 5:
                            console.log('No puede cambiar el estado del documento a aprobado.');
                            break;
                        case 6:
                            console.log('No puede cambiar el estado del documento a impreso.');
                            break;
                    }


                },error:function(){
                    console.log(data);
                }
            });
        }

        /*
        * Función para ver las revisiones del documento
        */
        $(document).on('click', '.btn-revisions', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let document_id = $(this).attr('data-id');
            let type = {{ \App\Revision::DOCUMENT }};

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('revisions.modalSeeForm') }}',
                data: {
                    document_id: document_id,
                    type: type
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        document_id_actual = document_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.revisions-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&document_id="+document_id_actual;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });

        $(document).on('click', '.create-docs-btn', function(){
            $("#tracking-label").html("Creando documento.");
        });
        /*
        * Función para poner el id del documento
        * en el modal
        */
        $(document).on('click', '.editDocument', function(){
            let data_id = $(this).data('id');
            let data_name = $(this).data('name');

            console.log(data_id);
            console.log(data_name);

            $("#model_id").val(data_id);
            $("#model_name").val(data_name);
        });
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
