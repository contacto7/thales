@component('mail::message')
# NUEVO CLIENTE REGISTRADO:

El usuario {{ $userCreated->name." ".$userCreated->last_name }} ha registrado el cliente <a href="{{ route('customers.admin', $customer->id) }}">{{ $customer->company_name }}</a>:

@component('mail::button', ['url' => route('customers.approve', $customer->id) ])
    Aprobar
@endcomponent

Gracias,<br>

{{ env("APP_NAME", "ThalesCorp") }}
@endcomponent
