<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Document;
use App\DocumentDate;
use App\DocumentTemplate;
use App\DocumentType;
use App\Helpers\Helper;
use App\Mail\ApproveDocumentDeletionMail;
use App\Mail\ApproveDocumentMail;
use App\Revision;
use App\Role;
use App\Track;
use App\Traits\GoogleApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class DocumentController extends Controller
{
    use GoogleApi;

    public function list(){
        $customers = Customer::with([])
            ->orderBy('id', 'desc')
            ->whereHas("user", function($sq) {
                $sq->where('role_id', Role::CUSTOMER);
            })
            ->paginate(12);

        //dd($vouchers);
        return view('customers.list', compact('customers') );
    }

    public function listForApproving(){
        $documents = Document::with([])
            ->orderBy('id', 'desc')
            ->where('state', '<', Document::APPROVED)
            ->paginate(12);

        //dd($vouchers);
        return view('documents.listForApproving', compact('documents') );
    }

    public function listForApproveDelete(){
        $documents = Document::with([])
            ->orderBy('id', 'desc')
            ->where('state', Document::FOR_DELETION)
            ->paginate(12);

        //dd($vouchers);
        return view('documents.listForApproveDelete', compact('documents') );
    }
    public function approveDelete($id){
        $document = Document::whereId($id);
        try {
            $document->delete();
            return redirect()->route('documents.listForApproveDelete')->with('message',['success', __("Se eliminó el documento.")]);
        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error eliminando el documento,
                por favor intente de nuevo.")]);


        }
    }
    public function listCustomerFromDate($id){
        $documentDate = DocumentDate::where('id',$id)->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $documentDate );
        //END POLICIES

        if (!$canViewThis){
            return redirect('/')->with('message',['danger', __("Usted no puede ver estos documentos.")]);
        }

        $documents = Document::with([])
            ->join('document_templates','documents.document_template_id','=','document_templates.id')
            ->select('documents.*', 'document_templates.id', 'documents.id as document_id')
            ->orderBy('document_templates.sub_category_id', 'desc')
            ->where('document_date_id', $id)
            ->paginate(12);

        $documentDateId = $id;

        //dd($documents);
        return view('documents.listCustomerFromDate', compact('documents', 'documentDateId') );
    }
    public function listMyDocuments($id = null){

        //If not id given, list the last documentDate
        if(!$id){
            $documentDate = DocumentDate::where('user_belong_id', auth()->user()->id)->orderBy('id', 'desc')->first();
            //Check if there is a documentDate
            //If there isn't, list 0
            if ($documentDate){
                $id = $documentDate->id;
            }else{
                $id = 0;
            }
        }

        $documents = Document::with([])
            ->join('document_templates','documents.document_template_id','=','document_templates.id')
            ->select('documents.*', 'document_templates.id', 'documents.id as document_id')
            ->orderBy('document_templates.sub_category_id', 'desc')
            ->where('document_date_id', $id)
            ->where('documents.state','>=', Document::APPROVED)
            ->paginate(12);

        $documentDateId = $id;

        //dd($documents);
        return view('documents.listMyDocuments', compact('documents', 'documentDateId') );
    }
    public function listCustomer($id){
        $documents = Document::with([])
            ->orderBy('id', 'desc')
            ->where('user_belong_id', $id)
            ->paginate(12);

        //dd($vouchers);
        return view('documents.listCustomer', compact('documents') );
    }
    public function editDocument($document_id, $google_document_id, $google_name){

        $revision = Revision::create(
            [
                'comments'=>'Editó documento',
                'type'=>Revision::DOCUMENT,
                'document_id'=>$document_id,
                'user_id'=>auth()->user()->id
            ]
        );

        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$google_document_id."/edit";

        return \Redirect::to($document_edit_url);

        //return view('documents.updateFromTemplate' );
    }
    public function downloadDocument($document_id, $google_document_id, $filename){
        $revision = Revision::create(
            [
                'comments'=>'Imprimió documento',
                'type'=>Revision::DOCUMENT,
                'document_id'=>$document_id,
                'user_id'=>auth()->user()->id
            ]
        );

        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);

        //DESCARGAMOS EL DOCUMENTO
        $responseD = $this->downloadDocumentFromDrive($google_document_id, $driveService, $filename);

        return $responseD;

        //return view('documents.updateFromTemplate' );
    }
    public function downloadDocumentAddedForPrint($document_id, $google_document_id, $filename){
        $revision = Revision::create(
            [
                'comments'=>'Imprimió documento',
                'type'=>Revision::DOCUMENT,
                'document_id'=>$document_id,
                'user_id'=>auth()->user()->id
            ]
        );

        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);

        //DESCARGAMOS EL DOCUMENTO
        $responseD = $this->downloadDocumentAddedForPrintFromDrive($google_document_id, $driveService);

        return $responseD;

        //return view('documents.updateFromTemplate' );
    }
    public function filter(Request $request, Document $documents){

        //$company = $company->newQuery();
        $documents = $documents::with([]);

        $documentDateId = $request->has('document_date_id') ? $request->input('document_date_id'): null;
        $name = $request->has('name') ? $request->input('name'): null;
        $work_type_id = $request->has('work_type_id') ? $request->input('work_type_id'): null;
        $sub_category_id = $request->has('sub_category_id') ? $request->input('sub_category_id'): null;
        $state = $request->has('state') ? $request->input('state'): null;

        //POLICIES
        //$canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        $canListAllUsers = true;
        //END POLICIES

        // Search for a user based on their name.
        if ($documentDateId) {
            $documents->where('document_date_id', $documentDateId);
        }else{
            //IF THERE IS NOT SET THE USER WHERE THE DOCUMENTS BELONG, WE RETURN BACK
            return back();
        }

        // Search for a user based on their name.
        if ($name) {
            $documents->where('name', 'LIKE', "%$name%");
        }

        // Search for a user based on their state.
        if ($state) {
            $documents->where('state', $state);
        }

        // Search for a user based on the documents for the work selected.
        if ($work_type_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($work_type_id) {
                $q->whereHas("workTypes", function ($q) use ($work_type_id) {
                    $q->where("id", $work_type_id);
                });
            });
        }

        // Search for a user based on their name.
        if ($sub_category_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($sub_category_id) {
                $q->where("sub_category_id", $sub_category_id);
            });
        }


        if($canListAllUsers || $name || $work_type_id || $sub_category_id){
            $documents = $documents->select('*', 'documents.id as document_id')->orderBy('id', 'desc')->paginate(12);
        }else{
            $documents =  $documents->select('*', 'documents.id as document_id')->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('documents.listCustomerFromDate', compact('documents', 'documentDateId') );

    }
    public function adminFilter(Request $request, Document $documents){

        //$company = $company->newQuery();
        $documents = $documents::with([]);

        $company_name = $request->has('company_name') ? $request->input('company_name'): null;
        $name = $request->has('name') ? $request->input('name'): null;
        $work_type_id = $request->has('work_type_id') ? $request->input('work_type_id'): null;
        $sub_category_id = $request->has('sub_category_id') ? $request->input('sub_category_id'): null;
        $state = $request->has('state') ? $request->input('state'): null;

        //POLICIES
        //$canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        $canListAllUsers = true;
        //END POLICIES

        // Search for a user based on their name.
        if ($company_name) {
            $documents->whereHas("documentDate", function ($q) use ($company_name) {
                $q->whereHas("userBelong", function ($q) use ($company_name) {
                    $q->whereHas("customer", function ($q) use ($company_name) {
                        $q->where('company_name', 'LIKE', "%$company_name%");
                    });
                });
            });
        }

        // Search for a user based on their name.
        if ($name) {
            $documents->where('name', 'LIKE', "%$name%");
        }

        // Search for a user based on their state.
        if ($state) {
            $documents->where('state', $state);
        }else{
            $documents->where('state', '<', Document::APPROVED);
        }

        // Search for a user based on the documents for the work selected.
        if ($work_type_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($work_type_id) {
                $q->whereHas("workTypes", function ($q) use ($work_type_id) {
                    $q->where("id", $work_type_id);
                });
            });
        }

        // Search for a user based on their name.
        if ($sub_category_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($sub_category_id) {
                $q->where("sub_category_id", $sub_category_id);
            });
        }


        if($canListAllUsers || $name || $work_type_id || $sub_category_id){
            $documents = $documents->select('*', 'documents.id as document_id')->orderBy('id', 'desc')->paginate(12);
        }else{
            $documents =  $documents->select('*', 'documents.id as document_id')->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('documents.listForApproving', compact('documents') );

    }
    public function customerFilter(Request $request, Document $documents){

        //$company = $company->newQuery();
        $documents = $documents::with([]);

        $documentDateId = $request->has('document_date_id') ? $request->input('document_date_id'): null;
        $name = $request->has('name') ? $request->input('name'): null;
        $work_type_id = $request->has('work_type_id') ? $request->input('work_type_id'): null;
        $sub_category_id = $request->has('sub_category_id') ? $request->input('sub_category_id'): null;
        $state = $request->has('state') ? $request->input('state'): null;

        //POLICIES
        //$canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        $canListAllUsers = true;
        //END POLICIES

        // Search for a user based on their name.
        if ($documentDateId) {
            $documents->where('document_date_id', $documentDateId);
        }else{
            //IF THERE IS NOT SET THE USER WHERE THE DOCUMENTS BELONG, WE RETURN BACK
            return back();
        }

        // Search for a user based on their name.
        if ($name) {
            $documents->where('name', 'LIKE', "%$name%");
        }

        // Search for a user based on their state.
        if ($state) {
            $documents->where('state', $state);
        }

        // Search for a user based on the documents for the work selected.
        if ($work_type_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($work_type_id) {
                $q->whereHas("workTypes", function ($q) use ($work_type_id) {
                    $q->where("id", $work_type_id);
                });
            });
        }

        // Search for a user based on their name.
        if ($sub_category_id) {
            $documents->whereHas("documentTemplate", function ($q) use ($sub_category_id) {
                $q->where("sub_category_id", $sub_category_id);
            });
        }

        $documents->where('documents.state','>=', Document::APPROVED);


        if($canListAllUsers || $name || $work_type_id || $sub_category_id){
            $documents = $documents->select('*', 'documents.id as document_id')->orderBy('id', 'desc')->paginate(12);
        }else{
            $documents =  $documents->select('*', 'documents.id as document_id')->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('documents.listMyDocuments', compact('documents', 'documentDateId') );

    }

    public function changeState(Request $request, Document $document){

        $document_id = (int) $request->has('document_id') ? $request->input('document_id'): null;
        $state = (int) $request->has('state') ? $request->input('state'): null;

        $document = $document->newQuery();

        //POLICIES
        $canChangeState = auth()->user()->can('changeState', Document::class );
        $canChangeStateToApproved = auth()->user()->can('changeStateToApproved', Document::class );
        $canChangeStateToPrinted = auth()->user()->can('changeStateToPrinted', Document::class );
        //END POLICIES

        // If there is not an ID of the document, we return and error code 3
        if (!$document_id) {
            return 3;
        }
        // If the user cannot change the state of the document, return error 4
        if (!$canChangeState){
            return 4;
        }
        // If the user cannot change the state of the document to approved, return error 5
        if ($state == Document::APPROVED && !$canChangeStateToApproved){
            return 5;
        }
        // If the user cannot change the state of the document to printed, return error 4
        if ($state == Document::PRINTED && !$canChangeStateToPrinted){
            return 6;
        }

        try {
            $document->where('id',$document_id)->update(['state' => $state]);
            $state_name = "Por revisar";
            switch ( (int) $state){
                case 1:
                    $state_name = "Por revisar";
                    break;
                case 2:
                    $state_name = "Revisado";
                    break;
                case 3:
                    $state_name = "Aprobado";
                    break;
                case 4:
                    $state_name = "Impreso";
                    break;
            }
            $revision = Revision::create(
                [
                    'comments'=>'Cambió estado a .'.$state_name,
                    'type'=>Revision::DOCUMENT,
                    'document_id'=>$document_id,
                    'user_id'=>auth()->user()->id
                ]);
            return 1;
        } catch(\Illuminate\Database\QueryException $e){
            return 2;
        }



    }

    public function store(Request $request, Document $document){
        $document_template_id = (int) $request->has('document_template_id_store') ? $request->input('document_template_id_store'): null;
        $document_date_id = (int) $request->has('document_date_id_store') ? $request->input('document_date_id_store'): null;

        $documentTemplate = DocumentTemplate::with([])->where('id', $document_template_id)->first();
        $documentDate = DocumentDate::with([])->where('id', $document_date_id)->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $documentDate );
        //END POLICIES

        if (!$canViewThis){
            return back()->with('message',['danger', __("Usted no puede agregar documentos a este lote.")]);
        }

        $document_date_formatted = Carbon::parse($documentDate->document_date)->format('d/m/Y');;


        $user =User::with('customer')->whereId($documentDate->user_belong_id)->first();

        //INITIALIZE GOOGLE VARIABLES
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);
        $sheetsService = $this->getGoogleServiceSheets($client);
        $slidesService = $this->getGoogleServiceSlides($client);


        try {
            $document->createDocument(
                $documentTemplate,
                $document_date_id,
                $driveService,
                $docsService,
                $sheetsService,
                $slidesService,
                $user,
                $document_date_formatted
            );
        }
        catch (\Throwable $t) {
            //if error, continue with the creation
        }

        $admins = User::where('role_id',Role::ADMIN)->get();

        $recipients = $admins;

        \Mail::to($recipients)->send( new ApproveDocumentMail() );

        return back()->with('message',['success',
            __("Se añadió correctamente el documento.")]);

    }

    public function ajaxStoreByWork(Request $request, Document $document){


        $work_type_id = (int) $request->has('work_type_id_store') ? $request->input('work_type_id_store'): null;
        $document_date_id = (int) $request->has('document_date_id_store') ? $request->input('document_date_id_store'): null;
        $randomTrackIdentifier = (int) $request->has('randomTrackIdentifier') ? $request->input('randomTrackIdentifier'): null;

        /*Fixing 524 Cloudfare error*/
        // sending a data.
        $spacer_size = 8; // increment me until it works
        echo str_pad('', (1024 * $spacer_size), "\n");
        // send 8kb of new line to browser (default), just make sure that this new line will not affect your code.
        /*Fixing 524 Cloudfare error*/

        $documentDate = DocumentDate::with([])->where('id', $document_date_id)->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $documentDate );
        //END POLICIES

        if (!$canViewThis){
            return 403;
        }

        $document_date_formatted = Carbon::parse($documentDate->document_date)->format('d/m/Y');;


        $user =User::with('customer')->whereId($documentDate->user_belong_id)->first();

        //INITIALIZE GOOGLE VARIABLES
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);
        $sheetsService = $this->getGoogleServiceSheets($client);
        $slidesService = $this->getGoogleServiceSlides($client);

        //LIST OF ALL DOCUMENT TEMPLATES
        $documentTemplates =  DocumentTemplate::with([])
            ->whereHas("workTypes", function ($q) use ($work_type_id) {
                $q->where("id", $work_type_id);
            })
            ->orderBy('id', 'desc')
            ->get();

        //CREATION VARIABLES
        $docsForCreation = count($documentTemplates);
        $docsCreated =0;
        $percentageCreated = 0;

        $tracking = Track::create(
            [
                'random_identifier'=>$randomTrackIdentifier,
                'completed'=>$docsCreated,
                'total'=>$docsForCreation,
                'document_date_id'=>$document_date_id,
            ]
        );

        foreach ($documentTemplates as $documentTemplate){
            try {
                $document->createDocument(
                    $documentTemplate,
                    $document_date_id,
                    $driveService,
                    $docsService,
                    $sheetsService,
                    $slidesService,
                    $user,
                    $document_date_formatted
                );
                $docsCreated++;

                $tracking->fill(['completed'=>$docsCreated])->save();
            }
            catch (\Throwable $t) {
                //if error, continue with the creation
            }
        }

        //FORCE THE COMPLETION TO CONTINUE TO NEXT PAGE
        $tracking->fill(['completed'=>$docsForCreation])->save();
        //echo route('documents.listCustomerFromDate', $document_date_id);

        $admins = User::where('role_id',Role::ADMIN)->get();

        $recipients = $admins;

        \Mail::to($recipients)->send( new ApproveDocumentMail() );

        return back()->with('message',['success',
            __("Se añadió correctamente el documento.")]);

    }

    public function ajaxStoreByCategory(Request $request, Document $document){


        $sub_category_id_store = (int) $request->has('sub_category_id_store') ? $request->input('sub_category_id_store'): null;
        $document_date_id = (int) $request->has('document_date_id_store') ? $request->input('document_date_id_store'): null;
        $randomTrackIdentifier = (int) $request->has('randomTrackIdentifier') ? $request->input('randomTrackIdentifier'): null;

        /*Fixing 524 Cloudfare error*/
        // sending a data.
        $spacer_size = 8; // increment me until it works
        echo str_pad('', (1024 * $spacer_size), "\n");
        // send 8kb of new line to browser (default), just make sure that this new line will not affect your code.
        /*Fixing 524 Cloudfare error*/

        $documentDate = DocumentDate::with([])->where('id', $document_date_id)->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $documentDate );
        //END POLICIES

        if (!$canViewThis){
            return 403;
        }

        $document_date_formatted = Carbon::parse($documentDate->document_date)->format('d/m/Y');;


        $user =User::with('customer')->whereId($documentDate->user_belong_id)->first();

        //INITIALIZE GOOGLE VARIABLES
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);
        $sheetsService = $this->getGoogleServiceSheets($client);
        $slidesService = $this->getGoogleServiceSlides($client);

        //LIST OF ALL DOCUMENT TEMPLATES
        $documentTemplates =  DocumentTemplate::with([])
            ->where("sub_category_id", $sub_category_id_store)
            ->orderBy('id', 'desc')
            ->get();

        //CREATION VARIABLES
        $docsForCreation = count($documentTemplates);
        $docsCreated =0;
        $percentageCreated = 0;

        $tracking = Track::create(
            [
                'random_identifier'=>$randomTrackIdentifier,
                'completed'=>$docsCreated,
                'total'=>$docsForCreation,
                'document_date_id'=>$document_date_id,
            ]
        );

        foreach ($documentTemplates as $documentTemplate){
            try {
                $document->createDocument(
                    $documentTemplate,
                    $document_date_id,
                    $driveService,
                    $docsService,
                    $sheetsService,
                    $slidesService,
                    $user,
                    $document_date_formatted
                );
                $docsCreated++;

                $tracking->fill(['completed'=>$docsCreated])->save();
            }
            catch (\Throwable $t) {
                //if error, continue with the creation
            }
        }

        //FORCE THE COMPLETION TO CONTINUE TO NEXT PAGE
        $tracking->fill(['completed'=>$docsForCreation])->save();
        //echo route('documents.listCustomerFromDate', $document_date_id);

        $admins = User::where('role_id',Role::ADMIN)->get();

        $recipients = $admins;

        \Mail::to($recipients)->send( new ApproveDocumentMail() );

        return back()->with('message',['success',
            __("Se añadió correctamente el documento.")]);

    }

    public function addDownloadDocument (Request $request, Document $document){
        $model_id = $request->has('model_id') ? $request->input('model_id'): null;

        //INITIALIZE GOOGLE VARIABLES
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);;
        $folderId = '1f3egIkCQ2WN3KTX9j6lvu91NLzX2rdcH';
        $name=request()->file('model_document')->getClientOriginalName();
        $fileMetadata = $this->getGoogleServiceDriveDriveFile($name, $folderId);

        $document = $document->newQuery();

        if($request->has('model_document')){
            $content = file_get_contents(request()->file('model_document')->getRealPath());

            $file = $this->uploadDocumentToDrive($driveService,$fileMetadata, $content);

            if ($file->id) {
                $document->whereId($model_id)->update([
                    'google_download_id' => $file->id
                ]);
                $revision = Revision::create(
                    [
                        'comments'=>'Añadió documento para impresión',
                        'type'=>Revision::DOCUMENT,
                        'document_id'=>$model_id,
                        'user_id'=>auth()->user()->id
                    ]
                );
                return back()->with('message',['success',
                    __("Se añadió el documento.")]);
            }else{
                return back()->with('message',['danger',
                    __("Ocurrió un error, por favor inténtelo de nuevo.")]);
            }

        }else{

            return back()->with('message',['danger',
                __("Por favor, seleccione un documento.")]);
        }
    }

    public function delete(Document $document){
        //POLICIES
        $canDelete = auth()->user()->can('delete', [Document::class] );
        //END POLICIES

        try {
            if ($canDelete){
                $document->delete();
                //dd($customerRequest);
                return back()->with('message',['success', __("Se eliminó el documento.")]);
            }else{
                $document->fill([
                    'state'=>Document::FOR_DELETION,
                    'user_deleted_id'=>auth()->user()->id,
                ])->save();

                $admins = User::where('role_id',Role::ADMIN)->get();

                $recipients = $admins;

                \Mail::to($recipients)->send( new ApproveDocumentDeletionMail( $document, auth()->user() ) );

                return back()->with('message',['success', __("Se solicitó al administrador eliminar el documento.")]);
            }

        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('message',['danger',
                __("Hubo un error eliminando el documento,
                por favor intente de nuevo.")]);


        }

    }

}
