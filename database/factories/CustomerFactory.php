<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        //'company_logo'=> \Faker\Provider\Image::image(storage_path(). '/app/public/companies',600,350,'business',false),
        'company_logo'=> 'company.png',
        'tax_number' => $faker->phoneNumber,
        'address' => $faker->address,
        'turn' => $faker->address,
        'workers' => $faker->numberBetween(1,500),
        'phone'=> $faker->phoneNumber,
        'cellphone'=> $faker->phoneNumber,
        'user_id'=>\App\User::all()->random()->id,
        'user_created_id'=>\App\User::all()->random()->id,
    ];
});
