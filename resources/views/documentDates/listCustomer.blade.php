@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.documentDates.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fecha de documentos</th>
                    <th scope="col">Estado de Proyeto</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Creó</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($documentDates as $documentDate)
                    @can('view', [\App\DocumentDate::class, $documentDate])
                    <tr>
                        <td>{{ $documentDate->id }}</td>
                        <td>{{ \Carbon\Carbon::parse($documentDate->document_date)->format('d/m/Y') }}</td>
                        <td>

                            <select
                                class="form-control document-date-state-change"
                                data-document="{{ $documentDate->id }}"
                                required
                                @cannot('changeState', [\App\Document::class] ) disabled  @endcannot
                            >
                                <option
                                    {{ (int) $documentDate->project_state=== \App\DocumentDate::PROJECT_ELABORATING ? 'selected' : '' }}
                                    value="{{ \App\DocumentDate::PROJECT_ELABORATING }}"
                                >En elaboración</option>
                                <option
                                    {{
                                        (int) $documentDate->project_state === \App\DocumentDate::PROJECT_ACTIVE ? 'selected' : '' }}
                                    value="{{ \App\DocumentDate::PROJECT_ACTIVE }}"
                                    @cannot('changeStateToActive', [\App\DocumentDate::class] ) disabled  @endcannot
                                >Activo</option>
                                <option
                                    {{ (int) $documentDate->project_state === \App\DocumentDate::PROJECT_TERMINATED ? 'selected' : '' }}
                                    value="{{ \App\DocumentDate::PROJECT_TERMINATED }}"
                                    @cannot('changeStateToTerminated', [\App\DocumentDate::class] ) disabled  @endcannot
                                >Terminado</option>
                            </select>
                        </td>
                        <td>{{ \Carbon\Carbon::parse($documentDate->created_at)->format('d/m/Y') }}</td>
                        <td>{{ $documentDate->userCreated->name }}</td>
                        <td>
                            @if(auth()->user()->id == 1)
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documentsTest.listCustomerFromDate', $documentDate->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver documentos Test"
                            >
                                <i class="fa fa-tint"></i>
                            </a>
                            @endif
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.listCustomerFromDate', $documentDate->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver documentos"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                        </td>
                    </tr>
                    @endcan
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalDocumentDate">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Nuevos documentos</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="modal-content"
                              method="POST"
                              action="{{ route('documentDates.store') }}"
                              novalidate
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="user_belong_id">Id de usuario cliente:</label>
                                <input type="number" class="form-control" name="user_belong_id" id="user_belong_id" value="{{ $userBelong }}">
                            </div>
                            <div class="form-group">
                                <label for="document_date">Día de documentos:</label>
                                <div class="input-group date" id="document_date_tp" data-target-input="nearest">
                                    <input
                                        type="text"
                                        name="document_date"
                                        id="document_date"
                                        class="form-control datetimepicker-input"
                                        data-target="#document_date_tp"
                                        data-toggle="datetimepicker"
                                    />
                                    <div class="input-group-append" data-target="#document_date_tp" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="tracking-wrapp">
                                <label id="tracking-label"></label>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info create-docs-btn">
                                    Agregar
                                </button>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            {{ $documentDates->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/jquery.session.js') }}"></script>
    <script>
        $(function () {
            $('#document_date_tp').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.create-docs-btn', function(){
            let user_belong_id = $("#user_belong_id").val();
            let document_date = $("#document_date").val();
            console.log("User belong:" + user_belong_id);
            console.log("Document Date:" + document_date);

            let randomTrackIdentifier = Math.floor((Math.random() * 1000000) + 1);//Random between 1 and 1'000.000

            console.log("identifier created: "+randomTrackIdentifier);

            $("#tracking-label").html("Creando documentos.");
            trackProgress(randomTrackIdentifier);
            createDocumentsAjax(user_belong_id, document_date,randomTrackIdentifier);
        });

        function createDocumentsAjax(user_belong_id, document_date,randomTrackIdentifier) {
            console.log("identifier creation: "+randomTrackIdentifier);
            //Enviamos una solicitud con el id del documento
            console.log("ajax sended");
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET,
                url: '{{ route('documentDates.ajaxStore') }}',
                async: true,
                cache:false,
                data: {
                    user_belong_id: user_belong_id,
                    document_date: document_date,
                    randomTrackIdentifier: randomTrackIdentifier,
                },
                success: function (data) {
                    console.log(data);
                    // similar behavior as clicking on a link
                    //window.location.href = data;

                },error:function(data){
                    $("#tracking-label").html("Hubo un error creando los documentos. Por favor inténtelo de nuevo.");
                }
            });
        }

        function trackProgress(randomTrackIdentifier) {
            console.log("identifier track: "+randomTrackIdentifier);
            //Enviamos una solicitud con el id del documento
            console.log("ajax sended");
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET,
                url: '{{ route('documentDates.progress') }}',
                dataType: "json",
                data: {
                    randomTrackIdentifier: randomTrackIdentifier,
                },
                success: function (data) {
                    console.log(data);
                    console.log("identifier: "+data.random_identifier);
                    console.log("completed: "+data.completed);
                    console.log("total: "+data.total);

                    if(data.completed < data.total){
                        $("#tracking-label").html("Creados "+data.completed +" documentos de "+data.total+".");
                        trackProgress(randomTrackIdentifier);
                    }else if(data.completed === undefined ){
                        trackProgress(randomTrackIdentifier);
                    }else if(data.completed === data.total){

                        $("#tracking-label").html("Se crearon los documentos");
                        console.log(data.document_date_id);
                        //WE RECEIVE THE URL PARSED FROM THE docCreationProgress
                        window.location.href = data.document_date_id;
                    }

                },error:function(data){
                    console.log(data);
                }
            });
        }



        $(document).on('change', '.document-date-state-change', function(){
            let document_date_id = $(this).attr("data-document");
            let state = $(this).val();

            console.log(document_date_id);
            console.log(state);

            changeStateFromDocumentDateSelected(document_date_id, state);
        });

        function changeStateFromDocumentDateSelected(document_date_id, state) {
            //Enviamos una solicitud con el id del documento
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('documentDates.changeState') }}',
                data: {
                    document_date_id: document_date_id,
                    state: state
                },
                success: function (data) {
                    switch (parseInt(data)) {
                        case 1:
                            console.log('Se actualizó el estado.');
                            break;
                        case 2:
                            console.log('Hubo un error actualizando el documento.');
                            break;
                        case 3:
                            console.log('No se ingresó un ID del documento.');
                            break;
                        case 4:
                            console.log('No puede cambiar el estado del documento.');
                            break;
                        case 5:
                            console.log('No puede cambiar el estado del documento a aprobado.');
                            break;
                        case 6:
                            console.log('No puede cambiar el estado del documento a impreso.');
                            break;
                        case 403:
                            alert("Usted no puede modificar documentos en este lote.");
                            location.reload();
                            break;


                    }


                },error:function(){
                    console.log(data);
                }
            });
        }
    </script>
@endpush
