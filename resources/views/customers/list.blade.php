@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.customers.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Información</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->company_name }}</td>
                        <td>{{ $customer->phone }}<br>{{ $customer->user->email }}</td>
                        <td>{!! $customer->user->name."<br> ".$customer->user->last_name !!}</td>
                        <td>
                            <div class="btn-group mb-2">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.admin', $customer->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Ver información del cliente"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.edit', $customer->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar información del cliente"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                            <br>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('documentDates.listCustomer', $customer->user->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Ver documentos"
                                >
                                    <i class="fa fa-bars"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('works.listCustomer', $customer->user->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Servicios brindados al cliente"
                                >
                                    <i class="fa fa-briefcase"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $customers->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
