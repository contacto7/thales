@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.documentTemplates.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">Documentos</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $subCategory=null;  @endphp
                @forelse($documents as $document)
                    @if($subCategory != $document->subCategory->name)
                        <tr>
                            <th colspan="3" class="table-row-subcategory">{{ $document->subCategory->name }}</th>
                        </tr>
                        @php $subCategory=$document->subCategory->name;  @endphp
                    @endif
                    <tr>
                        <td>{{ $document->name.$document->documentType->extension }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info edit-format"
                                href="{{ route('documentTemplates.editDocument', ['document_id'=>$document->id,'google_document_id'=>$document->google_document_id, 'google_name'=>$document->documentType->google_name]) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Editar documento"
                                target="_blank"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documentTemplates.downloadDocument', ['document_id'=>$document->id,"google_document_id"=>$document->google_document_id, "filename"=>$document->name]) }}"
                                target="_blank"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Imprimir documento"
                            >
                                <i class="fa fa-print"></i>
                            </a>
                            <a
                                class="btn btn-outline-info btn-revisions"
                                data-toggle="modal"
                                data-placement="top"
                                title="Historial de revisiones"
                                data-target="#modalRevisions"
                                href="#modalRevisions"
                                data-id="{{ $document->id }}"
                            >
                                <i class="fa fa-clock-o"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalRevisions">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Revisiones</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $documents->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        //Variable globar para guardar el id del cliente seleccionad
        let document_id_actual;
        let type = {{ \App\Revision::TEMPLATE }};

        /*
        * Función para cambiar el estado de un documento
         * cuando se cambio el selector del documento
        */


        $(function() {

            $("a.edit-formatd").on("click", function(e) {
                var link = this;
                e.preventDefault();

                console.log("hey")

                let confirmAction = confirm("Está seguro de querer modificar el formato?");

                if(confirmAction){
                    //window.location = link.href;
                    window.open(link.href);
                }
            });
        });

        /*
        * Función para ver las revisiones del documento
        */
        $(document).on('click', '.btn-revisions', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let document_id = $(this).attr('data-id');

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('revisions.modalSeeForm') }}',
                data: {
                    document_id: document_id,
                    type: type
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        document_id_actual = document_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.revisions-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&document_id="+document_id_actual+"&type="+type;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });



    </script>
@endpush
