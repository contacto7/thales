<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DocumentTemplate
 *
 * @property int $id
 * @property string $google_document_id
 * @property string $name
 * @property string $description
 * @property int $document_type_id
 * @property int $sub_category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\DocumentType $documentType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Document[] $documents
 * @property-read int|null $documents_count
 * @property-read \App\SubCategory $subCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereDocumentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereGoogleDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DocumentTemplate extends Model
{
    protected $fillable = [
        'google_document_id','name','description','document_type_id','sub_category_id',
    ];

    public function documentType(){
        return $this->belongsTo(DocumentType::class);
    }
    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }
    public function documents(){
        return $this->hasMany(Document::class);
    }
    public function workTypes(){
        return $this->belongsToMany(WorkType::class)->withTimestamps();
    }
}
