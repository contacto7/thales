<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name')->comment('nombre del rol del usuario');
            $table->text('description');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id')->default(\App\Role::CUSTOMER);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('name');
            $table->string('last_name');
            $table->string('slug');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('cellphone')->nullable();
            $table->string('position')->nullable();
            $table->enum('state', [
                \App\User::ACTIVE,
                \App\User::INACTIVE,
                \App\User::FOR_ACTIVATION
            ])->default(\App\User::ACTIVE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
