<li class="">
    <a href="#customersSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-building"></i>
        Clientes
    </a>
    <ul class="collapse list-unstyled" id="customersSubmenu">
        <li>
            <a href="{{ route('customers.list') }}">Listar</a>
        </li>
        <li>
            <a href="{{ route('customers.create') }}">Añadir</a>
        </li>
    </ul>
</li>

@include('partials.sidebar.logged')
