<div class="head-page">
    @include('partials.title', [
        'title' => __("Documentos"),
        'icon' => "files-o"
    ])
    <form action="{{ route('documentDates.customerSearch') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row d-none">
            <label
                for="user_belong_filter"
                class="col-sm-3 col-form-label"
            >
                Usuario de cliente
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="user_belong_filter"
                    id="user_belong_filter"
                    type="number"
                    value="{{ $userBelong }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="document_date_filter"
                class="col-sm-3 col-form-label"
            >
                Año de documentos
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="document_date_filter"
                    id="document_date_filter"
                    type="number"
                    min="1990"
                    max="2500"
                    step="1"
                    placeholder="{{ __("") }}"
                >
            </div>
        </div>
        <div class="form-group row d-none">
            <label
                for="user_created_id"
                class="col-sm-3 col-form-label"
            >
                Registrado por
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="user_created_id"
                    id="user_created_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\User::where('role_id', '!=', \App\Role::CUSTOMER)->get() as $user)
                        <option
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-thales-secondary"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>

</div>


