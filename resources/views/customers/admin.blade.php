@extends('layouts.modern', ['page' => 'admin'])
@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Empresa"),
                    'icon' => "building"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3 justify-content-center">
            <img
                src="{{ $customer->company_logo ? $customer->pathAttachment():url('image/company-logo.png') }}"
                class="company-logo"
            >
        </div>
        <div class="row mt-3 mb-3">
            <table class="table table-hover table-light">
                <tbody>
                <tr>
                    <th>EMPRESA</th>
                    <td>{{ $customer->company_name }}</td>
                </tr>
                <tr>
                    <th>LOGO</th>
                    <td>{{ $customer->company_logo }}</td>
                </tr>
                <tr>
                    <th>RUC</th>
                    <td>{{ $customer->tax_number }}</td>
                </tr>
                <tr>
                    <th>GIRO DE LA EMPRESA</th>
                    <td>{{ $customer->turn }}</td>
                </tr>
                <tr>
                    <th>NÚMERO DE TRABAJADORES</th>
                    <td>{{ $customer->workers }}</td>
                </tr>
                <tr>
                    <th>DIRECCIÓN</th>
                    <td>{{ $customer->address }}</td>
                </tr>
                <tr>
                    <th>TELÉFONO</th>
                    <td>{{ $customer->phone }}</td>
                </tr>
                <tr>
                    <th>CELULAR</th>
                    <td>{{ $customer->cellphone }}</td>
                </tr>
                <!-- USUARIO -->
                <tr>
                    <th>NOMBRE USUARIO:</th>
                    <td>{{ $customer->user->name." ".$customer->user->last_name }}</td>
                </tr>
                <tr>
                    <th>CORREO DE USUARIO</th>
                    <td>{{ $customer->user->email }}</td>
                </tr>
                <tr>
                    <th>CELULAR DE USUARIO</th>
                    <td>{{ $customer->user->cellphone }}</td>
                </tr>
                <tr>
                    <th>CARGO DE USUARIO</th>
                    <td>{{ $customer->user->position }}</td>
                </tr>
                <tr>
                    <th>ESTADO DE USUARIO</th>
                    <td>
                        @switch($customer->user->state)
                            @case(\App\User::ACTIVE)
                                Activo
                            @break
                            @case(\App\User::INACTIVE)
                                Inactivo
                            @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <th>SERVICIOS</th>
                    <td>
                        @foreach(\App\Work::where('user_id',$customer->user->id)->get() as $work)
                            <div class="comma-separated-wrapp">
                                {{ $work->workType->name }}<span class="comma-separated">,</span>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>REGISTRÓ</th>
                    <td>{{ $customer->userCreated->name." ".$customer->userCreated->last_name }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
