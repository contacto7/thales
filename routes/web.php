<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/countCustomersForApprove','GeneralController@countCustomersForApprove')
    ->name('countCustomersForApprove');
Route::get('/countDocumentDatesForApprove','GeneralController@countDocumentDatesForApprove')
    ->name('countDocumentDatesForApprove');
Route::get('/countDocumentsForApprove','GeneralController@countDocumentsForApprove')
    ->name('countDocumentsForApprove');
Route::get('/countDocumentsForApproveDelete','GeneralController@countDocumentsForApproveDelete')
    ->name('countDocumentsForApproveDelete');

Route::group(['middleware' => ['auth']], function (){

    Route::group(['prefix' => 'users'], function (){
        Route::get('/list','UserController@list')
            ->name('users.list');
        Route::get('/admin/{id}','UserController@admin')
            ->name('users.admin');
        Route::get('/search','UserController@filter')
            ->name('users.search');
        Route::get('/help','UserController@help')
            ->name('users.help');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/create','UserController@create')
                ->name('users.create');
            Route::post('/store','UserController@store')
                ->name('users.store');
            Route::get('/edit/{id}','UserController@edit')
                ->name('users.edit');
            Route::put('/update/{user}','UserController@update')
                ->name('users.update');
        });
    });
    Route::group(['prefix' => 'customers'], function (){
        Route::get('/list','CustomerController@list')
            ->name('customers.list');
        Route::get('/admin/{id}','CustomerController@admin')
            ->name('customers.admin');
        Route::get('/search','CustomerController@filter')
            ->name('customers.search');
        Route::get('/create','CustomerController@create')
            ->name('customers.create');
        Route::post('/store','CustomerController@store')
            ->name('customers.store');
        Route::get('/edit/{id}','CustomerController@edit')
            ->name('customers.edit');
        Route::put('/update/{user}/{customer}','CustomerController@update')
            ->name('customers.update');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/listForApproving','CustomerController@listForApproving')
                ->name('customers.listForApproving');
            Route::get('/approve/{customer}','CustomerController@approve')
                ->name('customers.approve');
        });
    });
    Route::group(['prefix' => 'documents'], function (){
        Route::get('/list','DocumentController@list')
            ->name('documents.list');
        Route::get('/listCustomer/{id}','DocumentController@listCustomer')
            ->name('documents.listCustomer');
        Route::get('/listCustomerFromDate/{id}','DocumentController@listCustomerFromDate')
            ->name('documents.listCustomerFromDate');
        Route::get('/listMyDocuments/{id?}','DocumentController@listMyDocuments')
            ->name('documents.listMyDocuments');
        Route::get('/editDocument/{document_id}/{google_document_id}/{google_name}','DocumentController@editDocument')
            ->name('documents.editDocument');
        Route::get('/downloadDocument/{document_id}/{google_document_id}/{filename}','DocumentController@downloadDocument')
            ->name('documents.downloadDocument');
        Route::get('/downloadDocumentAddedForPrint/{document_id}/{google_document_id}/{filename}','DocumentController@downloadDocumentAddedForPrint')
            ->name('documents.downloadDocumentAddedForPrint');
        Route::get('/admin/{id}','DocumentController@admin')
            ->name('documents.admin');
        Route::get('/search','DocumentController@filter')
            ->name('documents.search');
        Route::get('/customerSearch','DocumentController@customerFilter')
            ->name('documents.customerSearch');
        Route::get('/create','DocumentController@create')
            ->name('documents.create');
        Route::post('/store','DocumentController@store')
            ->name('documents.store');
        Route::get('/ajaxStoreByWork','DocumentController@ajaxStoreByWork')
            ->name('documents.ajaxStoreByWork');
        Route::get('/ajaxStoreByCategory','DocumentController@ajaxStoreByCategory')
            ->name('documents.ajaxStoreByCategory');
        Route::post('/addDownloadDocument','DocumentController@addDownloadDocument')
            ->name('documents.addDownloadDocument');
        Route::get('/changeState','DocumentController@changeState')
            ->name('documents.changeState');
        Route::get('/delete/{document}','DocumentController@delete')
            ->name('documents.delete');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/listForApproving','DocumentController@listForApproving')
                ->name('documents.listForApproving');
            Route::get('/listForApproveDelete','DocumentController@listForApproveDelete')
                ->name('documents.listForApproveDelete');
            Route::get('/approveDelete/{id}','DocumentController@approveDelete')
                ->name('documents.approveDelete');
            Route::get('/adminSearch','DocumentController@adminFilter')
                ->name('documents.adminSearch');
        });
    });
    Route::group(['prefix' => 'document-templates'], function (){
        Route::get('/list','DocumentTemplateController@list')
            ->name('documentTemplates.list');
        Route::get('/editDocument/{document_id}/{google_document_id}/{google_name}','DocumentTemplateController@editDocument')
            ->name('documentTemplates.editDocument');
        Route::get('/downloadDocument/{document_id}/{google_document_id}/{filename}','DocumentTemplateController@downloadDocument')
            ->name('documentTemplates.downloadDocument');
        Route::get('/admin/{id}','DocumentTemplateController@admin')
            ->name('documentTemplates.admin');
        Route::get('/search','DocumentTemplateController@filter')
            ->name('documentTemplates.search');
        Route::get('/create','DocumentTemplateController@create')
            ->name('documentTemplates.create');
    });
    Route::group(['prefix' => 'document-dates'], function (){
        Route::get('/listCustomer/{id}','DocumentDateController@listCustomer')
            ->name('documentDates.listCustomer');
        Route::get('/listMyHistoricDocuments','DocumentDateController@listMyHistoricDocuments')
            ->name('documentDates.listMyHistoricDocuments');
        Route::get('/search','DocumentDateController@filter')
            ->name('documentDates.search');
        Route::get('/customerSearch','DocumentDateController@customerFilter')
            ->name('documentDates.customerSearch');
        Route::get('/create','DocumentDateController@create')
            ->name('documentDates.create');
        Route::post('/store','DocumentDateController@store')
            ->name('documentDates.store');
        Route::get('/ajaxStore','DocumentDateController@ajaxStore')
            ->name('documentDates.ajaxStore');
        Route::get('/progress','DocumentDateController@progress')
            ->name('documentDates.progress');
        Route::get('/search','DocumentDateController@filter')
            ->name('documentDates.search');
        Route::get('/changeState','DocumentDateController@changeState')
            ->name('documentDates.changeState');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/listForApproving','DocumentDateController@listForApproving')
                ->name('documentDates.listForApproving');
            Route::get('/approve/{documentDate}','DocumentDateController@approve')
                ->name('documentDates.approve');
        });
    });
    Route::group(['prefix' => 'works'], function (){
        Route::get('/listCustomer/{id}','WorkController@listCustomer')
            ->name('works.listCustomer');
        Route::get('/create','WorkController@create')
            ->name('works.create');
        Route::post('/store','WorkController@store')
            ->name('works.store');
        Route::get('/edit/{id}','WorkController@edit')
            ->name('works.edit');
        Route::put('/update/{work}','WorkController@update')
            ->name('works.update');
        Route::delete('/delete/{work}','WorkController@delete')
            ->name('works.delete');
    });
    Route::group(['prefix' => 'revisions'], function (){
        Route::get('/list-from-document/{id}','RevisionController@listFromDocument')
            ->name('revisions.listFromDocument');
        Route::get('/modalSeeForm','RevisionController@modalSeeForm')
            ->name('revisions.modalSeeForm');
    });

    //TEST ROUTES//
    Route::group(['prefix' => 'documentsTest'], function (){
        Route::get('/list','DocumentController@list')
            ->name('documentsTest.list');
        Route::get('/listCustomer/{id}','DocumentTestController@listCustomer')
            ->name('documentsTest.listCustomer');
        Route::get('/listCustomerFromDate/{id}','DocumentTestController@listCustomerFromDate')
            ->name('documentsTest.listCustomerFromDate');
        Route::get('/updateFromTemplate','DocumentTestController@updateFromTemplate')
            ->name('documentsTest.updateFromTemplate');
        Route::get('/quickstart','DocumentTestController@quickstart')
            ->name('documentsTest.quickstart');
        Route::get('/docsTest','DocumentTestController@docsTest')
            ->name('documentsTest.docsTest');
        Route::get('/sheetsTest','DocumentTestController@sheetsTest')
            ->name('documentsTest.sheetsTest');
        Route::get('/slidesTest','DocumentTestController@slidesTest')
            ->name('documentsTest.slidesTest');
        Route::get('/updateFromTemplateTest/{id}','DocumentTestController@updateFromTemplateTest')
            ->name('documentsTest.updateFromTemplateTest');
        Route::get('/editDocument/{document_id}/{google_document_id}/{google_name}','DocumentTestController@editDocument')
            ->name('documentsTest.editDocument');
        Route::get('/downloadDocument/{document_id}/{google_document_id}/{filename}','DocumentTestController@downloadDocument')
            ->name('documentsTest.downloadDocument');
    });
    //TEST ROUTES//

});

Route::get('images/{path}/{attachment}', function ($path, $attachment){
    $file = sprintf('storage/%s/%s', $path, $attachment);

    if(File::exists($file)){
        return \Intervention\Image\Facades\Image::make($file)->response();
    }else{
        return \Intervention\Image\Facades\Image::make('image/not-found.png')->response();
    }
});
