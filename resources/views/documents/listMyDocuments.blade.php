@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.documents.customerSearch')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">Documentos</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $subCategory=null;  @endphp
                @forelse($documents as $document)
                    @if($subCategory != $document->documentTemplate->subCategory->name)
                        <tr>
                            <th colspan="3" class="table-row-subcategory">{{ $document->documentTemplate->subCategory->name }}</th>
                        </tr>
                        @php $subCategory=$document->documentTemplate->subCategory->name;  @endphp
                    @endif
                    <tr>
                        <td>
                            {{ $document->name.$document->documentTemplate->documentType->extension }}
                        </td>
                        <td>
                            @can('printDocument', [\App\Document::class, $document])
                                <a
                                    class="btn btn-outline-info"
                                    @if($document->google_download_id)
                                    href="{{ route('documents.downloadDocumentAddedForPrint', ['document_id'=>$document->document_id, "google_document_id"=>$document->google_download_id, "filename"=>$document->name]) }}"
                                    @else
                                    href="{{ route('documents.downloadDocument', ['document_id'=>$document->document_id, "google_document_id"=>$document->google_document_id, "filename"=>$document->name]) }}"
                                    @endif

                                    target="_blank"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Imprimir documento"
                                >
                                    <i class="fa fa-print"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalRevisions">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Revisiones</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>
        <!-- Modal para añadir documentos -->
        <!-- The Modal -->
        <div class="modal" id="modalDocumentAdd">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Añadir documento</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('documents.store') }}"
                              novalidate
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="document_date_id_store">Document Date Id:</label>
                                <input type="number" class="form-control" name="document_date_id_store" id="document_date_id_store" value="{{ $documentDateId }}">
                            </div>
                            <div class="form-group">
                                <label for="document_template_id_store">Documento:</label>
                                <select
                                    class="form-control"
                                    name="document_template_id_store"
                                    id="document_template_id_store"
                                >
                                    @foreach(\App\DocumentTemplate::pluck('name','id') as $id => $name)
                                        <option
                                            value="{{ $id }}"
                                        >{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="tracking-wrapp">
                                <label id="tracking-label"></label>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info create-docs-btn">
                                    Agregar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>
        <!-- Modal para agregar -->
        <!-- The Modal -->
        <div class="modal" id="modalEdit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Añadir documento de descarga</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('documents.addDownloadDocument') }}"
                              enctype="multipart/form-data"
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="model_id">Id de modelo:</label>
                                <input type="number" class="form-control" name="model_id" id="model_id" required>
                            </div>
                            <div class="form-group">
                                <label for="">Documento</label>
                                <div class="col-md-12">
                                    <input
                                        type="file"
                                        class="custom-file-input form-control {{ $errors->has('model_document') ? 'is-invalid': '' }}"
                                        name="model_document"
                                        id="model_document"
                                        placeholder=""
                                        value=""
                                    >
                                    <label  class="custom-file-label" for="model_document">
                                        Buscar archivo
                                    </label>
                                    @if($errors->has('model_document'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('model_document') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Agregar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $documents->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        let document_id_actual;
        /*
        * Función para cambiar el estado de un documento
         * cuando se cambio el selector del documento
        */
        $(document).on('change', '.document-state-change', function(){
            let document_id = $(this).attr("data-document");
            let state = $(this).val();

            console.log(document_id);
            console.log(state);

            changeStateFromDocumentSelected(document_id, state);
        });

        function changeStateFromDocumentSelected(document_id, state) {
            //Enviamos una solicitud con el id del documento
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('documents.changeState') }}',
                data: {
                    document_id: document_id,
                    state: state
                },
                success: function (data) {
                    switch (parseInt(data)) {
                        case 1:
                            console.log('Se actualizó el estado.');
                            break;
                        case 2:
                            console.log('Hubo un error actualizando el documento.');
                            break;
                        case 3:
                            console.log('No se ingresó un ID del documento.');
                            break;
                        case 4:
                            console.log('No puede cambiar el estado del documento.');
                            break;
                        case 5:
                            console.log('No puede cambiar el estado del documento a aprobado.');
                            break;
                        case 6:
                            console.log('No puede cambiar el estado del documento a impreso.');
                            break;
                    }


                },error:function(){
                    console.log(data);
                }
            });
        }

        /*
        * Función para ver las revisiones del documento
        */
        $(document).on('click', '.btn-revisions', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let document_id = $(this).attr('data-id');
            let type = {{ \App\Revision::DOCUMENT }};

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('revisions.modalSeeForm') }}',
                data: {
                    document_id: document_id,
                    type: type
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        document_id_actual = document_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.revisions-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&document_id="+document_id_actual;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });

        $(document).on('click', '.create-docs-btn', function(){
            $("#tracking-label").html("Creando documento.");
        });
        /*
        * Función para poner el id del documento
        * en el modal
        */
        $(document).on('click', '.editDocument', function(){
            let data_id = $(this).data('id');
            let data_name = $(this).data('name');

            console.log(data_id);
            console.log(data_name);

            $("#model_id").val(data_id);
            $("#model_name").val(data_name);
        });
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
