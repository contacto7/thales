@component('mail::message')
# DOCUMENTO ELIMINADO:

<div>
    El usuario {{ $userDeleted->name." ".$userDeleted->last_name }} ha eliminado el documento
    <a href="{{ route('documents.editDocument', ['document_id'=>$document->id, 'google_document_id'=>$document->google_document_id, 'google_name'=>$document->documentTemplate->documentType->google_name]) }}"
    >{{ $document->name }}</a>:
</div>
@component('mail::button', ['url' => route('documents.approveDelete', $document->id) ])
    Eliminar
@endcomponent

Gracias,<br>

{{ env("APP_NAME", "ThalesCorp") }}
@endcomponent
