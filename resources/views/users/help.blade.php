@extends('layouts.modern', ['page' => 'admin'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Ayuda"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3 d-flex justify-content-center">
            @switch($user->role_id)
                @case(1)
                <iframe
                    width="600"
                    height="400"
                    src="https://www.youtube-nocookie.com/embed/Rgw9kTT8nPY"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                >
                </iframe>
                @break
                @case(2)
                <iframe
                    width="600"
                    height="400"
                    src="https://www.youtube-nocookie.com/embed/omofTbRpFGI"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                >
                </iframe>
                @break
                @case(3)
                <iframe
                    width="600"
                    height="400"
                    src="https://www.youtube-nocookie.com/embed/2xqlu7s5jLE"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                >
                </iframe>
                @break
                @case(4)
                <iframe
                    width="600"
                    height="400"
                    src="https://www.youtube-nocookie.com/embed/jaadZpm-iz0"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                >
                </iframe>
                @break
                @case(5)
                <iframe
                    width="600"
                    height="400"
                    src="https://www.youtube-nocookie.com/embed/c5bADeNA1fY"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                >
                </iframe>
                @break
            @endswitch
        </div>
    </div>
@endsection
