<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentTemplatePolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::PROJECT_COORDINATOR;
    }
}
