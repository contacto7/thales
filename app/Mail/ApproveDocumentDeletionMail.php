<?php

namespace App\Mail;

use App\Document;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApproveDocumentDeletionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Document $document, User $userDeleted)
    {
        $this->document = $document;
        $this->user_deleted = $userDeleted;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Documento Eliminado")
            ->markdown("emails.approve_document_deletion_mail")
            ->with('document', $this->document)
            ->with('userDeleted', $this->user_deleted);
    }
}
