<?php
//require __DIR__ . '/vendor/autoload.php';
define('STDIN',fopen("php://stdin","r"));
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */

function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Docs API PHP Quickstart');
    $client->setScopes(Google_Service_Docs::DOCUMENTS);
    $client->setScopes(Google_Service_Slides::PRESENTATIONS);
    $client->setScopes(Google_Service_Docs::DRIVE);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');

    //HESER
    /*
        $authUrl = $client->createAuthUrl();
        dd($authUrl);
    */
    //HESER

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory('token.json');
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode("4/vwEcc5a6Qd7_mc8c8yLvinVbAK5eYKvswMBu5prSRU3WYgCdwQzIX6w");

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path)
{
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

/*COPIAR UN DOCUMENTO DESDE UN TEMPLATE*/
function copyFromTemplate($templateDocumentId, $copyTitle, $driveService){
    $copy = new Google_Service_Drive_DriveFile(array(
        'name' => $copyTitle
    ));
    $driveResponse = $driveService->files->copy($templateDocumentId, $copy);
    $documentCopyId = $driveResponse->id;

    return $documentCopyId;
}

/*ACTUALIZAR UN DOCUMENTO*/
function updateDocument($requests, $documentId, $service){
    $batchUpdateRequest = new Google_Service_Slides_BatchUpdatePresentationRequest(array(
        'requests' => $requests
    ));

    $response = $service->presentations->batchUpdate($documentId, $batchUpdateRequest);

    return $response;
}

/*LEER ELEMENTOS ESTRUCTURALES*/
function readParagraphElement($element){
    $text_run = $element['textRun'];
    if(!$text_run){
        return '';
    }
    return $text_run['content'];
}

/*LEER ELEMENTOS ESTRUCTURALES*/
function readStructuralElements($elements){

    $text = '';

    foreach ($elements as $value) {
        $value = (array) $value;

        //dd($value);

        if (array_key_exists('paragraph',$value)){
            $elements = $value['paragraph']['elements'];
            foreach ($elements as $elem) {
                $text.=readParagraphElement($elem);
            }
        }elseif (array_key_exists('table',$value)){
            # The text in table cells are in nested Structural Elements and tables may be
            # nested.
            $table = $value['table'];
            foreach ($table['tableRows'] as $row) {
                $cells = $row['tableCells'];

                foreach ($cells as $cell) {
                    $text.=readStructuralElements($cell['content']);
                }
            }
        }elseif (array_key_exists('tableOfContents',$value)){
            # The text in the TOC is also in a Structural Element.
            $toc = $value['tableOfContents'];
            $text .= readStructuralElements($toc['content']);
        }
    }

    return $text;
}

function deleteText($text, $startIndex, $segmentId = null){
    $textLength = strlen($text);
    $endIndex = ($startIndex + $textLength);

    $range = array(
        'startIndex' => $startIndex,
        'endIndex' => $endIndex
    );

    //SI MODIFICAMOS UN HEADER O FOOTER
    if ($segmentId){
        $range['segmentId'] = $segmentId;
    }

    $request = new Google_Service_Docs_Request(array(
        'deleteContentRange' => array(
            'range' => $range,
        ),
    ));

    return $request;
}

function insertImage($imageURL, $index, $height, $width, $segmentId = null){
    $location = array(
        'index' => $index,
    );

    //SI MODIFICAMOS UN HEADER O FOOTER
    if ($segmentId){
        $location['segmentId'] = $segmentId;
    }

    $request = new Google_Service_Docs_Request(array(
        'insertInlineImage' => array(
            'uri' => $imageURL,
            'location' => $location,
            'objectSize' => array(
                'height' => array(
                    'magnitude' => $height,
                    'unit' => 'PT',
                ),
                'width' => array(
                    'magnitude' => $width,
                    'unit' => 'PT',
                ),
            )
        )
    ));

    return $request;
}

/*ENONTRAR TEXTO AL LEER ELEMENTOS ESTRUCTURALES*/
function getTexPositionByReadingStructuralElements($elements, $segmentId = null){

}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Slides($client);
$driveService = new Google_Service_Drive($client);

// https://docs.google.com/document/d/195j9eDD3ccgjQRttHhJPymLJUCOUjs-jmwTrekvdjFE/edit
//$documentId = '1hlVFO4YCNESMxWvWHy44BiqE3838N2d28rCfEBZbZRk';

$documentId = '1sit4OfWRQgz4pDGbgtG2uRXYmkLTq-LBeyOv2j9oOuE';

$doc = $service->presentations->get($documentId);
$copyTitle = "Aca debería estar el titulo del documento - Empresa";
//$copyTitle = 'Copy - '.$doc->getTitle();
//printf("The document title is: %s\n", $doc->getTitle());


//PRIMERO: COPIAMOS EL DOCUMENTO
$documentCopyId = copyFromTemplate($documentId, $copyTitle, $driveService);

$slides = $service->presentations->get($documentCopyId)->getSlides();

$requests = array();

foreach ($slides as $slide){

    $pageElements = $slide['pageElements'];
    foreach ($pageElements as $pageElement){


        if(isset($pageElement["shape"]["text"]["textElements"])){
            $textElements = $pageElement["shape"]["text"]["textElements"];

            foreach ($textElements as $textElement){

                if( strpos($textElement["textRun"]["content"], '#LOGO') !== false  ){

                    $pageObjectId = $slide["objectId"];
                    $textRectangleObjectId = $pageElement["objectId"];
                    $size = $pageElement["size"];
                    $transform = $pageElement["transform"];
                    $imageUrl = "https://inoloop.com/img/Logo-morado.png";

                    $requests[]=new Google_Service_Slides_Request(array(
                        'createImage' => array(
                            'url' => $imageUrl,
                            'elementProperties' => array(
                                'pageObjectId' => $pageObjectId,
                                'size' => $size,
                                'transform' => $transform
                            )
                        )
                    ));

                    $requests[]=new Google_Service_Slides_Request(array(
                        'deleteObject' => array(
                            'objectId' =>$textRectangleObjectId
                        )
                    ));



                }
            }
        }


    }
}



//TERCERO: ACTUALIZAMOS EL DOCUMENTO CREADO DEL TEMPLATE
$responseFromUpdate =  updateDocument($requests, $documentCopyId, $service);


//dd( $responseFromUpdate );


$document_edit_url = "https://docs.google.com/presentation/d/".$documentCopyId."/edit";
header('Location: '.$document_edit_url);
exit();
