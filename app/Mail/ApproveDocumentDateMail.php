<?php

namespace App\Mail;

use App\DocumentDate;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApproveDocumentDateMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(DocumentDate $documentDate, User $UserCreated)
    {
        $this->document_date = $documentDate;
        $this->user_created = $UserCreated;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Nuevo lote de documentos registrado")
            ->markdown("emails.approve_document_date_mail")
            ->with('documentDate', $this->document_date)
            ->with('userCreated', $this->user_created);
    }
}
