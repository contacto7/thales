<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    //USER
                    'role_id' => 'nullable',
                    'name' => 'required',
                    'last_name' => 'required',
                    'slug' => 'nullable',
                    'email' => 'required|email',
                    'password' => 'required|min:6',
                    'cellphone' => 'nullable',
                    'position' => 'nullable',
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ]),
                    'remember_token' => 'nullable',
                    //CUSTOMER
                    'company_name' => 'required|min: 3',
                    'company_logo' => 'nullable',
                    'tax_number' => 'required|min: 5',
                    'address' => 'nullable',
                    'turn' => 'nullable',
                    'phone' => 'nullable',
                ];
            case 'PUT':
                return [
                    //USER
                    'role_id' => 'nullable',
                    'name' => 'required',
                    'last_name' => 'required',
                    'slug' => 'nullable',
                    'email' => 'required|email',
                    'password' => 'nullable',
                    'cellphone' => 'nullable',
                    'position' => 'nullable',
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ]),
                    'remember_token' => 'nullable',
                    //CUSTOMER
                    'company_name' => 'required|min: 3',
                    'company_logo' => 'nullable',
                    'tax_number' => 'required|min: 5',
                    'address' => 'nullable',
                    'turn' => 'nullable',
                    'phone' => 'nullable',

                ];
        }
    }





}
