<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Track;
use Faker\Generator as Faker;

$factory->define(Track::class, function (Faker $faker) {
    return [
        'random_identifier'=> $faker->numberBetween(1,2),
        'completed'=> $faker->numberBetween(1,2),
        'total'=> $faker->numberBetween(2,50),
        'document_date_id'=>\App\Role::all()->random()->id,
    ];
});
