<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('document_date');
            $table->unsignedBigInteger('user_created_id');
            $table->foreign('user_created_id')->references('id')->on('users');
            $table->unsignedBigInteger('user_belong_id');
            $table->foreign('user_belong_id')->references('id')->on('users');
            $table->enum('state', [
                \App\DocumentDate::FOR_ACTIVATION,
                \App\DocumentDate::ACTIVE
            ])->default(\App\DocumentDate::ACTIVE);
            $table->enum('project_state', [
                \App\DocumentDate::PROJECT_ELABORATING,
                \App\DocumentDate::PROJECT_ACTIVE,
                \App\DocumentDate::PROJECT_TERMINATED,
            ])->default(\App\DocumentDate::PROJECT_ELABORATING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_dates');
    }
}
