<?php
//require __DIR__ . '/vendor/autoload.php';
define('STDIN',fopen("php://stdin","r"));
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */

function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Docs API PHP Quickstart');
    $client->setScopes(Google_Service_Docs::DOCUMENTS);
    $client->setScopes(Google_Service_Docs::DRIVE);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');

    //HESER
    /*
        $authUrl = $client->createAuthUrl();
        dd($authUrl);
    */
    //HESER

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory('token.json');
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode("4/vwEcc5a6Qd7_mc8c8yLvinVbAK5eYKvswMBu5prSRU3WYgCdwQzIX6w");

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path)
{
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

/*COPIAR UN DOCUMENTO DESDE UN TEMPLATE*/
function copyFromTemplate($templateDocumentId, $copyTitle, $driveService){
    $copy = new Google_Service_Drive_DriveFile(array(
        'name' => $copyTitle
    ));
    $driveResponse = $driveService->files->copy($templateDocumentId, $copy);
    $documentCopyId = $driveResponse->id;

    return $documentCopyId;
}

/*ACTUALIZAR UN DOCUMENTO*/
function updateDocument($requests, $documentId, $service){
    $batchUpdateRequest = new Google_Service_Docs_BatchUpdateDocumentRequest(array(
        'requests' => $requests
    ));

    $response = $service->documents->batchUpdate($documentId, $batchUpdateRequest);

    return $response;
}

/*LEER ELEMENTOS ESTRUCTURALES*/
function readParagraphElement($element){
    $text_run = $element['textRun'];
    if(!$text_run){
        return '';
    }
    return $text_run['content'];
}

/*LEER ELEMENTOS ESTRUCTURALES*/
function readStructuralElements($elements){

    $text = '';

    foreach ($elements as $value) {
        $value = (array) $value;

        //dd($value);

        if (array_key_exists('paragraph',$value)){
            $elements = $value['paragraph']['elements'];
            foreach ($elements as $elem) {
                $text.=readParagraphElement($elem);
            }
        }elseif (array_key_exists('table',$value)){
            # The text in table cells are in nested Structural Elements and tables may be
            # nested.
            $table = $value['table'];
            foreach ($table['tableRows'] as $row) {
                $cells = $row['tableCells'];

                foreach ($cells as $cell) {
                    $text.=readStructuralElements($cell['content']);
                }
            }
        }elseif (array_key_exists('tableOfContents',$value)){
            # The text in the TOC is also in a Structural Element.
            $toc = $value['tableOfContents'];
            $text .= readStructuralElements($toc['content']);
        }
    }

    return $text;
}

function deleteText($text, $startIndex, $segmentId = null){
    $textLength = strlen($text);
    $endIndex = ($startIndex + $textLength);

    $range = array(
        'startIndex' => $startIndex,
        'endIndex' => $endIndex
    );

    //SI MODIFICAMOS UN HEADER O FOOTER
    if ($segmentId){
        $range['segmentId'] = $segmentId;
    }

    $request = new Google_Service_Docs_Request(array(
        'deleteContentRange' => array(
            'range' => $range,
        ),
    ));

    return $request;
}

function insertImage($imageURL, $index, $height, $width, $segmentId = null){
    $location = array(
        'index' => $index,
    );

    //SI MODIFICAMOS UN HEADER O FOOTER
    if ($segmentId){
        $location['segmentId'] = $segmentId;
    }

    $request = new Google_Service_Docs_Request(array(
        'insertInlineImage' => array(
            'uri' => $imageURL,
            'location' => $location,
            'objectSize' => array(
                'height' => array(
                    'magnitude' => $height,
                    'unit' => 'PT',
                ),
                'width' => array(
                    'magnitude' => $width,
                    'unit' => 'PT',
                ),
            )
        )
    ));

    return $request;
}

/*ENONTRAR TEXTO AL LEER ELEMENTOS ESTRUCTURALES*/
function getTexPositionByReadingStructuralElements($elements, $segmentId = null){
    $startIndexOfText = 0;
    $requests = array();

    foreach ($elements as $value) {
        $value = (array) $value;

        //dd($value);

        if (array_key_exists('paragraph',$value)){
            $elements = $value['paragraph']['elements'];
            foreach ($elements as $elem) {
                $textInElement = readParagraphElement($elem);

                $pos = strpos($textInElement, '##LOGO##');

                if ($pos !== false) {
                    $startIndexOfText = $elem['startIndex']+$pos;
                    $imageURL = "https://inoloop.com/img/Logo-morado.png";
                    /*
                     INSERTAMOS AL PRINCIPIO PARA QUE SE ELIMINEN PRIMERO
                    LOS ÚLTIMOS Y QUE NO SE CAMBIE LA POSICIÓN DE LOS PRIMEROS
                    */
                    array_unshift($requests,insertImage($imageURL, $startIndexOfText, 50, 50, $segmentId));
                    array_unshift($requests,deleteText('##LOGO##', $startIndexOfText,$segmentId));

                    //deleteText('##LOGO##', $startIndexOfText);
                    //dd("Position: ".$pos.". Lenght: ".strlen($text));

                    //POSITION = STARTINDEX + $POS
                }
            }
        }elseif (array_key_exists('table',$value)){
            # The text in table cells are in nested Structural Elements and tables may be
            # nested.
            $table = $value['table'];
            foreach ($table['tableRows'] as $row) {
                $cells = $row['tableCells'];

                foreach ($cells as $cell) {
                    getTexPositionByReadingStructuralElements($cell['content']);
                }
            }
        }elseif (array_key_exists('tableOfContents',$value)){
            # The text in the TOC is also in a Structural Element.
            $toc = $value['tableOfContents'];
            getTexPositionByReadingStructuralElements($toc['content']);
        }
    }

    return $requests;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Docs($client);
$driveService = new Google_Service_Drive($client);

// https://docs.google.com/document/d/195j9eDD3ccgjQRttHhJPymLJUCOUjs-jmwTrekvdjFE/edit
//$documentId = '1hlVFO4YCNESMxWvWHy44BiqE3838N2d28rCfEBZbZRk';

$documentId = '1mi_wTKZSoBQcrHr5MH-kB77Gz5KskYHkplpIuc8lm98';

$doc = $service->documents->get($documentId);
$copyTitle = "Aca debería estar el titulo del documento - Empresa";
//$copyTitle = 'Copy - '.$doc->getTitle();
//printf("The document title is: %s\n", $doc->getTitle());


//PRIMERO: COPIAMOS EL DOCUMENTO
$documentCopyId = copyFromTemplate($documentId, $copyTitle, $driveService);

//SEGUNDO: DEFINIMOS LO QUE SE ACTUALIZARÁ
$requests = array();
$deleteRequests = array();
$deleteRequestsHeaders = array();
$deleteRequestsFooters = array();

$docContent = $doc->getBody()->getContent();
$docHeaders = $doc->getHeaders();
$docFooters = $doc->getFooters();

foreach ($docHeaders as $docHeader) {
    $docHeaderContent = $docHeader['content'];
    $deleteRequestsHeaders = getTexPositionByReadingStructuralElements($docHeaderContent, $docHeader['headerId']);
    foreach ($deleteRequestsHeaders as $deleteRequestsHeader) {
        $requests[] = $deleteRequestsHeader;
    }
}
foreach ($docFooters as $docFooter) {
    $docFooterContent = $docFooter['content'];
    $deleteRequestsFooters = getTexPositionByReadingStructuralElements($docFooterContent, $docFooter['footerId']);
    foreach ($deleteRequestsFooters as $deleteRequestsFooter) {
        $requests[] = $deleteRequestsFooter;
    }
}

//$startIndexOfText = getTexPositionByReadingStructuralElements($docContent);

$deleteRequests = getTexPositionByReadingStructuralElements($docContent);

foreach ($deleteRequests as $deleteRequest) {
    $requests[] = $deleteRequest;
}

$requests[] = new Google_Service_Docs_Request(array(
    'replaceAllText' => array(
        'containsText' => array(
            'text' => "#NOMBRE",
            'matchCase' => true,
        ),
        'replaceText' => "GIANCARLA",
    ),
));
$requests[] = new Google_Service_Docs_Request(array(
    'replaceAllText' => array(
        'containsText' => array(
            'text' => "#FECHA",
            'matchCase' => true,
        ),
        'replaceText' => "23/01/2020",
    ),
));
/*
$requests[] = new Google_Service_Docs_Request(array(
    'insertInlineImage' => array(
        'uri' => 'https://www.gstatic.com/images/branding/product/1x/docs_64dp.png',
        'location' => array(
            'index' => 1,
        ),
        'objectSize' => array(
            'height' => array(
                'magnitude' => 50,
                'unit' => 'PT',
            ),
            'width' => array(
                'magnitude' => 50,
                'unit' => 'PT',
            ),
        )
    )
));
*/





//TERCERO: ACTUALIZAMOS EL DOCUMENTO CREADO DEL TEMPLATE
$responseFromUpdate =  updateDocument($requests, $documentCopyId, $service);


//dd( $responseFromUpdate );


$document_edit_url = "https://docs.google.com/document/d/".$documentCopyId."/edit";
header('Location: '.$document_edit_url);
exit();



/*END HESER*/

