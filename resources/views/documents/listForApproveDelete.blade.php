@extends('layouts.modern')

@section('content')
    <div class="row">
        <div class="head-page">
            @include('partials.title', [
                'title' => __("Aprobar eliminación de documentos"),
                'icon' => "check"
            ])
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center pt-3 mt-4">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Eliminó</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($documents as $document)
                    <tr>
                        <td>{{ $document->id }}</td>
                        <td>

                            <a
                                href="{{ route('documents.editDocument', ['document_id'=>$document->id, 'google_document_id'=>$document->google_document_id, 'google_name'=>$document->documentTemplate->documentType->google_name]) }}"
                                target="_blank"
                            >
                                {{ $document->name }}
                            </a>
                        </td>
                        <td>

                            <a
                                class=""
                                href="{{ route('customers.admin', $document->documentDate->userBelong->customer->id) }}"
                            >
                                {{ $document->documentDate->userBelong->customer->company_name }}
                            </a>
                        </td>
                        <td>{{ $document->userDeleted->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.approveDelete', $document->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Eliminar"
                            >
                                <i class="fa fa-times"></i>
                            </a>
                            <a
                                class="btn btn-outline-info btn-revisions"
                                data-toggle="modal"
                                data-placement="top"
                                title="Historial de revisiones"
                                data-target="#modalRevisions"
                                href="#modalRevisions"
                                data-id="{{ $document->id }}"
                            >
                                <i class="fa fa-clock-o"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalRevisions">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Revisiones</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            {{ $documents->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/jquery.session.js') }}"></script>
    <script>
        /*
        * Función para ver las revisiones del documento
        */
        $(document).on('click', '.btn-revisions', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let document_id = $(this).attr('data-id');
            let type = {{ \App\Revision::DOCUMENT }};

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('revisions.modalSeeForm') }}',
                data: {
                    document_id: document_id,
                    type: type
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        document_id_actual = document_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.revisions-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&document_id="+document_id_actual;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });

    </script>
@endpush
