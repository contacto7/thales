<div class="head-page">
    @include('partials.title', [
        'title' => __("Documentos"),
        'icon' => "files-o"
    ])
    <form action="{{ route('documents.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row d-none">
            <label
                for="document_date_id"
                class="col-sm-3 col-form-label"
            >
                Documento
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="document_date_id"
                    id="document_date_id"
                    type="number"
                    value="{{ $documentDateId }}"
                    placeholder="{{ __("Document Date Id") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <div
                class="col-sm-3"
            >
                <label
                    for="name"
                    class="col-form-label"
                >
                    Documento
                </label>
                <a
                    class="form-control btn btn-thales-secondary btn-add-icon"
                    data-toggle="modal"
                    data-target="#modalDocumentAdd"
                    href="#modalDocumentAdd"
                >
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Ingrese el nombre del documento") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <div
                class="col-sm-3"
            >
                <label
                    for="work_type_id"
                    class="col-form-label"
                >
                    Por servicio
                </label>
                <a
                    class="form-control btn btn-thales-secondary btn-add-icon"
                    data-toggle="modal"
                    data-target="#modalDocumentAddByWork"
                    href="#modalDocumentAddByWork"
                >
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="work_type_id"
                    id="work_type_id"
                >
                    <option value="">Seleccionar</option>
                    @php $userBelongId = \App\DocumentDate::where('id',$documentDateId)->first()->user_belong_id; @endphp;
                    @foreach(
                        \App\WorkType::whereHas('works', function ($q) use ($userBelongId){
                            $q->where("user_id", $userBelongId);
                        })->get() as $workType)
                        <option
                            value="{{ $workType->id }}"
                        >{{ $workType->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div
                class="col-sm-3"
            >
                <label
                    for="sub_category_id"
                    class="col-form-label"
                >
                    Categoría
                </label>
                <a
                    class="form-control btn btn-thales-secondary btn-add-icon"
                    data-toggle="modal"
                    data-target="#modalDocumentAddByCategory"
                    href="#modalDocumentAddByCategory"
                >
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="sub_category_id"
                    id="sub_category_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\SubCategory::documentDateAvailable($documentDateId) as $subCategory)
                        <option
                            value="{{ $subCategory->id }}"
                        >{{ $subCategory->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label
                for="state"
                class="col-sm-3 col-form-label"
            >
                Estado
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="state"
                    id="state"
                >
                    <option value="">Seleccionar</option>
                    <option value="{{ \App\Document::FOR_REVISION }}">Por revisar</option>
                    <option value="{{ \App\Document::REVISED }}">Revisado</option>
                    <option value="{{ \App\Document::APPROVED }}">Aprobado</option>
                    <option value="{{ \App\Document::PRINTED }}">Impreso</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-thales-secondary"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>
