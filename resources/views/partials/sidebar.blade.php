<!--
Sidebar
Example of: https://bootstrapious.com/p/bootstrap-sidebar
-->
<nav id="sidebar">
    <a class="sidebar-header" href="{{ url('/') }}">
        <img
            src="{{ asset('image/logo-white.svg') }}"
             alt="Logo de Thales"
             class="logo-thales logo-complete">
        <img
            src="{{ asset('image/logo-white-partial.png') }}"
             alt="Logo de Thales"
             class="logo-thales logo-partial">
    </a>

    <ul class="list-unstyled components">
        @include('partials.sidebar.' . \App\User::navigation())
    </ul>

</nav>
