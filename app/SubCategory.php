<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SubCategory
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DocumentTemplate[] $documentTemplates
 * @property-read int|null $document_templates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubCategory extends Model
{
    protected $fillable = [
        'name','description','category_id',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function documentTemplates(){
        return $this->hasMany(DocumentTemplate::class);
    }
    public static function documentDateAvailable($documentDateId){
        $subCategories = SubCategory::whereHas("documentTemplates", function ($q) use ($documentDateId) {
            $q->whereHas("documents", function ($q) use ($documentDateId) {
                $q->whereHas("documentDate", function ($q) use ($documentDateId) {
                    $q->where("id", $documentDateId);
                });
            });
        })->orderBy('id','desc')->get();

        return $subCategories;
    }
}
