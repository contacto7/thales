@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Aprobar lote de documentos creados"),
                    'icon' => "check"
                ])
            </div>
        </div>
        <div class="row justify-content-center pt-3 mt-4">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fecha de documentos</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Creó</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($documentDates as $documentDate)
                    <tr>
                        <td>{{ $documentDate->id }}</td>
                        <td>{{ \Carbon\Carbon::parse($documentDate->document_date)->format('d/m/Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($documentDate->created_at)->format('d/m/Y') }}</td>
                        <td>

                            <a
                                class=""
                                href="{{ route('customers.admin', $documentDate->userBelong->customer->id) }}"
                            >
                                {{ $documentDate->userBelong->customer->company_name }}
                            </a>
                        </td>
                        <td>{{ $documentDate->userCreated->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documentDates.approve', $documentDate->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Aprobar"
                            >
                                <i class="fa fa-check"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.listCustomerFromDate', $documentDate->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver documentos"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {{ $documentDates->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/jquery.session.js') }}"></script>
@endpush
