@extends('layouts.modern')

@section('content')
    <div class="container">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Aprobar clientes creados"),
                    'icon' => "check"
                ])
            </div>
        </div>
        <div class="row justify-content-center pt-3 mt-4">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Información</th>
                    <th scope="col">Agregó</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->company_name }}</td>
                        <td>{{ $customer->phone }}<br>{{ $customer->user->email }}</td>
                        <td>{!! $customer->user->name."<br> ".$customer->user->last_name !!}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.admin', $customer->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.approve', $customer->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Aprobar"
                                >
                                    <i class="fa fa-check"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $customers->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
