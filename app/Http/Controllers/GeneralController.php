<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Document;
use App\DocumentDate;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function countCustomersForApprove(){
        $count = Customer::with([])
            ->whereHas("user", function($sq) {
                $sq->where('role_id', Role::CUSTOMER);
                $sq->where('state', User::FOR_ACTIVATION);
            })
            ->count();

        return $count;
    }
    public function countDocumentDatesForApprove(){
        $count = DocumentDate::with([])
            ->where('state', DocumentDate::FOR_ACTIVATION)
            ->count();

        return $count;
    }
    public function countDocumentsForApprove(){
        $count = Document::with([])
            ->where('state', '<', Document::APPROVED)
            ->count();

        return $count;
    }
    public function countDocumentsForApproveDelete(){
        $count = Document::with([])
            ->where('state', Document::FOR_DELETION)
            ->count();

        return $count;
    }
}
