<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Revision
 *
 * @property int $id
 * @property string|null $comments
 * @property int $document_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Document $document
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Revision whereUserId($value)
 * @mixin \Eloquent
 */
class Revision extends Model
{
    const DOCUMENT = 1;
    const TEMPLATE = 2;

    protected $fillable = [
        'comments', 'type', 'document_id', 'user_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function document(){
        return $this->belongsTo(Document::class);
    }
}
