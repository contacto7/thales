@component('mail::message')
# NUEVO LOTE DE DOCUMENTOS REGISTRADO:

<div>El usuario {{ $userCreated->name." ".$userCreated->last_name }} ha registrado un <a href="{{ route('documents.listCustomerFromDate', $documentDate->id) }}">nuevo lote
de documentos</a> para el cliente <a href="{{ route('customers.admin', $documentDate->userBelong->customer->id) }}">{{ $documentDate->userBelong->customer->company_name }}</a>:
</div>

@component('mail::button', ['url' => route('documentDates.approve', $documentDate->id) ])
    Aprobar lote
@endcomponent

Recuerde también aprobar los  <a href="{{ route('documents.listCustomerFromDate', $documentDate->id) }}">documentos del lote</a>.

Gracias,<br>

{{ env("APP_NAME", "ThalesCorp") }}
@endcomponent
