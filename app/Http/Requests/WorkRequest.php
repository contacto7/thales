<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'cost' => 'nullable',
                    'user_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'work_type_id' => [
                        'required',
                        Rule::exists('work_types','id')
                    ],

                ];
            }
            case 'PUT':{
                return [
                    'cost' => 'nullable',

                ];



            }
        }
    }








}
