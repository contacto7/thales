@component('mail::message')
# DOCUMENTOS POR APROBAR:

<div>
    Se han creado nuevos documentos por aprobar.
</div>

@component('mail::button', ['url' => route('documents.listForApproving') ])
    Ver documentos por aprobar
@endcomponent

Gracias,<br>

{{ env("APP_NAME", "ThalesCorp") }}
@endcomponent
