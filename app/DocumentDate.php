<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentDate extends Model
{
    const FOR_ACTIVATION = 1;
    const ACTIVE = 2;
    const PROJECT_ELABORATING = 1;
    const PROJECT_ACTIVE = 2;
    const PROJECT_TERMINATED = 3;

    protected $fillable = [
        'document_date','user_created_id','user_belong_id','state','project_state',
    ];

    public function userCreated(){
        return $this->belongsTo(User::class, 'user_created_id');
    }
    public function userBelong(){
        return $this->belongsTo(User::class, 'user_belong_id');
    }
    public function documents(){
        return $this->hasMany(User::class, 'user_belong_id');
    }
    public function tracks(){
        return $this->hasMany(Track::class);
    }
}
