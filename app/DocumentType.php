<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DocumentType
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $extension
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DocumentTemplate[] $documentTemplates
 * @property-read int|null $document_templates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereName($value)
 * @mixin \Eloquent
 */
class DocumentType extends Model
{
    const DOCS = 1;
    const SHEETS = 2;
    const SLIDES = 3;

    public $timestamps = false;

    protected $fillable = [
        'name','google_name','extension',
    ];

    public function documentTemplates(){
        return $this->hasMany(DocumentTemplate::class);
    }

}
