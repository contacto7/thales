<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js" integrity="sha256-aS5HnZXPFUnMTBhNEiZ+fKMsekyUqwm30faj/Qh/gIA=" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

@stack('scripts')

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  rel="stylesheet" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.css" integrity="sha256-U6YrSIhyGybBdtKMg2+iJsIbyHtNY3Yj9gacnUG2jNo=" crossorigin="anonymous" />
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    @stack('styles')
</head>
<body>


<div class="wrapp d-flex">
    @auth
        @include('partials.sidebar')
        <!-- Page Content  -->
    @endauth
    @yield('jumbotron')
    <div id="modern">
        @auth
            <button type="button" id="sidebarCollapse" class="btn btn-thales-primary">
                <i class="fa fa-bars"></i>
            </button>
        @endauth
        <main
            class="main-content page-@php echo isset($page) ? $page : 'default'; @endphp"
        >
            @if(session('message'))
                <div class="row justify-content-center text-center">
                    <div class="col-md-10">
                        <div class="alert alert-{{ session('message')[0] }} alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <h4 class="alert-heading">{{ __("Mensaje informativo") }}</h4>
                            <p>{{ session('message')[1] }}</p>
                        </div>
                    </div>
                </div>
            @endif
            @yield('title')
            @yield('content')
        </main>
    </div>
    @include('partials.modals.modalMensaje')

    @include('partials.footer')
</div>
<script>
    $(function () {
        let total_count_approve = 0;
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({html:true});
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

        @auth
        @if(auth()->user()->role_id == \App\Role::ADMIN)
        /*OBTENER CUENTA DE SOLICITUDES DE APROBACION*/
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET,
            url: '{{ route('countCustomersForApprove') }}',
            success: function (data) {
                console.log("Clientes por aprobar: "+ data);
                total_count_approve += parseInt(data);
                $(".count-customer").html("("+data+")");
                /*CUENTA TOTAL*/
                $(".count-total").html("("+total_count_approve+")");
            }
        });
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET,
            url: '{{ route('countDocumentDatesForApprove') }}',
            success: function (data) {
                console.log("Lote de documentos por aprobar: "+data);
                total_count_approve += parseInt(data);
                $(".count-doc-dates").html("("+data+")");
                /*CUENTA TOTAL*/
                $(".count-total").html("("+total_count_approve+")");
            }
        });
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET,
            url: '{{ route('countDocumentsForApprove') }}',
            success: function (data) {
                console.log("Documentos por aprobar: "+data);
                total_count_approve += parseInt(data);
                $(".count-docs-created").html("("+data+")");
                /*CUENTA TOTAL*/
                $(".count-total").html("("+total_count_approve+")");
            }
        });
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET,
            url: '{{ route('countDocumentsForApproveDelete') }}',
            success: function (data) {
                console.log("Documentos por eliminar: "+data);
                total_count_approve += parseInt(data);
                $(".count-docs-eliminated").html("("+data+")");
                /*CUENTA TOTAL*/
                $(".count-total").html("("+total_count_approve+")");
            }
        });
        @endif
        @endauth

        /*OBTENER CUENTA DE SOLICITUDES DE APROBACION*/



    });
</script>
</body>
</html>
