<?php

namespace App\Policies;

use App\DocumentDate;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentDatePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any document dates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the document date.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentDate  $documentDate
     * @return mixed
     */
    public function view(User $user, DocumentDate $documentDate)
    {
        $isCustomer = $user->role_id === Role::CUSTOMER;
        return  ! $isCustomer || ( $isCustomer && ( $documentDate->state == \App\DocumentDate::ACTIVE) );
    }

    /**
     * Determine whether the user can view only their customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function viewOnlyTheirs(User $user)
    {
        return  $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can view the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function viewThis(User $user, DocumentDate $documentDate)
    {
        $viewAll = $user->role_id === Role::ADMIN || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::EXTERNAL_CONSULTANT;
        return  $viewAll || ( (!$viewAll ) && ( $documentDate->user_created_id == $user->id) );
    }

    /**
     * Determine whether the user can create document dates.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN;
    }

    /**
     * Determine whether the user can update the document date.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentDate  $documentDate
     * @return mixed
     */
    public function update(User $user, DocumentDate $documentDate)
    {
        //
    }

    /**
     * Determine whether the user can delete the document date.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentDate  $documentDate
     * @return mixed
     */
    public function delete(User $user, DocumentDate $documentDate)
    {
        //
    }

    /**
     * Determine whether the user can restore the document date.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentDate  $documentDate
     * @return mixed
     */
    public function restore(User $user, DocumentDate $documentDate)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the document date.
     *
     * @param  \App\User  $user
     * @param  \App\DocumentDate  $documentDate
     * @return mixed
     */
    public function forceDelete(User $user, DocumentDate $documentDate)
    {
        //
    }


    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeState(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeStateToActive(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeStateToTerminated(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }


}
