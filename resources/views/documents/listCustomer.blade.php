@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Documentos"),
        'icon' => "files-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.users.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Real</th>
                    <th scope="col">Punt. Real</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($documents as $document)
                    <tr>
                        <td>{{ $document->id }}</td>
                        <td>{{ $document->name }}</td>
                        <td>
                            {{ $document->probability * $document->impact }}
                        </td>
                        <td>{{ $document->probability * $document->impact }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.updateFromTemplate') }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Crear documento"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('documents.updateFromTemplateTest', $document->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Crear documento test"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('users.edit', $document->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Editar documento"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay documentos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $documents->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
