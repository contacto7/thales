<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\WorkType
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Work[] $works
 * @property-read int|null $works_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkType whereName($value)
 * @mixin \Eloquent
 */
class WorkType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name','description',
    ];

    public function works(){
        return $this->hasMany(Work::class);
    }
    public function documentTemplates(){
        return $this->belongsToMany(DocumentTemplate::class)->withTimestamps();
    }

}
