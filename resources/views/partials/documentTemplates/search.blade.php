<div class="head-page">
    @include('partials.title', [
        'title' => __("Formatos"),
        'icon' => "files-o"
    ])
    <form action="{{ route('documentTemplates.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Documento
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Ingrese el nombre del documento") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="work_type_id"
                class="col-sm-3 col-form-label"
            >
                Por servicio
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="work_type_id"
                    id="work_type_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\WorkType::pluck('name','id') as $id => $name)
                        <option
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label
                for="sub_category_id"
                class="col-sm-3 col-form-label"
            >
                Categoría
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="sub_category_id"
                    id="sub_category_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\SubCategory::pluck('name','id') as $id => $name)
                        <option
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-thales-secondary"
                    name="filter"
                    type="submit"
                    value="buscar"
                >
            </div>
        </div>
    </form>
</div>
