@extends('layouts.modern', ['page' => 'admin'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Usuario"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <table class="table table-hover table-light">
                <tbody>
                <tr>
                    <th>NOMBRES</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th>APELLIDOS</th>
                    <td>{{ $user->last_name }}</td>
                </tr>
                <tr>
                    <th>CORREO</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>CELULAR</th>
                    <td>{{ $user->cellphone }}</td>
                </tr>
                <tr>
                    <th>PUESTO</th>
                    <td>{{ $user->position }}</td>
                </tr>
                <tr>
                    <th>CREADO</th>
                    <td>{{ $user->created_at }}</td>
                </tr>
                <tr>
                    <th>MODIFICADO</th>
                    <td>{{ $user->updated_at }}</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
