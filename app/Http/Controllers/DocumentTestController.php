<?php

namespace App\Http\Controllers;

use App\Document;
use App\DocumentType;
use App\Track;
use Illuminate\Http\Request;
use App\Traits\GoogleApi;

class DocumentTestController extends Controller
{
    use GoogleApi;
    public function listCustomerFromDate($id){
        $documents = Document::with([])
            ->orderBy('id', 'desc')
            ->where('document_date_id', $id)
            ->paginate(12);

        //dd($vouchers);
        return view('documentsTest.listCustomerFromDate', compact('documents') );
    }
    public function updateFromTemplate(){
        //EJEMPLO PRIMERO
        return view('documentsTest.updateFromTemplate' );
    }
    public function quickstart(){
        //EJEMPLO PRIMERO
        return view('documentsTest.quickstart' );
    }
    public function docsTest(){
        //////TRY RETRY/////
        /// https://stackoverflow.com/questions/25002164/php-try-catch-and-retry

        $NUM_OF_ATTEMPTS = 10;
        $attempts = 0;
        $tracking = Track::create(
            [
                'random_identifier'=>123,
                'completed'=>$attempts,
                'total'=>$NUM_OF_ATTEMPTS,
                'document_date_id'=>1,
            ]
        );
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);

        //VARIABLES
        //$documentId = '1V3hGlaLrs5au2rNVYeTK0D2oUss67AgPYqb5R3LrvKk';
        $documentId = '1hijJJ12pwo-ZMn5pMhw9RvqZRLyDpLfgKRKqL7z8Sx8';
        $documentCopyId="";
        $copyTitle = "Aca debería estar el titulo del documento - Empresa";
        $imgUrl = "https://inoloop.com/img/Logo-morado.png";
        $documentType = DocumentType::DOCS;
        $google_name = "document";
        $nombres = "EL FABRI";
        $fecha = "24/01/2020";
        $empresa = "INOLOOP SAC";
        //VARIABLES

        $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);

        $document  = $this->getDocumentByType($docsService, $documentCopyId, $documentType);

        $requests = array();
        $requestsReplaceImage = $this->replaceImageByType($imgUrl,Document::LOGO, $documentType, $document);
        if (is_array($requestsReplaceImage)){
            $requests = array_merge($requests, $requestsReplaceImage);
        }else{
            $requests[] = $requestsReplaceImage;
        }
        $requests = array_reverse($requests);

        $requests[] = $this->replaceTextByType(Document::LOGO, "", $documentType);
        $requests[] = $this->replaceTextByType(Document::NOMBRES, $nombres, $documentType);
        $requests[] = $this->replaceTextByType(Document::FECHA, $fecha, $documentType);
        $requests[] = $this->replaceTextByType(Document::EMPRESA, $empresa, $documentType);


        //dd($requests);

        $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $docsService, $documentType);

        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$documentCopyId."/edit";

        return \Redirect::to($document_edit_url);
        var_dump($document_edit_url);

    }
    public function docsTest2(){
        //////TRY RETRY/////
        /// https://stackoverflow.com/questions/25002164/php-try-catch-and-retry

        $NUM_OF_ATTEMPTS = 10;
        $attempts = 0;
        $tracking = Track::create(
            [
                'random_identifier'=>123,
                'completed'=>$attempts,
                'total'=>$NUM_OF_ATTEMPTS,
                'document_date_id'=>1,
            ]
        );
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);

        //VARIABLES
        $documentId = '1V3hGlaLrs5au2rNVYeTK0D2oUss67AgPYqb5R3LrvKk';
        $documentCopyId="";
        $copyTitle = "Aca debería estar el titulo del documento - Empresa";
        $imgUrl = "https://inoloop.com/img/Logo-morado.png";
        $documentType = DocumentType::DOCS;
        $google_name = "document";
        $nombres = "EL FABRI";
        $fecha = "24/01/2020";
        $empresa = "INOLOOP SAC";
        //VARIABLES

        do {
            try
            {
                $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);
                $tracking->fill(['random_identifier'=>$documentCopyId])->save();
            } catch (\Exception $e) {
                //Exponential backoffs
                //https://stackoverflow.com/questions/36101162/google-drive-api-uploaded-file-but-return-500-error
                //https://developers.google.com/drive/api/v3/handle-errors#exponential-backoff
                $attempts++;
                $tracking->fill(['random_identifier'=>"error-en-drive".$attempts])->save();
                sleep(1+1*$attempts);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);

        $document  = $this->getDocumentByType($docsService, $documentCopyId, $documentType);


        $requests = array();
        $requestsReplaceImage = $this->replaceImageByType($imgUrl,Document::LOGO, $documentType, $document);
        $tracking->fill(['random_identifier'=>"docs-2"])->save();
        if (is_array($requestsReplaceImage)){
            $requests = array_merge($requests, $requestsReplaceImage);
        }else{
            $requests[] = $requestsReplaceImage;
        }
        $requests[] = $this->replaceTextByType(Document::NOMBRES, $nombres, $documentType);
        $requests[] = $this->replaceTextByType(Document::FECHA, $fecha, $documentType);
        $requests[] = $this->replaceTextByType(Document::EMPRESA, $empresa, $documentType);

        $attempts = 0;
        $tracking->fill(['random_identifier'=>"continuo-a-actualizar-docs"])->save();
        do {
            try
            {
                $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $docsService, $documentType);
            } catch (\Exception $e) {
                $attempts++;
                $tracking->fill(['random_identifier'=>"error-actualizando-doc"])->save();
                sleep(1+1*$attempts);
                continue;
            }
            break;
        } while($attempts < $NUM_OF_ATTEMPTS);



        $tracking->fill(['completed'=>1])->save();
        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$documentCopyId."/edit";

        //return \Redirect::to($document_edit_url);
        var_dump($document_edit_url);

    }
    public function sheetsTest(){
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceSheets($client);

        //VARIABLES
        $documentId = '12FxCow7DqyifA2vWYYVdblhzeRP0ac_24U_IlZfDi9I';
        $copyTitle = "Aca debería estar el titulo del documento - Empresa";
        $imgUrl = "https://inoloop.com/img/Logo-morado.png";
        $documentType = DocumentType::SHEETS;
        $google_name = "spreadsheets";
        $nombres = "EL FABRI";
        $fecha = "24/01/2020";
        $empresa = "INOLOOP SAC";
        //VARIABLES

        $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);
        $document  = $this->getDocumentByType($docsService, $documentCopyId, $documentType);

        $requests = array();

        $requestsReplaceImage = $this->replaceImageByType($imgUrl,"#LOGO", $documentType, $document);

        if (is_array($requestsReplaceImage)){
            $requests = array_merge($requests, $requestsReplaceImage);
        }else{
            $requests[] = $requestsReplaceImage;
        }

        $requests[] = $this->replaceTextByType(Document::NOMBRES, "EL FABRI", $documentType);
        $requests[] = $this->replaceTextByType(Document::FECHA, "24/01/2020", $documentType);
        $requests[] = $this->replaceTextByType(Document::EMPRESA, "INOLOOP SAC", $documentType);

        $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $docsService, $documentType);

        //dd( $responseFromUpdate );
        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$documentCopyId."/edit";

        return \Redirect::to($document_edit_url);
    }
    public function slidesTest(){
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceSlides($client);

        //VARIABLES
        $documentId = '1sit4OfWRQgz4pDGbgtG2uRXYmkLTq-LBeyOv2j9oOuE';
        $copyTitle = "Aca debería estar el titulo del documento - Empresa";
        $imgUrl = "https://inoloop.com/img/Logo-morado.png";
        $documentType = DocumentType::SLIDES;
        $google_name = "presentation";
        $nombres = "EL FABRI";
        $fecha = "24/01/2020";
        $empresa = "INOLOOP SAC";
        //VARIABLES


        $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);
        $document  = $this->getDocumentByType($docsService, $documentCopyId, $documentType);

        $requests = array();
        $requestsReplaceImage = $this->replaceImageByType($imgUrl, "#LOGO", $documentType, $document);

        if (is_array($requestsReplaceImage)){
            $requests = array_merge($requests, $requestsReplaceImage);
        }else{
            $requests[] = $requestsReplaceImage;
        }

        $requests[] = $this->replaceTextByType(Document::NOMBRES, "EL FABRI", $documentType);
        $requests[] = $this->replaceTextByType(Document::FECHA, "24/01/2020", $documentType);
        $requests[] = $this->replaceTextByType(Document::EMPRESA, "INOLOOP SAC", $documentType);

        $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $docsService, $documentType);

        //dd( $responseFromUpdate );
        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$documentCopyId."/edit";

        return \Redirect::to($document_edit_url);
    }
    public function updateFromTemplateTest($id){
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);

        //COPIA DEL DOCUMENTO
        $documentId = '1hlVFO4YCNESMxWvWHy44BiqE3838N2d28rCfEBZbZRk';
        $copyTitle = "Aca debería estar el titulo del documento - Empresa";
        $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);

        $imgUrl = "https://inoloop.com/img/Logo-morado.png";
        $documentType = DocumentType::DOCS;
        $document  = $docsService->documents->get($documentCopyId);

        //dd($documentCopyId);
        //SEGUNDO: DEFINIMOS LO QUE SE ACTUALIZARÁ
        $requests = array();
        $requestsReplaceImage = $this->replaceImageByType($imgUrl,"##LOGO##", $documentType, $document);
        foreach ($requestsReplaceImage as $requestReplaceImage) {
            $requests[] = $requestReplaceImage;
        }
        $requests[] = $this->replaceDocsText("#NOMBRE", "EL FABRI");
        $requests[] = $this->replaceDocsText("#FECHA", "24/01/2020");

        $responseFromUpdate =  $this->updateDocument($requests, $documentCopyId, $docsService);

        //dd( $responseFromUpdate );
        $document_edit_url = "https://docs.google.com/document/d/".$documentCopyId."/edit";

        return \Redirect::to($document_edit_url);

        //return view('documents.updateFromTemplate' );
    }
    public function editDocument($google_document_id, $google_name){
        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$google_document_id."/edit";

        return \Redirect::to($document_edit_url);

        //return view('documents.updateFromTemplate' );
    }
    public function downloadDocument($google_document_id, $filename){
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);

        //DESCARGAMOS EL DOCUMENTO
        $responseD = $this->downloadDocumentFromDrive($google_document_id, $driveService, $filename);

        return $responseD;

        //return view('documents.updateFromTemplate' );
    }
}
