<?php

namespace App\Http\Controllers;

use App\DocumentTemplate;
use App\Revision;
use App\Traits\GoogleApi;
use Illuminate\Http\Request;

class DocumentTemplateController extends Controller
{
    use GoogleApi;

    public function list(){
        //POLICIES
        $canView = auth()->user()->can('view', DocumentTemplate::class );
        //POLICIES

        if (!$canView){
            return "Usted no puede ver los formatos.";
        }

        $documents = DocumentTemplate::with([])
            ->orderBy('sub_category_id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('documentTemplates.list', compact('documents') );
    }
    public function editDocument($document_id, $google_document_id, $google_name){

        $revision = Revision::create(
            [
                'comments'=>'Editó documento',
                'type'=>Revision::TEMPLATE,
                'document_id'=>$document_id,
                'user_id'=>auth()->user()->id
            ]
        );

        $document_edit_url = "https://docs.google.com/".$google_name."/d/".$google_document_id."/edit";

        return \Redirect::to($document_edit_url);

        //return view('documents.updateFromTemplate' );
    }
    public function downloadDocument($document_id, $google_document_id, $filename){
        $revision = Revision::create(
            [
                'comments'=>'Imprimió documento',
                'type'=>Revision::TEMPLATE,
                'document_id'=>$document_id,
                'user_id'=>auth()->user()->id
            ]
        );

        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);

        //DESCARGAMOS EL DOCUMENTO
        $responseD = $this->downloadDocumentFromDrive($google_document_id, $driveService, $filename);

        return $responseD;

        //return view('documents.updateFromTemplate' );
    }
    public function filter(Request $request, DocumentTemplate $documents){
        //POLICIES
        $canView = auth()->user()->can('view', DocumentTemplate::class );
        //POLICIES

        if (!$canView){
            return "Usted no puede ver los formatos.";
        }

        //$company = $company->newQuery();
        $documents = $documents::with([]);

        $name = $request->has('name') ? $request->input('name'): null;
        $work_type_id = $request->has('work_type_id') ? $request->input('work_type_id'): null;
        $sub_category_id = $request->has('sub_category_id') ? $request->input('sub_category_id'): null;

        //POLICIES
        //$canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        $canListAllUsers = true;
        //END POLICIES

        // Search for a user based on their name.
        if ($name) {
            $documents->where('name', 'LIKE', "%$name%");
        }

        // Search for a user based on the documents for the work selected.
        if ($work_type_id) {
            $documents->whereHas("workTypes", function ($q) use ($work_type_id) {
                $q->where("id", $work_type_id);
            });
        }

        // Search for a user based on their name.
        if ($sub_category_id) {
            $documents->where("sub_category_id", $sub_category_id);
        }

        $documents = $documents->orderBy('id', 'desc')->paginate(12);
        //dd($movements);
        return view('documentTemplates.list', compact('documents') );

    }
}
