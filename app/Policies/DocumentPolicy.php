<?php

namespace App\Policies;

use App\Document;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function seeAllDocuments(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can edit the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can delete the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->role_id === Role::ADMIN;
    }

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeState(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::EXTERNAL_CONSULTANT || $user->role_id === Role::PROJECT_COORDINATOR || $user->role_id === Role::INTERNAL_CONSULTANT;
    }

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeStateToApproved(User $user)
    {
        return $user->role_id === Role::ADMIN;
    }

    /**
     * Determine whether the user can change the state of the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function changeStateToPrinted(User $user)
    {
        return $user->role_id === Role::ADMIN;
    }

    /**
     * Determine whether the user can print the document.
     * @param  \App\User  $user
     * @return mixed
     */
    public function printDocument(User $user, Document $document)
    {
        $isAdmin = $user->role_id === Role::ADMIN;
        return  $isAdmin || ( !$isAdmin && ($document->state >= \App\Document::APPROVED) );
    }
}
