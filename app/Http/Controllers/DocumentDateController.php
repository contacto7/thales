<?php

namespace App\Http\Controllers;

use App\Document;
use App\DocumentDate;
use App\DocumentTemplate;
use App\DocumentType;
use App\Http\Requests\DocumentDateRequest;
use App\Mail\ApproveCustomerMail;
use App\Mail\ApproveDocumentDateMail;
use App\Revision;
use App\Role;
use App\Track;
use App\Traits\GoogleApi;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DocumentDateController extends Controller
{
    use GoogleApi;

    public function listCustomer($id){
        //POLICIES
        $canViewOnlyTheirs = auth()->user()->can('viewOnlyTheirs', [DocumentDate::class] );
        //END POLICIES

        $documentDates = DocumentDate::with([])
            ->orderBy('id', 'desc')
            ->where('user_belong_id', $id);

        $userBelong = $id;

        if($canViewOnlyTheirs){
            $documentDates = $documentDates->where('user_created_id', auth()->user()->id);
        }

        $documentDates = $documentDates->paginate(12);

        //dd($vouchers);
        return view('documentDates.listCustomer', compact('documentDates', 'userBelong') );
    }
    public function listMyHistoricDocuments(){
        $id = auth()->user()->id;

        $documentDates = DocumentDate::with([])
            ->orderBy('id', 'desc')
            ->where('user_belong_id', $id)
            ->paginate(12);

        $userBelong = $id;

        //dd($vouchers);
        return view('documentDates.listMyHistoricDocuments', compact('documentDates', 'userBelong') );
    }
    public function listForApproving(){
        $documentDates = DocumentDate::with([])
            ->orderBy('id', 'desc')
            ->where('state', DocumentDate::FOR_ACTIVATION)
            ->paginate(12);

        //dd($vouchers);
        return view('documentDates.listForApproving', compact('documentDates') );
    }
    public function approve(DocumentDate $documentDate){
        try {
            $documentDate->fill(['state'=>DocumentDate::ACTIVE])->save();
            return back()->with('message',['success',
                __("Se aprobó la documentación")]);
        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error aprobando la documentación,
                por favor intente de nuevo.")]);


        }
    }
    public function filter(Request $request, DocumentDate $documentDates){

        //$company = $company->newQuery();
        $documentDates = $documentDates::with([]);

        $userBelong = $request->has('user_belong_filter') ? $request->input('user_belong_filter'): null;
        $document_date = $request->has('document_date_filter') ? $request->input('document_date_filter'): null;
        $user_created_id = $request->has('user_created_id') ? $request->input('user_created_id'): null;

        //POLICIES
        $canViewOnlyTheirs = auth()->user()->can('viewOnlyTheirs', [DocumentDate::class] );
        //END POLICIES

        // Search for a user based on their name.
        if ($userBelong) {
            $documentDates->where('user_belong_id', $userBelong);
        }else{
            //IF THERE IS NOT SET THE USER WHERE THE DOCUMENTS BELONG, WE RETURN BACK
            return back();
        }

        // Search for a user based on their name.
        if ($document_date) {
            $documentDates->whereYear('document_date', $document_date);
        }

        // Search for a user based on their name.
        if ($user_created_id) {
            $documentDates->where('user_created_id', $user_created_id);
        }

        if($canViewOnlyTheirs){
            $documentDates = $documentDates->where('user_created_id', auth()->user()->id);
        }

        $documentDates = $documentDates->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('documentDates.listCustomer', compact('documentDates', 'userBelong') );

    }
    public function customerFilter(Request $request, DocumentDate $documentDates){

        //$company = $company->newQuery();
        $documentDates = $documentDates::with([]);

        $userBelong = $request->has('user_belong_filter') ? $request->input('user_belong_filter'): null;
        $document_date = $request->has('document_date_filter') ? $request->input('document_date_filter'): null;
        $user_created_id = $request->has('user_created_id') ? $request->input('user_created_id'): null;

        //POLICIES
        //$canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        $canListAllUsers = true;
        //END POLICIES

        // Search for a user based on their name.
        if ($userBelong) {
            $documentDates->where('user_belong_id', $userBelong);
        }else{
            //IF THERE IS NOT SET THE USER WHERE THE DOCUMENTS BELONG, WE RETURN BACK
            return back();
        }

        // Search for a user based on their name.
        if ($document_date) {
            $documentDates->whereYear('document_date', $document_date);
        }

        // Search for a user based on their name.
        if ($user_created_id) {
            $documentDates->where('user_created_id', $user_created_id);
        }


        if($canListAllUsers || $document_date || $user_created_id){
            $documentDates = $documentDates->orderBy('id', 'desc')->paginate(12);
        }else{
            $documentDates =  $documentDates->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }


        //dd($movements);
        return view('documentDates.listMyHistoricDocuments', compact('documentDates', 'userBelong') );

    }

    public function store(DocumentDateRequest $documentDateRequest){

        $documentDateRequest->merge(['user_created_id' => auth()->user()->id ]);
        $user_belong_id  =$documentDateRequest->user_belong_id;
        $document_date  =$documentDateRequest->document_date;
        $document_date_formatted = Carbon::parse($document_date)->format('d/m/Y');

        //Random Identifier for the tracking of the documents creation
        $randomTrackIdentifier = $documentDateRequest->randomTrackIdentifier;

        /*Fixing 524 Cloudfare error*/
        // sending a data.
        $spacer_size = 8; // increment me until it works
        echo str_pad('', (1024 * $spacer_size), "\n");
        // send 8kb of new line to browser (default), just make sure that this new line will not affect your code.
        /*Fixing 524 Cloudfare error*/

        try {

            $documentDateRequest = $documentDateRequest->except(['randomTrackIdentifier']);

            $documentDate = DocumentDate::create($documentDateRequest);

            $document_date_id = $documentDate->id;


            //CREAR DOCUMENTOS

            //LIST OF ALL DOCUMENT TEMPLATES
            $documentTemplates =  DocumentTemplate::with([])
                ->whereHas("workTypes", function ($q) use ($user_belong_id) {
                    $q->whereHas("works", function ($q) use ($user_belong_id) {
                        $q->where("user_id", $user_belong_id);
                    });
                })
                ->orderBy('id', 'desc')
                ->get();

            //GET CUSTOMER DATA

            $user = User::with('customer')->whereId($user_belong_id)->first();

            $contact_name = $user->name;
            $contact_last_name = $user->last_name;
            $contact_email = $user->last_name;
            $company_name = $user->customer->company_name;
            /*
            $storagePath = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $company_logo = $storagePath.$user->customer->company_logo ? $user->customer->pathAttachment():url('image/company-logo.png');
            dd($company_logo);
            */
            $company_logo = "https://inoloop.com/img/Logo-morado.png";
            $ruc = $user->customer->tax_number;


            //INITIALIZE GOOGLE VARIABLES
            $client = $this->getClient();
            $driveService = $this->getGoogleServiceDrive($client);
            $docsService = $this->getGoogleServiceDocs($client);
            $sheetsService = $this->getGoogleServiceSheets($client);
            $slidesService = $this->getGoogleServiceSlides($client);

            //CREATION VARIABLES
            $docsForCreation = count($documentTemplates);
            $docsCreated =0;
            $percentageCreated = 0;

            $tracking = Track::create(
                [
                    'random_identifier'=>$randomTrackIdentifier,
                    'completed'=>$docsCreated,
                    'total'=>$docsForCreation,
                ]
            );

            foreach ($documentTemplates as $documentTemplate){

                //VARIABLES
                $documentId = $documentTemplate->google_document_id;
                $copyTitle = $documentTemplate->name." - ".$company_name;
                $documentType = $documentTemplate->document_type_id;
                //VARIABLES

                //PICK THE CORRECT SERVICE: DOCS, SPREADSHEETS OR SLIDES
                $service = $this->getServiceByType($docsService, $sheetsService, $slidesService,  $documentType);
                $documentCopyId = $this->copyFromTemplate($documentId, $copyTitle, $driveService);
                $document  = $this->getDocumentByType($service, $documentCopyId, $documentType);

                $requests = array();

                $requestsReplaceImage = $this->replaceImageByType($company_logo,Document::LOGO, $documentType, $document);

                if (is_array($requestsReplaceImage)){
                    $requests = array_merge($requests, $requestsReplaceImage);
                }else{
                    $requests[] = $requestsReplaceImage;
                }
                //REORDENAMOS LAS INSERCIONES DE IMAGENES, PARA QUE SE AGREGUEN DESDE EL ÚLTIMO Y
                //NO SE VAYA PERDIENDO EL POSICIONAMIENTO DE LOS OTROS REEMPLAZOS
                $requests = array_reverse($requests);

                $requests[] = $this->replaceTextByType(Document::LOGO, "", $documentType);

                $requests[] = $this->replaceTextByType(Document::NOMBRES, $contact_name, $documentType);
                $requests[] = $this->replaceTextByType(Document::FECHA, $document_date_formatted, $documentType);
                $requests[] = $this->replaceTextByType(Document::EMPRESA, $company_name, $documentType);
                $requests[] = $this->replaceTextByType(Document::RUC, $ruc, $documentType);

                $responseFromUpdate =  $this->updateDocumentByType($requests, $documentCopyId, $service, $documentType);


                //CREATION OF DOCUMENT IN THE SYSTEM
                Document::create([
                    'name'=>$copyTitle,
                    'description'=>$copyTitle,
                    'state'=>1,
                    'google_document_id'=>$documentCopyId,
                    'document_template_id'=>$documentTemplate->id,
                    'document_date_id'=>$document_date_id,
                ]);

                $docsCreated++;

                $tracking->fill(['completed'=>$docsCreated])->save();

                //sleep(3); // this should halt for 3 seconds for every loop
            }

            $tracking->delete();
            //dd($customerRequest);
            //return back()->with('message',['success', __("Se agregó la nueva fecha correctamente.")]);
            return redirect()->route('documents.listCustomerFromDate', $document_date_id);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando la fecha,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }


    public function ajaxStore(DocumentDateRequest $documentDateRequest){

        //POLICIES
        $canCreate = auth()->user()->can('create', [DocumentDate::class] );
        //END POLICIES
        $state = $canCreate ? DocumentDate::ACTIVE : DocumentDate::FOR_ACTIVATION;
        $documentDateRequest->merge(['state' => $state ]);

        $documentDateRequest->merge(['user_created_id' => auth()->user()->id ]);
        $user_belong_id  =$documentDateRequest->user_belong_id;
        $document_date  =$documentDateRequest->document_date;
        $document_date_formatted = Carbon::parse($document_date)->format('d/m/Y');

        //Random Identifier for the tracking of the documents creation
        $randomTrackIdentifier = $documentDateRequest->input('randomTrackIdentifier');

        /*Fixing 524 Cloudfare error*/
        // sending a data.
        $spacer_size = 8; // increment me until it works
        echo str_pad('', (1024 * $spacer_size), "\n");
        // send 8kb of new line to browser (default), just make sure that this new line will not affect your code.
        /*Fixing 524 Cloudfare error*/


        $documentDateRequest = $documentDateRequest->except(['randomTrackIdentifier']);

        $documentDate = DocumentDate::create($documentDateRequest);

        $document_date_id = $documentDate->id;

        //CREAR DOCUMENTOS

        //LIST OF ALL DOCUMENT TEMPLATES
        $documentTemplates =  DocumentTemplate::with([])
            ->whereHas("workTypes", function ($q) use ($user_belong_id) {
                $q->whereHas("works", function ($q) use ($user_belong_id) {
                    $q->where("user_id", $user_belong_id);
                });
            })
            ->orderBy('id', 'desc')
            ->get();

        //GET CUSTOMER DATA

        $user = User::with('customer')->whereId($user_belong_id)->first();

        //INITIALIZE GOOGLE VARIABLES
        $client = $this->getClient();
        $driveService = $this->getGoogleServiceDrive($client);
        $docsService = $this->getGoogleServiceDocs($client);
        $sheetsService = $this->getGoogleServiceSheets($client);
        $slidesService = $this->getGoogleServiceSlides($client);

        //CREATION VARIABLES
        $docsForCreation = count($documentTemplates);
        $docsCreated =0;
        $percentageCreated = 0;

        $tracking = Track::create(
            [
                'random_identifier'=>$randomTrackIdentifier,
                'completed'=>$docsCreated,
                'total'=>$docsForCreation,
                'document_date_id'=>$document_date_id,
            ]
        );


        foreach ($documentTemplates as $documentTemplate){
            $document = new Document();
            try {
                $document->createDocument(
                    $documentTemplate,
                    $document_date_id,
                    $driveService,
                    $docsService,
                    $sheetsService,
                    $slidesService,
                    $user,
                    $document_date_formatted
                );
                $docsCreated++;

                $tracking->fill(['completed'=>$docsCreated])->save();
            }
            catch (\Throwable $t) {
                //if error, continue with the creation
                continue;
            }

        }

        //FORCE THE COMPLETION TO CONTINUE TO NEXT PAGE
        $tracking->fill(['completed'=>$docsForCreation])->save();

        $admins = User::where('role_id',Role::ADMIN)->get();

        $recipients = $admins;

        \Mail::to($recipients)->send( new ApproveDocumentDateMail( $documentDate, auth()->user() ) );
        //echo route('documents.listCustomerFromDate', $document_date_id);

    }

    public function progress(Request $request){

        $randomTrackIdentifier  = $request->randomTrackIdentifier;
        //dd($vouchers);

        $tracking = Track::where('random_identifier',$randomTrackIdentifier)->first();

        return view('partials.docCreationProgress', compact('tracking'));
    }



    public function changeState(Request $request, DocumentDate $documentDate){

        $document_date_id = (int) $request->has('document_date_id') ? $request->input('document_date_id'): null;
        $state = (int) $request->has('state') ? $request->input('state'): null;

        $documentDate = $documentDate->newQuery();

        $documentDate = $documentDate->where('id',$document_date_id)->first();

        //POLICIES
        $canViewThis = auth()->user()->can('viewThis', $documentDate );
        //END POLICIES

        if (!$canViewThis){
            return 403;
        }

        //POLICIES
        $canChangeState = auth()->user()->can('changeState', DocumentDate::class );
        $canChangeStateToActive = auth()->user()->can('changeStateToActive', DocumentDate::class );
        $canChangeStateToTerminated = auth()->user()->can('changeStateToPrinted', DocumentDate::class );
        //END POLICIES

        // If there is not an ID of the document, we return and error code 3
        if (!$document_date_id) {
            return 3;
        }
        // If the user cannot change the state of the document, return error 4
        if (!$canChangeState){
            return 4;
        }
        // If the user cannot change the state of the document to approved, return error 5
        if ($state == DocumentDate::PROJECT_ACTIVE && !$canChangeStateToActive){
            return 5;
        }
        // If the user cannot change the state of the document to printed, return error 4
        if ($state == DocumentDate::PROJECT_TERMINATED && !$canChangeStateToTerminated){
            return 6;
        }

        try {
            $documentDate->update(['project_state' => $state]);
            return 1;
        } catch(\Illuminate\Database\QueryException $e){
            return 2;
        }



    }

}
