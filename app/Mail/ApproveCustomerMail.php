<?php

namespace App\Mail;

use App\Customer;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApproveCustomerMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, User $UserCreated)
    {
        $this->customer = $customer;
        $this->user_created = $UserCreated;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Nuevo cliente registrado")
            ->markdown("emails.approve_customer_mail")
            ->with('customer', $this->customer)
            ->with('userCreated', $this->user_created);
    }
}
