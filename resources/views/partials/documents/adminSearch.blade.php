<div class="head-page">
    @include('partials.title', [
        'title' => __("Aprobar documentos creados"),
        'icon' => "files-o"
    ])
    <form action="{{ route('documents.adminSearch') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="company_name"
                class="col-sm-3 col-form-label"
            >
                Cliente
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="company_name"
                    id="company_name"
                    type="text"
                    placeholder="{{ __("Ingrese la razón social") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Documento
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Ingrese el nombre del documento") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="work_type_id"
                class="col-sm-3 col-form-label"
            >
                Por servicio
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="work_type_id"
                    id="work_type_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(
                        \App\WorkType::get() as $workType)
                        <option
                            value="{{ $workType->id }}"
                        >{{ $workType->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label
                for="sub_category_id"
                class="col-sm-3 col-form-label"
            >
                Categoría
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="sub_category_id"
                    id="sub_category_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\SubCategory::get() as $subCategory)
                        <option
                            value="{{ $subCategory->id }}"
                        >{{ $subCategory->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label
                for="state"
                class="col-sm-3 col-form-label"
            >
                Estado
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="state"
                    id="state"
                >
                    <option value="">Seleccionar</option>
                    <option value="{{ \App\Document::FOR_REVISION }}">Por revisar</option>
                    <option value="{{ \App\Document::REVISED }}">Revisado</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-thales-secondary"
                    name="filter"
                    type="submit"
                    value="buscar"
                >
            </div>
        </div>
    </form>
</div>


