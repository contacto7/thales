<li class="">
    <a href="#customersSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fa fa-files-o"></i>
        Documentos
    </a>
    <ul class="collapse list-unstyled" id="customersSubmenu">
        <li>
            <a href="{{ route('documents.listMyDocuments') }}">Listar</a>
        </li>
        <li>
            <a href="{{ route('documentDates.listMyHistoricDocuments') }}">Historial</a>
        </li>
    </ul>
</li>

@include('partials.sidebar.logged')
