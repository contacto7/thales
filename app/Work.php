<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Work
 *
 * @property int $id
 * @property float|null $cost
 * @property int $work_type_id
 * @property int $customer_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Customer $customer
 * @property-read \App\WorkType $workType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereWorkTypeId($value)
 * @mixin \Eloquent
 */
class Work extends Model
{
    protected $fillable = [
        'cost','work_type_id','user_id',
    ];

    public function workType(){
        return $this->belongsTo(WorkType::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
