<table class="table table-striped table-light">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Descripción</th>
        <th scope="col">Realizado</th>
        <th scope="col">Usuario</th>
    </tr>
    </thead>
    <tbody>
    @forelse($revisions as $revision)
        <tr>
            <td>{{ $revision->getKey('') }}</td>
            <td>{!! nl2br(e($revision->comments)) !!}</td>
            <td>{{ $revision->created_at }}</td>
            <td>
                {{ $revision->user->name." ".$revision->user->last_name }}
            </td>
        </tr>
    @empty
        <tr>
            <td>{{ __("No hay revisiones disponibles")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>
<div class="row justify-content-center revisions-pagination">{{ $revisions->links() }}</div>
