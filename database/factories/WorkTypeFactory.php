<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WorkType;
use Faker\Generator as Faker;

$factory->define(WorkType::class, function (Faker $faker) {
    return [
        'name'=> $faker->word,
        'description'=> $faker->sentence,
    ];
});
