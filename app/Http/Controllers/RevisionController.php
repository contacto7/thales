<?php

namespace App\Http\Controllers;

use App\Revision;
use Illuminate\Http\Request;

class RevisionController extends Controller
{
    public function listFromDocument($id, $type){
        $revisions = Revision::with([])
            ->where('document_id', $id)
            ->where('type', $type)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('revisions.listFromDocument', compact('revisions') );
    }

    public function modalSeeForm(Request $request){

        $document_id = (int) $request->has('document_id') ? $request->input('document_id'): null;
        $type = (int) $request->has('type') ? $request->input('type'): null;

        return self::listFromDocument($document_id, $type);
    }

}
