<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DocumentTemplate;
use Faker\Generator as Faker;

$factory->define(DocumentTemplate::class, function (Faker $faker) {
    return [
        'google_document_id'=> $faker->bothify('##????###?????####???#?#?#?#?#'),
        'name'=> $faker->word,
        'description'=> $faker->sentence,
        'document_type_id'=>\App\DocumentType::all()->random()->id,
        'sub_category_id'=>\App\SubCategory::all()->random()->id,
    ];
});
