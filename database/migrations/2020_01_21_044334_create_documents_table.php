<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            /*
            NO USAMOS EL MODELO DOCUMENTO PORQUE GENERA ERROR POR EL "USE GOOGLEAPI"
            EN EL MODELO DOCUMENT.PHP
            */
            $table->enum('state', [
                1, //\App\Document::FOR_REVISION,
                2, //\App\Document::REVISED,
                3, //\App\Document::APPROVED,
                4,//\App\Document::PRINTED,
                5,//\App\Document::FOR_DELETION,
            ])->default(1);//\App\Document::FOR_REVISION
            $table->text('google_document_id');
            $table->unsignedBigInteger('document_template_id');
            $table->foreign('document_template_id')->references('id')->on('document_templates');
            $table->unsignedBigInteger('document_date_id');
            $table->foreign('document_date_id')->references('id')->on('document_dates');
            $table->text('google_download_id')->nullable();
            $table->unsignedBigInteger('user_deleted_id')->nullable();
            $table->foreign('user_deleted_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
