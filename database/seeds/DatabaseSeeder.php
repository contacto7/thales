<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        if (!File::exists('companies')) {
            Storage::makeDirectory('companies');
        }

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'name' => 'administrador'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'consultor-externo'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'coordinador-de-proyectos'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'consultor-interno'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'cliente'
        ]);

        //////////USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Billy',
            'last_name' => 'Areche',
            'email' => 'contacto@inoloop.pe',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'External Consultant',
            'email' => 'external_consultant@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::EXTERNAL_CONSULTANT,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Project Coordinator',
            'email' => 'project_coordinator@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::PROJECT_COORDINATOR,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Internal Consultant',
            'email' => 'internal_consultant@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::INTERNAL_CONSULTANT,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Cliente',
            'email' => 'cliente@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::CUSTOMER,
            'state' => 1
        ]);

        factory(\App\User::class, 8)
            ->create([
                'password' => 'secret',
                'role_id' => \App\Role::CUSTOMER,
                'state' => 1
            ])
            ->each(function (\App\User $us) {
                factory(\App\Customer::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                        'user_created_id'=>3,
                        'company_logo'=>'logo-dev-'.random_int(1,5).'.png',
                    ]);
            });

        factory(\App\User::class, 8)
            ->create([
                'password' => 'secret',
                'role_id' => \App\Role::CUSTOMER,
                'state' => 1
            ])
            ->each(function (\App\User $us) {
                factory(\App\Customer::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                        'user_created_id'=>4,
                        'company_logo'=>'logo-dev-'.random_int(1,5).'.png',
                    ]);
            });

        //WORK TYPES
        factory(\App\WorkType::class, 1)->create([
            'name' => 'Ambiental',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'Responsabilidad Social',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'Seguridad y Salud en el Trabajo',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'SGC',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'SGCA',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'SGCN',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'SGHS',
        ]);
        factory(\App\WorkType::class, 1)->create([
            'name' => 'SGAS',
        ]);

        //WORKS
        factory(\App\Work::class, 15)->create();

        //CATEGORY & SUB-CATEGORIES
        factory(\App\Category::class, 1)
            ->create([
                'id' => 1,
                'name' => 'Ambiental',
                ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>1,
                        'category_id'=>$ca->id,
                        'name'=>'Matriz IAEI',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>2,
                        'category_id'=>$ca->id,
                        'name'=>'Plan de manejo de residuos',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 2,
                'name' => 'Responsabilidad Social',
                ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>3,
                        'category_id'=>$ca->id,
                        'name'=>'Código de Etica y Conducta',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>4,
                        'category_id'=>$ca->id,
                        'name'=>'Inducción del personal',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>5,
                        'category_id'=>$ca->id,
                        'name'=>'Procedimiento de contratación',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>6,
                        'category_id'=>$ca->id,
                        'name'=>'Procedimiento de remunceración justa',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>7,
                        'category_id'=>$ca->id,
                        'name'=>'Procedimiento de sugerencias y reclamos',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 3,
                'name' => 'Seguridad y Salud en el Trabajo',
                ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>8,
                        'category_id'=>$ca->id,
                        'name'=>'Capacitaciones',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>9,
                        'category_id'=>$ca->id,
                        'name'=>'CSST',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>10,
                        'category_id'=>$ca->id,
                        'name'=>'Estudio de Línea Base',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>11,
                        'category_id'=>$ca->id,
                        'name'=>'Formatos - Seguridad y Salud en el Trabajo',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>12,
                        'category_id'=>$ca->id,
                        'name'=>'Formatos de Equipos de Emergencia',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>13,
                        'category_id'=>$ca->id,
                        'name'=>'Formatos de Registro de Ley',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>14,
                        'category_id'=>$ca->id,
                        'name'=>'Informes de inspección',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>15,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo - Seguridad y Salud en el Trabajo',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>16,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo Ergonómico',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>17,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo de Iluminación',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>18,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo de Material Particulado',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>19,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo Psicosocial',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>20,
                        'category_id'=>$ca->id,
                        'name'=>'Monitoreo de Ruido',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>21,
                        'category_id'=>$ca->id,
                        'name'=>'Plan y programa anual de SST',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>22,
                        'category_id'=>$ca->id,
                        'name'=>'Política de SST',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>23,
                        'category_id'=>$ca->id,
                        'name'=>'Procedimientos',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>24,
                        'category_id'=>$ca->id,
                        'name'=>'Reglamento interno de SST',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 4,
                'name' => 'SGC',
            ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>25,
                        'category_id'=>$ca->id,
                        'name'=>'Actas',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>26,
                        'category_id'=>$ca->id,
                        'name'=>'Análisis de riesgos y oportunidades',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>27,
                        'category_id'=>$ca->id,
                        'name'=>'Auditoría interna',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>28,
                        'category_id'=>$ca->id,
                        'name'=>'Contexto d la organización',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>29,
                        'category_id'=>$ca->id,
                        'name'=>'Control de documentos',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>30,
                        'category_id'=>$ca->id,
                        'name'=>'Evaluación de partes interesadas',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>31,
                        'category_id'=>$ca->id,
                        'name'=>'Indicadores de Calidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>32,
                        'category_id'=>$ca->id,
                        'name'=>'Informes de capacitación',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>33,
                        'category_id'=>$ca->id,
                        'name'=>'Mantenimiento y calibración',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>34,
                        'category_id'=>$ca->id,
                        'name'=>'Manual de calidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>35,
                        'category_id'=>$ca->id,
                        'name'=>'Mapa de procesos',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>36,
                        'category_id'=>$ca->id,
                        'name'=>'PE',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>37,
                        'category_id'=>$ca->id,
                        'name'=>'Política de Calidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>38,
                        'category_id'=>$ca->id,
                        'name'=>'Procedimientos',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>39,
                        'category_id'=>$ca->id,
                        'name'=>'Programa anual de calidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>40,
                        'category_id'=>$ca->id,
                        'name'=>'Proveedores',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>41,
                        'category_id'=>$ca->id,
                        'name'=>'Registros de conformidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>42,
                        'category_id'=>$ca->id,
                        'name'=>'Revisión por la Dirección',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>43,
                        'category_id'=>$ca->id,
                        'name'=>'Salidas no conformes y liberación del producto',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>44,
                        'category_id'=>$ca->id,
                        'name'=>'Sastifación del cliente',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 5,
                'name' => 'SGCA',
            ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>45,
                        'category_id'=>$ca->id,
                        'name'=>'Capacitaciones',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>46,
                        'category_id'=>$ca->id,
                        'name'=>'Seguridad de Contenedores',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>47,
                        'category_id'=>$ca->id,
                        'name'=>'Seguridad de Personal',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>48,
                        'category_id'=>$ca->id,
                        'name'=>'Seguridad Documentaria',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>49,
                        'category_id'=>$ca->id,
                        'name'=>'Seguridad Fisica',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>50,
                        'category_id'=>$ca->id,
                        'name'=>'Sensibilizacion',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>51,
                        'category_id'=>$ca->id,
                        'name'=>'Generales',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 6,
                'name' => 'SGCN',
            ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>52,
                        'category_id'=>$ca->id,
                        'name'=>'Línea Base',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>53,
                        'category_id'=>$ca->id,
                        'name'=>'Caso de Negocio',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>54,
                        'category_id'=>$ca->id,
                        'name'=>'Roles y comunicación',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>55,
                        'category_id'=>$ca->id,
                        'name'=>'Plan BC',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>56,
                        'category_id'=>$ca->id,
                        'name'=>'Política de BC',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>57,
                        'category_id'=>$ca->id,
                        'name'=>'Control de documentos',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>58,
                        'category_id'=>$ca->id,
                        'name'=>'Actas de designación de roles',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>59,
                        'category_id'=>$ca->id,
                        'name'=>'Procesos del sistema de calidad',
                    ]);
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>60,
                        'category_id'=>$ca->id,
                        'name'=>'Análisis del impacto del negocio',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 7,
                'name' => 'SGHS',
            ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>61,
                        'category_id'=>$ca->id,
                        'name'=>'Generales',
                    ]);
            });
        factory(\App\Category::class, 1)
            ->create([
                'id' => 8,
                'name' => 'SGAS',
            ])
            ->each(function (\App\Category $ca) {
                factory(\App\SubCategory::class, 1)
                    ->create([
                        'id'=>62,
                        'category_id'=>$ca->id,
                        'name'=>'Generales',
                    ]);
            });


        //DOCUMENT DATES
        //factory(\App\DocumentDate::class, 15)->create();

        //DOCUMENT TYPES & DOCUMENT TEMPLATES
        factory(\App\DocumentType::class, 1)
            ->create([
                'name' => 'Documentos',
                'google_name' => 'document',
                'extension' => '.docx',
            ]);
        factory(\App\DocumentType::class, 1)
            ->create([
                'name' => 'Hoja de cálculo',
                'google_name' => 'spreadsheets',
                'extension' => '.xlsx',
            ]);
        factory(\App\DocumentType::class, 1)
            ->create([
                'name' => 'Presentaciones',
                'google_name' => 'presentation',
                'extension' => '.pptx',
            ]);

        //DOCUMENT TEMPLATES
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1-hdjUOi5ScBJRMa4SGhHTU6JH8R-fzE3_b6czMaWEJo',
                'name'=>'MA-001 Matriz IAEI',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>1,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1VryrjC3nyNNUNrryD0yEm6LolXcvgMWcU5th5klbUc8',
                'name'=>'MA-002 Plan de manejo de residuos solidos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>2,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1DqQoBA0j2PxG8iuH4iC9x3OD_I6Kzu4iXZZoMnDHO6A',
                'name'=>'Cargo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>3,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1NKM6IZfzZ6SAwa2Ekz4fJw-sR1ISgQKBNtpDL1rAOy8',
                'name'=>'Código de conducta',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>3,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1lbpwj_b8z3MSNGOf25K0nAJraQhqiDu2MRhdfn_SVdw',
                'name'=>'RRHH-F-002 Registro sobre la inducción',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>4,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1nfS3uDBcf-xlcBwr55yeFGtmecGCNP8g8xKuulD9NtY',
                'name'=>'RRHH-P-04- Inducción de Personal',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>4,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1fh4fIUbKuhpdCoLRS01jRv5sZb3V4X_hmR_DHqbP6FM',
                'name'=>'RRHH-F-001 Acta de entrega de activos e inventarios',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>5,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'15aU5jOWaYy4lW9m02z_dZR1tShduTyF3yMETvC49-i8',
                'name'=>'RRHH-P-001 Procedimiento de contratación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>5,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'11mLHLBvYrQo197pHlQWPbGw_IOyZ5aaR-FZHWUz2D3M',
                'name'=>'RRHH-P-002 Procedimiento Remuneración Justa',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>6,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1UWLrEM0C2zxEkXfQLI0SKWrWepIz4PJX1mG1DXWMwx8',
                'name'=>'RRHH-P-003 Procedimiento Buzón de Sugerencias y,o Reclamos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>7,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1q4hE9E2JEDZQzVu1PqODjvToOwW2pMuXoV8TbBhqlz0',
                'name'=>'Informe de capacitación - Matriz IPERC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1eprwgAW_fZGRBkKgBjguM3aHbYCXUPaaRAJygDpd1QA',
                'name'=>'Informe de capacitación - Sistema de Gestión de SST',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1xoFXCB-t73C0hsWnadAtPCZ2Sv26DkVbyBob9JT35M8',
                'name'=>'Informe de capacitación - Uso de Extintores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1VQkd55BtdsaDSlY3exSdV_8TbSacCLq5wA0sZJShl6g',
                'name'=>'Informe de Capacitación al CSST - Funciones del CSST',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1hsJFBUxOKQ3TwOGudX2hzld1RYlgTPwUZelh2HzM_hA',
                'name'=>'Informe de Capacitación al CSST - Investigación de Accidentes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'12DRb1HxXbxLeLCtb6H4zi_n23YrNs-u0OPr8p2aXSC4',
                'name'=>'Informe de inspección - Primeros auxilios',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>8,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1puKsCpN-s9Ip15p7R7Co-LPzZibm1OhvD6ZNF727Fng',
                'name'=>'Cedula de votación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>9,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'19707ysLRBqL7MIrKNLUfYHveyD-SOo1juf4tM_VaMPk',
                'name'=>'Padrón Electoral',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>9,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1OQ78jvREOiG2ENzXwmcevIh_bH1v70q6HbOg1Z5n1v4',
                'name'=>'Proceso de elección del CSST',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>9,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1l-ai2a3j7UIGdZzxYgs_FfLcqYFCav2QAMe_q7W0kfY',
                'name'=>'REGLAMENTO PARA EL PROCESO DE ELECCIONES',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>9,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Ei7hWtZF-5IMk2pp4VfR674obhTWYDeAhTwObM_Xqso',
                'name'=>'0.1 Estudio de Línea Base',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>10,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1QZ6duXaPWxEeSrbGKATNuxmdOan2-elsNM6EdVC8e28',
                'name'=>'SST-002 Objetivos de Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>11,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1TYs4KN01JI2T9DAdzC9nYFBqICN1fgJ-aT9fa3Z_Ye8',
                'name'=>'SST-027 Listado Maestro de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>11,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1z0H7klgjtzlQTRPC2onJsq90vM_8HDJYoSY4cnauaco',
                'name'=>'SST-028 Matriz de Requisitos Legales en Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>11,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1lQrIz3tbapeBTEeL67S5twzAuDk6MC6QCHslDtg3Vw0',
                'name'=>'SST - 018 - Inspección Mensual de Extintores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>12,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1yLGUCG7gSffmK2WDkNGqBNqVg8NzR2JxO606blRXgR8',
                'name'=>'SST - 019 - Inspección de Botiquín',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>12,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1m7pHGQQOQOx1v-LL-5QMn9J3u9kfgfrOY14QOTN1DRo',
                'name'=>'SST - 020 - Inspección de Luces de Emergencia',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>12,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1L9RA2jWVvGmfupSgDcUN-GAA7zwz_0pSDD50z8lJXx4',
                'name'=>'SST - 006 - Registro de Enfermedades Ocupacionales',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Ft35ftqPKdXdxljgAewRBS5WALMxAZg3FAIkww5IoPw',
                'name'=>'SST - 007 - Estadíticas de Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1e2VsBDwIGpGDOuG7R57P2D0TGWM4JdS_Vl_NiY-oHl0',
                'name'=>'SST - 008 - Informe de Simulacro',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1B4iCG6nIcP3zAQDJDwZ69PpvVoZP8caIpjVquRdvcfY',
                'name'=>'SST - 009 - Registro de Auditorías',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Vs0it9pzrx7sHfe-Ql_RwUarWaUfPdU9PxPltsl6DVE',
                'name'=>'SST - 010 - Registro de Asistencia',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CoRnPUiUAoIt96Tp9YWq604CTKGtVrxqZXM12Slgwz4',
                'name'=>'SST - 011 - Investigación de Incidentes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CDl2rFYof-4yOotNEu-UhOLMW3lgz5hEoEE2SftTuVE',
                'name'=>'SST - 012 - Investigación de Accidentes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1xxtl59ogyvClGwvNsWD9mqx-sf0g1sXc9MBujPH7fnc',
                'name'=>'SST - 013 - Reporte Preliminar de Accidente-Incidente',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1kHOaPbB_Stbr9dTY5WdcY6NlTY4tujw8nIelquv44PA',
                'name'=>'SST - 014 - Registro de Monitoreos de Agentes Ocupacionales',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'13L9vhrJ-vxOolMc1odMskGikdMy43Kfa-BY7SZ8L6qA',
                'name'=>'SST - 015 - Informe de Inspección de Seguridad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1V3hGlaLrs5au2rNVYeTK0D2oUss67AgPYqb5R3LrvKk',
                'name'=>'SST - 016 - Lista de Chequeo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1OWZke_VLzvk1AqmPyN_tmxw1KWtKD6V0cPs2pGJW4Jc',
                'name'=>'SST - 017 - MATRIZ IPERC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1OYnuJ8mzau8yvzF5mE-JqZyel0oF2jpyCyNY32sJSbo',
                'name'=>'SST - 029 - Programa Anual de Capacitaciones de SST',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'17b8gQuzGOEz2QeTKMcUPm_-POZ_dsyd1s0lMvm-Z864',
                'name'=>'SST - 030 - Registro de Exámenes Médicos Ocupacionales',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1a00bM08OOsCHLeepIClP3DJMuRmAsp6caTE337yAf5Y',
                'name'=>'SST - 033 - Declaración de accidente ,incidente de trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>13,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1eF_3dH-7enYptP2lbHuX8HpjYMiwAgSoaFOXFSI8NVo',
                'name'=>'INFORME DE INSPECCIÓN',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>14,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Agtkd_nWd69hrEFphnpToYf7OMRIHJ7AXN2N_R1iayU',
                'name'=>'SST - 014 - Registro de Monitoreos de Agentes Ocupacionales - PINTER',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>15,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1drckkVQ9LQJCauQ5pR2QgI-czjfbPq2cLjb_YkPs9vw',
                'name'=>'Informe de Monitoreo - Disergonómico',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>16,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'17i2Ckb-9aj8wHXEbZkbSyF4SFgf2iX9PA6PddcGzfDM',
                'name'=>'Fórmula de interpolación-iluminación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>17,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'18FJgji9GKFIqOpi93cXj4fB4_ZgdkcFEpT-gZgiQppQ',
                'name'=>'Informe de Monitoreo Ocupacional - Iluminación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>17,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CURCFdxlf47ec6BLGDTNBHsM5wsY6oooz9SeoueKxLI',
                'name'=>'Informe de Monitoreo Ocupacional - Material particulado',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>18,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1funyJUlybsTNd008Vr_vDiastOOxx7p1e15FAW80NA8',
                'name'=>'Informe de Monitoreo Ocupacional',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>19,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1E1j_f36TMKEc5n3D5P4RMb5C1H3o3E6Un4Mvt669nVk',
                'name'=>'Informe de Monitoreo Ocupacional - Ruido',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>20,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1SP81Vm5zWr-eMiGxgORzOnCyDPuI8M6TtdCR_uYT3XM',
                'name'=>'SST-003 Plan anual de Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>21,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'10LXKkb1tvJynNY2PG7jPHoGV1CO1Le4CBBXp5_ROYeA',
                'name'=>'SST-004 Programa anual de Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>21,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1E2kgNp0DzdnHBvWAIl-c8PGTpEbTAAr19jeIV-c6-VQ',
                'name'=>'SST-001 Política de Seguridad y Salud en el Trabajo',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>22,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1EcCPwSWYPZiKzwJJ2HrxUaJbrQFMgU8tuYNDvZ19T8E',
                'name'=>'SST-021 - Equipos de Protección Personal',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1TsAEohYc12BS24Dopd2shP0Qcurt6NHj-O-DkGf5wEg',
                'name'=>'SST-022 - Gestión de Salud Ocupacional',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'10qQOPCau_jOFUoF7bjn2u7SsPcdpTK3Mj4POjaNfi2c',
                'name'=>'SST-023 - Procedimiento de MATPEL',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1sJASJZsQz5_RiGgIbylsgF1bq56j6_TKEN0OUf0h6DY',
                'name'=>'SST-024 - Procedimiento de Investigación de incidentes y accidentes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Qf3Vw7TTmuaq904wL4XPjMfaGVYFrjogCNImQDK0FLE',
                'name'=>'SST-025 - Procedimiento IPERC-IAEI',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1J9KPSsy2Z5l4sSqH8keryvaA0ql0G0rdjsu-5e3Ixrg',
                'name'=>'SST-026 - Procedimiento de Ergonomía',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1_h85rH3IW-xOvG-Xdsm-YPIK4nxyZ8smmsnOI-pEiYw',
                'name'=>'SST-032 - Inspecciones de Seguridad en las instalaciones',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>23,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1wIg3bmSlNycEorD5yNfYT6JE3GUL_5xZJKaKDupwI1M',
                'name'=>'Reglamento Interno de Seguridad y Salud',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>24,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1IbySVyTcge9DNjlfKIpyCXxjZIFm4ALg2xsmOSXsPCE',
                'name'=>'Acta de Instalación CGC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>25,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'14NfoTPp1XZNEMaSUvo94z4WAuUlHSwWWcc2tR1K7eqw',
                'name'=>'Acta de Reunión Ordinaria CGC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>25,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'13fDsv-Eg5-yKLETnTXVl3bK8crQU2jnMKLLwmMk6oeU',
                'name'=>'Aprobación de Documentos del CGC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>25,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1YjCB6FCLERycAyHllMvuEv9iETF44RHHXf43cpxpKFM',
                'name'=>'SGC-P-009 Análsis de riesgos y oportunidades',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>26,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1GWiwapgQrsoDU8EsDmXOPo-oh_ePvDMXCxCunPr2X2Q',
                'name'=>'SGC-F-008 Programa de Anual de Auditoría Interna y Revisión por la Dirección',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>27,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'15E50CyJpOuFSLcBjf4GZZnh0cLBtO1i2CJCFWhZ3jOo',
                'name'=>'SGC-F-009 Solicitud de acciones de mejora',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>27,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1J-nGi_6zg95Ih5ee1nYTlqZDCzKm--_fCGMgd4e9VxM',
                'name'=>'SGC-P-003 Auditoria Interna',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>27,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1x0eidmfa4noXRyZcWwyvvsOFnJgaw4wKNAH5UMCYwbI',
                'name'=>'SGC-P-004 Plan de auditoría interna',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>27,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1BOvJT6rrHc2j6ZyHO-TsDxitkBKzEmndBNVcRB09qk8',
                'name'=>'SGC-P-012 Contexto de la organización',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>28,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1mNVmPgiTcWWenc1K1VM1IWGwfebwcrd1H8bwn7PloG4',
                'name'=>'SGC-F-001 Lista Maestra de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1GDBEsD47BwxDv0cHhGokfyx0KQa6QekgKIibla8vOOY',
                'name'=>'SGC-F-002 Solicitud de elaboración, actualización y eliminación de documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ui3U32mzyIN096OWT5EOSl1pJ1AeurtLwB1EMhSt99o',
                'name'=>'SGC-F-003 Reporte de incidencias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1IDB4Q2AgVYqP0L-V_RqqILPLbjfQq_vIRLZflcqDE5w',
                'name'=>'SGC-F-004 Consolidado de incidencias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1FSDz6IUuMEg6WpMbXv4kuvGdmJzU4WUqCs1pRYBz4ns',
                'name'=>'SGC-F-005 Cargo de entrega',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'19ZStIcoZjNqA4JN7YDRJiZNRtEFn88QU2QMVLuxkbP0',
                'name'=>'SGC-F-006 Registro de Asistencia',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'12FxCow7DqyifA2vWYYVdblhzeRP0ac_24U_IlZfDi9I',
                'name'=>'SGC-F-018 Solicitud de Cambios',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1kGYRq3RYmJq2GJytGAr1b5Q3r-gIe-q2AhYdbJbo-Ew',
                'name'=>'SGC-P-001 Control de documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>29,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1me4lvYFtQw3dQa-B36IIBtrrrhVnjCE5923-4XM3Sh4',
                'name'=>'SGC-P-011 Evaluación de las partes interesadas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>30,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1bJgwFBhxse0YH0uICo4zmBBHS7-b5PJgIClr6xqoTZg',
                'name'=>'SGC-F-016 Indicadores de Calidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>31,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1PqVQ8art5940M-OrV-Zz5MyFac0fJ5HGf33NhKn6lBY',
                'name'=>'Informe de Capacitación - Objetivo y evaluación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>32,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1T85IHu901f4jzkYaOO6SFNNX8J0_sZS53pOovMLSgWM',
                'name'=>'Informe de Capacitación - SGC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>32,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1lcVZM5NA33E2oobUiwscN-M2gvMq2y7t2Djz5gG7wcI',
                'name'=>'SGC-F-013 Programa Anual de mantenimiento y calibración de instrumento',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>33,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ykOJJpRn6rMYVnLdoTbGKNxPRJBv4xk1TlZA7vNMxBM',
                'name'=>'SGC-P-006 Mantenimiento preventivo y calibración de instrumentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>33,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ZJ5nHP9d-v5zH7oqZ5PGC1tVPRDT8mXoBJ4z63ygpTM',
                'name'=>'SGC-P-010 Manual de Calidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>34,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1t9C4kRAd4yGWquIAZ6wXeAkEa8uo-Yn1irylnV4B_gk',
                'name'=>'SGC-P-013 Mapa de procesos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>35,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'196dmU03UqSweJ8ANINDZlDqC741gOXcvTQowILEpD0U',
                'name'=>'SGC-P-008 Planeamiento Estratégico',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>36,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'10UhzmByFpqj8tdhZrN7gX71Pj56KFlFejzBn4jKXh8o',
                'name'=>'SGC-F-015 Política de Calidad oficial',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>37,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1TVF5sjzOuIzNKGlBJqSISJ4nTUo5UXB2O-hok1kCNEM',
                'name'=>'AA-P-001 Procedimiento de Facturación y cobranzas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1LtYtFCZ7aB07pJOpFd0UdiVJbuZqhTrl7g2zDnaiCBo',
                'name'=>'AC-P-001 Procedimiento Comercial',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1b6WkCMXoQIMyB5W9BHcThdhCd9HG8OgNqlg5tc8x3Dw',
                'name'=>'AL-P-001 Procedimiento de Compras',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1aIw_XA7dbIeFCcfT5e39rytfXdumZv2-FGdCmQxqiR4',
                'name'=>'AL-P-002 Procedimiento de despacho',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1LSFdaXiRm8IYdYCkeCJbsemeVKRMLV-lwJBLpx-tWvw',
                'name'=>'AL-P-003 Procedimiento de Almacén',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'16b02bd80pkP7kAQqhzp4E5QxuntN76cz-aZhcivUmY0',
                'name'=>'AP-P-001 Procedimiento de producción',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1QO5oOd0vClNp2ce-YFPX5DAmMg_2jQbFcGGBXiR0jAk',
                'name'=>'AQ-P-001 Procedimiento de Calidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>38,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ot_HueGACz_ccJePJGo0R2gGBU07-XfwmIeJG0iSgJk',
                'name'=>'SGC-F-019 Programa anual de calidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>39,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1D7Vdis_g4IPkX6WHWaGageE62tPfaeXF4ciLjT5Zagg',
                'name'=>'SGC-P-005 Evaluación, selección y reevaluación de proveedores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>40,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1MhIrI8APmMXKvbeKdsPuKEadZ0KZkTb5kH5gLDXPb54',
                'name'=>'SGC-F-010 Evaluación de proveedores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>40,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CFzSERu5eznPDjaJ0Myy2T-yc7S1bLGL0clDsj9XNcU',
                'name'=>'SGC-F-011 Relación de proveedores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>40,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1n13Rw0GcemdcrZvFP4cfY3D2t47VSYa5FO43RnLOU1Y',
                'name'=>'SGC-F-012 Reevaluación de proveedores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>40,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1weqeshqRRfI849N9B0MsCoJ7tX4MVJtgIed-EAMfxfc',
                'name'=>'SGC-F-020 - lista de conformidad del procedimiento',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>41,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'17FLfFx1VJUO7JVUX8CO2958AFZdhbBGtVvEoDDnE3rM',
                'name'=>'SGC-F-007 Informe de Desempeño del Sistema de Gestión de Calidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>42,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'10KotDhcMITq-Js1LVQkpCOcS2_yVI3K_A8IYk2BgxV8',
                'name'=>'SGC-P-002 Revisión por la dirección',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>42,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'102e6Yh8rRZfgPaGC7frk2OR_Clmv-J83EBLvvrY1HNo',
                'name'=>'SGC-F-017 Registro de productos no conformes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>43,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1srr9t0PfEA_EQIFZfQoKkV4nOkon0a_VISHWcd1vk-U',
                'name'=>'SGC-P-013 Control de las salidas no conformes y liberación del producto',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>43,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1buWiUMjgQA7lbkNoHLq_01cnPunKWHnodr0AYgzaSZ0',
                'name'=>'Evaluación de Encuesta de sastifacción al cliente',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>44,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1XH3YUdHWwa6o0seb_evzf4P1LSH6fETV1sHP6Zgpn1A',
                'name'=>'SGC-F-014 Encuesta de sastifacción del cliente',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>44,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1aRDcT_nAERZoDYuM3znr0EG3mlsKNVHe_OHg0dA7iDY',
                'name'=>'SGC-P-007 Sastifacción al cliente',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>44,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1-hwwgcVfCrJWK3b3BIv4iQm8u5Hu6HYvlYtolaCY5bE',
                'name'=>'Material de Sensibilizacion',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>45,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1deO3weIL9gNbkoo7uyPNNG0RyHVrCD2M2yLQII5rXfc',
                'name'=>'Política de Sistema de Seguridad en la Cadena de Abastecimiento',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ttKlo5co9mFMQyQCnM8X2Wg-Bz_iHyH8NI4FHPotrUI',
                'name'=>'SGCA-SC-RE-02 Registro control precintos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1x39PuqQSgYfi5NPun9eM0pPf31YNmGiYkk96S7xLdWQ',
                'name'=>'SGCA-SC-RE-02- REGISTRO FOTOGRAFICO DE EMBARQUE',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CkVFpafbeeW3q1rLRp32vD3_d6O6gUs4_ohzL5XDGWk',
                'name'=>'SGCA-SC-PO-01- Seguridad Carga',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1IVLk6dxXYO7AQPZpfS06na-3fzJ9Wg198SZ3bMWrV44',
                'name'=>'SGCA-SC-PO-02- Procedimiento de uso y control de precintos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'10JD7SHbxYEjr55P7NjpzqvQq_tR2eITq1mA1sIEagVI',
                'name'=>'SGCA-SC-PO-03- CONTROL E INSPECCION DE MATERIALES EMBALAJE',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1zYkjg96iBDzTIyWViqhPRUYMTixr7ziKBMrd3EiiC1c',
                'name'=>'SGCA-SC-RE-01 Registro de Inspeccion de Vehiculos de Carga',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>46,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1qpRCqiCU75xMcVYqVxRcT0_2jta6cBWswLajqQ18sKg',
                'name'=>'Listado de Personal Critico',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1K9r79Kp4R1yNeJZNCgIS67fFN3pv7hgl4fXM6KBc2mo',
                'name'=>'SGCA-RH-FO-01- D.J de domicilio',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1S-bjLrvP1YfIBLOdhIBog7ZF7vt_0jHstm2UFzKfaJk',
                'name'=>'SGCA-RH-PO-01- Procedimiento-de-reclutamiento-y-selección-de-personal',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1W4weargXaLkKj8ekNYs7uUwvnU5H3zKmKX_dhMxnA1Y',
                'name'=>'SGCA-RH-PO-02- Procedimiento de Desvinculacion de trabajadores',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ykhsO_14X7JoFHUS9xizMg6ujEvdDlwJgwtyJfGiiLg',
                'name'=>'SGCA-RH-RE-01- Registro de Ficha de Ingreso',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1B1s-msbxrbangpoYU1Tt6PkHNjlvB6vJuJRYP02GOeo',
                'name'=>'SGCA-RH-RE-02- Registro de Puestoss Criticos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>47,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1o3nkTLF-ZpaBRL3ROXGQtqdSBc-IzfVd3JJLQv2ccLg',
                'name'=>'Procedimiento de Sellos y firmas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>48,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1QHgNIxco1NSoW9IDVDUpVRcJeeSP0PVMd6UABAHXl-4',
                'name'=>'Registro de Firmas Autorizadas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>48,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1QGIFWhKdlhb-QVBdAjas-i5umNdgGj65c6ubVvg_XBg',
                'name'=>'Registro de Sellos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>48,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'192gihuW-kJ-SnUSUDFQQrp-IcUxQkkpMsI7X0CEtAB8',
                'name'=>'Registro de Firmas Autorizadas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CiYnVPWips-Si3c77JI-hpZLRmF7GTL-T21ZQPaBv4k',
                'name'=>'SGCA-SF- PO-03 - Procedimiento de Respuesta ante eventos critico',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1dmvlVNI7JwgVv0zJQ1TRiZ8SLzAXzW09QEYVwXc-exE',
                'name'=>'SGCA-SF-PO-01 - Procedimiento de Seguridad Fisica',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'11IF6d6eNubiTY2ftCBfSCpOWox2REANEwLj1dK0AG3k',
                'name'=>'SGCA-SF-PO-02 - Procedimiento de Control de llaves',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1gUlwJpQIFF7Vv3UcMxaeEhw6dtEBSUYE3rU1CkZbv5Q',
                'name'=>'SGCA-SF-RE-01 - Registro de Visitas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Ds4Bhp8txapjTrcXU_BvoXVh3VoZQBIvri8YXYvuZAc',
                'name'=>'SGCA-SF-RE-02 - Inventario de Llaves',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1gXOzuKLeAydZX7BTVp8wiWOdF7Hf03vqq2-I_mjAOT4',
                'name'=>'SGCA-SF-RE-03 - Registro de Inspeccion de Instalaciones',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Pkn9FQ8UxAraL5iQCDKm7QMWMThYmFT6ANGRVzeoej0',
                'name'=>'SGCA-SF-RE-04 - Registro de Despacho de Corte Rev1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1jFk9x7Q3KcskcUVgXn8NosKfDSkXzp5J2kJmb-b9V9Y',
                'name'=>'SGCA-SF-RE-04 - Registro de Despacho de Corte',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1ybtJkKCq6npXJ_D_S5lsQM-IijmIWNjGOXxpVyM4TlI',
                'name'=>'SGCA-SF-RE-05- Registro de Control de Llaves',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>49,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1hHV-bBJnxwIBVVNnIaK9ma287cvK_111sAwqI7vBs9M',
                'name'=>'AFICHES BASC_Mesa de trabajo 1 copia 3',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>50,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'11KwKk4QU7ZHakt1QawoTyl5U1mG7nshncu7fDcWfhCw',
                'name'=>'AFICHES BASC_Mesa de trabajo 1 copia',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>50,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1CJMhk2BGrxTO_yiMH-mSnQ9OZdXSTeSbD0-gT05Lq_U',
                'name'=>'AFICHES BASC_Mesa de trabajo 1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>50,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'12spUXXg5GISo5TOn0VZfoGyUfUIWWQm2AQoP3eDkoHk',
                'name'=>'SGCA-F-001 Listado Maestro de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>51,
            ]);
        //NUEVOS FORMATOS
        //SGCN
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1JIfepVGsZ6H8gCRmCRzH9dBbY_eOqbj39nPDWOlh5jk',
                'name'=>'Línea base 22301',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>52,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1a_s2aSNd4xF2pGo2Spy009B-pTPNDU7WrFYNHRDQ0IU',
                'name'=>'Preliminar del Proyecto',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>53,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1JzY1ptH0STVUD8G2ajfQ20OhCoTw5JiEux3TUqiJ2q0',
                'name'=>'Plan de trabajo y comunicación',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>54,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1kGikTdGOipIJE2CnSts95w7cequi1QfAFoAy7yBM1m4',
                'name'=>'Plan de BC',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>55,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1RQzBTTEy3lUD8of5j6kEZaMOyKXoo-uJeMK6opxzeS8',
                'name'=>'Política del Sistema de Continuidad',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>56,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1abcX4mnkKC55m5QPlleyNV1G-NbmXvNiL9-gy9Jp2aU',
                'name'=>'SGCN-P-001 Procedimiento de Control de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>57,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1dXz7pmIwmJ3FxQkOo7erayrbEHdqTYuzX7pDWLCBc8E',
                'name'=>'SGCN-F-004 Consolidado de incidencias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>57,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'14JHBK-Re3n99gvRiWjsEx6dTHcWgedEzEn1szLcr13c',
                'name'=>'SGCN-F-003 Formato Reporte de incidencias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>57,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1AsVTWSpxo2zEGtj3aIipe1FgDxdIqbgjcvdXNSeHj_k',
                'name'=>'SGCN-F-005 Solicitud de Cambios',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>57,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1iVOUEYDWu6ia5Xy6mBSfQCosWWq1XcVeuqFnKb4V40g',
                'name'=>'SGCN-F-002 Formato de Solicitud de elaboracion, actualización y eliminación de documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>57,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1SL_HXGj9hAxj9jhxWA4vFFUFk7PJWroXaS62Th7AqVY',
                'name'=>'SGCN-F-001 Lista Maestra de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>57,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'17UyQ5YuU6v6XtSGnRgEHAydrEsc8w78L3y5maahmRfY',
                'name'=>'Acta-003-20 Reunión Ordinaria - SGCN',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>58,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1_MMQZF0T4CghvuwInibxy6rL_QTW7Lr-naSor0aHnHg',
                'name'=>'Acta-002-20 Aprobación de Documentos - SGCN',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>58,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1v81i7_-m4orrDsL3u-C27BX9GoitsV_1vOsHgw8tbTE',
                'name'=>'Acta-001- 20 Instalacion - SGCN',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>58,
            ]);


        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Vah4_ClOq5uS1cRX42-o-hR9yhZ6Nh1sGz2Izlzw0DA',
                'name'=>'AF-P-001 Procedimiento de Facturación y cobranzas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1UFzc-x72idqel9BrD8WE5SZ65LVwu0I-UaJmpdbBDC4',
                'name'=>'AA-P-003 Procedimiento de Gestión de proyectos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1PexpcN8UCDUoeBUEf5JlUFK4dyXO3op-tf45mjQ-6wo',
                'name'=>'AA-P-002 Procedimiento de Pagos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1RCrlnueDqQqWt71s7ucpX95y42B2WgESH7R_IBg6kqI',
                'name'=>'OXP-P-001 Procedimiento de Compras',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);


        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1soGYiQAUum-mTVnbRmZ1eRBcseZAGA45K9ILAiOJYL0',
                'name'=>'HR-P-006 Procedimiento de Staffing 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1D2WRWkX46RkaGOic9HBP9NhT6RUnYj1Ixs7bdaWt5IU',
                'name'=>'HR-P-005 Procedimiento de contratacion y desvinculación 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1KvBnkyps4RTB15X-C24qbgQH-DzKaxL08uFUcBZ5LaY',
                'name'=>'HR-P-004 Procedimiento de reconocimientos 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1lLr-4Qys6JtEd_Fy9__Eu0MSEloFYS1fQhkSjaAkD-s',
                'name'=>'HR-P-003 Procedimiento de evaluación de desempeño 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1GyjD93U-O8izK2kxN3zIHmC3iAe0JEnfry-7H5SH7tU',
                'name'=>'HR-P-002 Procedimiento de inducción 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1VyN00UiWH5i4QjmKEH0wUVKKP2OwPeuhX_4jMw5IQyA',
                'name'=>'HR-P-001 Reclutamiento y Selección 1.0',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>59,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1esQuUS0m7G5vOdXKAM1kRU56vdVN3zfx1NaEndF4crM',
                'name'=>'SGCN-P-003 Plan del Impacto al Negocio',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>60,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1dJFXiIa4kTBoooLAm-QGCVg0eQDz-aPIA8gt-mCfwD4',
                'name'=>'SGCN-F-008 Análisis del impacto al Negocio - RRHH',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>60,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1sTtBCb8PdS7lEFaDBfEfXEE7jmh3CYp2Qqy5n2f9x8M',
                'name'=>'SGCN-F-007 Análisis del impacto al Negocio - Finanzas',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>60,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Fdgm_1jqrPknBnRVZ82iV5JxGYqcmGqkr3aOoTCc7Ho',
                'name'=>'SGCN-F-006 Análisis del impacto al Negocio - OPX',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>60,
            ]);
        //SGHS
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'14pE7OVnA5oe2ErI81FsC73OFGd5MT9C5wfcwu9RmCfk',
                'name'=>'ADM-PRO-002 Programa de Capacitaciones en prevención sexual',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1_57576RVUrWOwVa-s7KV9mYa2YUgljMabx2DPwQQZnM',
                'name'=>'ADM-PRO-001 Programa anual de Prevención frente al Hostigamiento sexual',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1WY-hwoKJCl9t30YfnLiFii1R-PHh_So9It77vjAu-EU',
                'name'=>'ADM-P-006 Programa de capacitación, difusión y control',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1kcItihMZK84k_JZMJCCd9L84pkFIW1tYPYiolfg_-XI',
                'name'=>'ADM-P-005 Encuesta de Acoso y Hostigamiento Sexual',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1KSI_eelGf462GegMCVK1km82YD1HL6EJ7bogulrHKLY',
                'name'=>'ADM-P-004  Plan anual de Prevención del hostigamiento sexual',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1_WUyZzvXREjHFamobTHI11d2qbAwQMKhznxoGl6Y-MA',
                'name'=>'ADM-P-003 Procedimiento de responsabilidades y funciones del Comité',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1pcaG52c272psZ73yWUuqPLJrRjdHIlfj_U9kbHi7rZc',
                'name'=>'ADM-P-002 Procedimiento de creación del Comité de Intervención frente al HS',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1TlwfcXsNsrZsZf5ZumtnBwpuYl_-z8X9nmkI__AnpgQ',
                'name'=>'ADM-P-001 Procedimiento de Acoso y Hostigamiento Sexual',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>61,
            ]);
        //SGAS
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1SmvNFh9uQtQYDssBgz-Pq6mmuZ9hsHUDAJ3psdbAC1w',
                'name'=>'SGAS - 006 - Sistema Confidencial de Denuncias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1Quhl1vr8DAsphw8U_Mtq7_JjpNVTU_hlX8D29I_ZFic',
                'name'=>'SGAS - 004 - Plan anual de Prevencion de Corrupcion lavado de activosy financiamiento terrorista. Rev1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1o87P3X3V5mFKrHwU5Z_phNFdIULPSuhl_0q-7VyVrSc',
                'name'=>'SGAS - 003 - Metodologia de la Matriz de Identificacion de Riesgos Rev.1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'15YIE7O3iDUuNvHGXoM1xvsWrts7U4Ctq7bZR-gORxZk',
                'name'=>'SGAS - 002 - Politica para La Prevencion de la Corrupción Lavado de Activos y Financiamiento Terrorista. Rev 1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'17Nb47-rNUKamJP8KyyyyGlb4y6eWbqaF3e_VEGservM',
                'name'=>'SGAS - 001- Código de ética y conducta',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1niwOnl4mn24TvxwkHRTl9D55lHJmm5Oeti9PrWH98u4',
                'name'=>'SGAS - 008 - Evaluación, selección y reevaluación de clientes Rev 1',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::DOCS,
                'sub_category_id'=>62,
            ]);

        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'14s_Lbo5nvHx96k1ihw4SR66nfHbS4lSbHPcLgIH_qK0',
                'name'=>'SGAS - 012 - Listado Maestro de Documentos',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1bcD_CZSX1fwm5ZyuTmU8ZgXGZXkS2rZjzKkHfrW98lY',
                'name'=>'SGAS - 010 - Relación de Clientes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1YARH4juP20NbOy7AYR_bpHb3_XLuD97JI7NYhmla2LI',
                'name'=>'SGAS - 011 - Reevaluación de Clientes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1o75Sh7n3PhXXj0GkSXD2I9eMO_oumaBnqya6MYk_ZHA',
                'name'=>'SGAS - 009 - Evaluación de Clientes',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'11Qul4WriwoOdOH1fslf2SjSNDzXYwrfcHkGmNDsKw3g',
                'name'=>'SGAS - 007 - Consolidado de Denuncias',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);
        factory(\App\DocumentTemplate::class, 1)
            ->create([
                'google_document_id'=>'1GlprfmxQLC6u_wy_XFuU6BVBqhgtoUG770lgU_yJAtM',
                'name'=>'SGAS - 005 - Programa Anual SGAS',
                'description'=>'Descripción',
                'document_type_id'=>\App\DocumentType::SHEETS,
                'sub_category_id'=>62,
            ]);







        //DOCUMENTOS POR TIPO DE TRABAJO
        for ($i = 1; $i <= 2; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  1,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 3; $i <= 10; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  2,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 11; $i <= 61; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  3,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 62; $i <= 108; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  4,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 109; $i <= 139; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  5,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 140; $i <= 167; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  6,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 168; $i <= 175; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  7,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }
        for ($i = 176; $i <= 187; $i++) {
            DB::table('document_template_work_type')->insert(
                [
                'document_template_id' =>  $i,
                'work_type_id' =>  8,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ]
            );
        }


    }
}
